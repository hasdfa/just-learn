package com.vadim.hasdfa.justlearn.model

/**
 * Singleton object, which shows user type
 * in current application lifecycle
 */
object UserData {
    var userType = UserType.Null

    var student: Student? = null
    var teacher: Teacher? = null

    var didChange: (UserType) -> Unit = {}

    fun set(teacher: Teacher) {
        this.teacher = teacher
        userType = UserType.Teacher
        didChange(userType)
    }

    fun set(student: Student) {
        this.student = student
        userType = UserType.Student
        didChange(userType)
    }

    enum class UserType {
        Student, Teacher,
        Null
    }
}