package com.vadim.hasdfa.justlearn.core.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.view.LoadingPreview

/**
 * Base activity, which must be superclass for all activity in this module
 * Implements {@link BaseFunctions}
 *
 * Does not must be registered in manifest
 */
@SuppressLint("Registered")
open class ActivityBase: AppCompatActivity(), BaseFunctions {
    override fun nullableContext(): Context? = this

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportFinishAfterTransition()
    }

    override val loading: View by lazy {
        LoadingPreview.addToParentOf(this)
    }

    fun replaceFragment(fragment: FragmentBase) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()

//        fragmentTransaction.setCustomAnimations(R.anim.fade_in,
//                R.anim.fade_out)
        fragmentTransaction.replace(R.id.mFrame, fragment)
        fragmentTransaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        supportActionBar?.title = ""
        actionBar?.title = ""
    }

    /**
     * Clear all listeners, when activity stops
     *
     * Lifecycle element
     */
    override fun onStop() {
        super.onStop()
        if (FirebaseDatabaseHelper.clearAllListeners()) {
            Log.d("myLog", "Success removed all listeners")
        } else {
            Log.e("myLog", "Error removed all listeners")
        }
    }
}