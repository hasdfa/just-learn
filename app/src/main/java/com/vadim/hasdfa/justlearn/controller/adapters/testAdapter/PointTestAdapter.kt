package com.vadim.hasdfa.justlearn.controller.adapters.testAdapter

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.support.annotation.LayoutRes
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.*
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.widget.LinearLayout
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import com.vadim.hasdfa.justlearn.utils.Statuses
import com.vadim.hasdfa.justlearn.utils.bd
import com.vadim.hasdfa.justlearn.utils.try_bd
import com.vadim.hasdfa.justlearn.view.CheckBoxGroup
import java.math.BigDecimal


/**
* Created by Raksha Vadim on 12.08.17, 01:39.
* With love.
* In project JustLearn.
*/
class PointTestAdapter(): RecyclerView.Adapter<PointTestAdapter.BaseHolder>() {
    private var test: Test = Test()

    public var afterUpdate: () -> Unit = {}

    private var _q: MutableList<QBase> = arrayListOf()
    private var questions: MutableList<QBase>
        get() = _q
        set(value) {
            _q = value.filter { it._value == null }.toMutableList()
        }

    constructor(test: Test): this() {
        this.test = test
        questions = test.questions
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0)
            return ViewTypes.header // header

        val q = questions[position-1]

        return when (q.type) {
            QType.Dropdown -> ViewTypes.dropdown
            QType.RadioBtn -> ViewTypes.radio
            QType.Checkbox -> ViewTypes.checkbox
            QType.Text -> ViewTypes.text
            QType.TextArea -> ViewTypes.textArea
            else -> throw Exception("Type doesn`t support")
        }
    }

    private fun createViewCreator(parent: ViewGroup?): (Int) -> View {
        return fun (@LayoutRes l: Int): View {
            return LayoutInflater.from(parent?.context).inflate(l, parent, false)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PointTestAdapter.BaseHolder {
        val createView = createViewCreator(parent)

        return when (viewType) {
            ViewTypes.header -> HeaderViewHolder(createView(R.layout.question_preview_header))
            ViewTypes.dropdown -> DropdownViewHolder(createView(R.layout.question_dropdown))
            ViewTypes.radio -> RadioBtnViewHolder(createView(R.layout.question_radiobtns))
            ViewTypes.checkbox -> CheckboxViewHolder(createView(R.layout.question_checkboxes))
            ViewTypes.text -> TextViewHolder(createView(R.layout.question_text))
            ViewTypes.textArea -> TextAreaViewHolder(createView(R.layout.question_text_area))
            else -> throw ClassNotFoundException("Type not found")
        }
    }

    fun value(str: CharSequence) = when (str) {
        "⅓" -> BigDecimal("0.33")
        "½" -> BigDecimal("0.5")
        "¼" -> BigDecimal("0.25")
        else -> bd(str)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PointTestAdapter.BaseHolder, position: Int) {
        val context = holder.itemView.context
        if (position == 0) {
            if (holder is HeaderViewHolder) {
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.titleView.setTextColor(it.questionColor)
                    }
                }
            }
            return
        }

        val question = questions[position - 1]

        val viewGroup = ((holder.itemView as? ViewGroup)?.getChildAt(0) as? ViewGroup)
        val editMenu = holder.itemView.findViewById<View>(R.id.editMenu)
        viewGroup?.removeView(editMenu)


        val buttonPointsView = LayoutInflater.from(viewGroup?.context).inflate(R.layout.buttons_points, viewGroup, false)
        val buttonsLP = buttonPointsView?.layoutParams as? RelativeLayout.LayoutParams
        buttonsLP?.addRule(RelativeLayout.BELOW, R.id.helperTextView)
        buttonPointsView?.layoutParams = buttonsLP
        viewGroup?.addView(buttonPointsView)

        val pointView = LayoutInflater.from(viewGroup?.context).inflate(R.layout.point_view, viewGroup, false) as AppCompatButton
        pointView.layoutParams = editMenu?.layoutParams
        pointView.visibility = View.INVISIBLE
        viewGroup?.addView(pointView)

        val colorPrimary =  ContextCompat.getColor(context, R.color.colorPrimary)
        val colorPrimaryDark =  ContextCompat.getColor(context, R.color.colorPrimaryDark)

        fun onClick(id: Int, action: (View) -> Unit) { buttonPointsView.findViewById<AppCompatButton>(id)?.setOnClickListener(action) }

        val ids = arrayOf(R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn_custom)
        var selected = -1
        fun clearAll(out: Int) {
            pointView.visibility = View.VISIBLE
            if (selected == out) return
            selected = out
            ids.forEachIndexed { index, it ->
                val btn = buttonPointsView.findViewById<AppCompatButton>(it)
                if (index == out) {
                    btn.setBackgroundColor(colorPrimaryDark)
                    if (index != 5) {
                        val v = value(btn.text)
                        pointView.text = v.toString()
                        question._value = v.toString()
                    }
                } else
                    btn.setBackgroundColor(colorPrimary)
            }
            afterUpdate()
        }

        onClick(R.id.btn1) { clearAll(0) }
        onClick(R.id.btn2) { clearAll(1) }
        onClick(R.id.btn3) { clearAll(2) }
        onClick(R.id.btn4) { clearAll(3) }
        onClick(R.id.btn5) { clearAll(4) }
        onClick(R.id.btn_custom) {
            val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
            builder.setTitle("Enter question value")

            val input = EditText(context)
            input.setTextColor(Color.WHITE)
            input.setHintTextColor(Color.WHITE)
            input.hint = "Enter value"
            (context as? ActivityBase)
                    ?.setInputTextLayoutColor(Color.WHITE, null, input)

            input.inputType = EditorInfo.TYPE_CLASS_NUMBER
            input.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                     LinearLayout.LayoutParams.MATCH_PARENT)
            input.setPadding(32, input.paddingTop + 8, 24, input.paddingBottom + 8)
            builder.setView(input)

            builder.setPositiveButton("OK") { _, _ ->
                val num = try_bd(input.text)
                if (num != null) {
                    pointView.text = num.toString()
                    question._value = num.toString()
                    clearAll(5)
                } else {
                    (context as? ActivityBase)
                            ?.showError("Incorrect number. Please enter correct number")
                }
            }
            builder.setNegativeButton("Cancel", null)

            val alertDialog = builder.create()
            alertDialog.show()
        }

        fun styleCardView() {
            (holder.itemView as? CardView)?.let {
                it.setCardBackgroundColor(Color.alpha(0))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.elevation = 0f
                }
                it.radius = 0f
            }
        }

        when (holder) {
            is TextViewHolder -> {
                val q = question as QText
                holder.headerTextView.hint = q.question
                holder.helperTextView.text = q.helpText

                holder.contentTextView.requestFocus()
                holder.contentTextView.isEnabled = false
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.contentTextView.setTextColor(it.answerColor)
                        holder.helperTextView.setTextColor(it.questionColor)
                        holder.contentTextView.setHintTextColor(it.questionColor)
                        setUpperHintColor(holder.headerTextView, it.questionColor)

                        ViewCompat.setBackgroundTintList(holder.contentTextView, ColorStateList.valueOf(it.questionColor))
                        styleCardView()
                    }
                }
            }
            is TextAreaViewHolder -> {
                val q = question as QTextArea
                holder.headerTextView.hint = q.question
                holder.helperTextView.text = q.helpText
                holder.contentTextView.isEnabled = false
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.contentTextView.setTextColor(it.answerColor)
                        holder.helperTextView.setTextColor(it.questionColor)
                        holder.contentTextView.setHintTextColor(it.questionColor)
                        setUpperHintColor(holder.headerTextView, it.questionColor)
                        holder.symbolsTextView.setTextColor(it.answerColor)
                        holder.divTextView.setTextColor(it.answerColor)
                        holder.maxSymbolsTextView.setTextColor(it.answerColor)

                        ViewCompat.setBackgroundTintList(holder.contentTextView, ColorStateList.valueOf(it.questionColor))
                        styleCardView()
                    }
                }
            }
            is DropdownViewHolder -> {
                val q = question as QDropdown
                holder.headerTextView.text = q.question
                configureSpinner(holder.dropdown, q.items, {})
                holder.helperTextView.text = q.helpText
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.helperTextView.setTextColor(it.questionColor)
                        holder.headerTextView.setTextColor(it.questionColor)
                        styleCardView()
                    }
                }

                holder.dropdown.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        q.answer = holder.dropdown.adapter.getItem(position).toString()
                        Log.d("myLog", "Rbt: ${q.answer}")
                        afterUpdate()
                    }
                }
            }
            is RadioBtnViewHolder -> {
                val q = question as QRadioButtons
                holder.headerTextView.text = q.question
                holder.helperTextView.text = q.helpText

                var theme: FormThemes.ThemeBase? = null
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.helperTextView.setTextColor(it.questionColor)
                        holder.headerTextView.setTextColor(it.questionColor)
                        styleCardView()
                        theme = it
                    }
                }
                holder.radioGroup.removeAllViews()
                for (i in 0 until q.items.size) {
                    val radioButton = AppCompatRadioButton(holder.itemView?.context)
                    radioButton.text = q.items[i]
                    radioButton.id = i
                    radioButton.textSize = context.resources.getDimension(R.dimen.answer_text_size)
                    val rParams = RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                    holder.radioGroup.addView(radioButton, rParams)
                }
                holder.radioGroup.setTheme(theme)

                holder.radioGroup.setOnCheckedChangeListener { group, checkedId ->
                    val rbt = group.getChildAt(checkedId) as AppCompatRadioButton
                    q.answer = rbt.text.toString()
                    Log.d("myLog", "Rbt: ${q.answer}")
                    afterUpdate()
                }
            }
            is CheckboxViewHolder -> {
                val q = question as QCheckbox
                var theme: FormThemes.ThemeBase? = null

                holder.headerTextView.text = q.question
                holder.helperTextView.text = q.helpText
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.helperTextView.setTextColor(it.questionColor)
                        holder.headerTextView.setTextColor(it.questionColor)
                        styleCardView()
                        theme = it
                    }
                }
                holder.checkboxGroup.removeAllViews()
                for (i in 0 until q.items.size) {
                    val checkBox = AppCompatCheckBox(holder.itemView?.context)
                    checkBox.text = q.items[i]
                    checkBox.id = i
                    checkBox.textSize = context.resources.getDimension(R.dimen.answer_text_size)
                    holder.checkboxGroup.addCheckBox(checkBox)
                }
                holder.checkboxGroup.setTheme(theme)

                holder.checkboxGroup.onCheckListener { chb, isChecked ->
                    val answer = chb.text.toString()

                    val index = q.answers.indexOf(answer)
                    if (index >= 0) {
                        q.answers.removeAt(index)
                    } else {
                        q.answers.add(answer)
                    }
                    afterUpdate()
                    Log.d("myLog", "Chb: ${q.answers.joinToString()}")
                }
            }
            else -> throw Exception("Holder child wasn't found")
        }
    }

    override fun getItemCount(): Int {
        val size = questions.size
        return if (size != 0 && test.testStatus == Statuses.Editing) {
            size + 1
        } else
            size
    }


    inner open class BaseHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    }

    inner class TextViewHolder(itemView: View): BaseHolder(itemView) {
        val headerTextView: TextInputLayout = itemView.findViewById(R.id.headerTextView)
        val contentTextView: TextInputEditText = itemView.findViewById(R.id.contentTextView)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class TextAreaViewHolder(itemView: View): BaseHolder(itemView) {
        val headerTextView: TextInputLayout = itemView.findViewById(R.id.headerTextView)
        val contentTextView: TextInputEditText = itemView.findViewById(R.id.contentTextView)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
        val symbolsTextView: TextView = itemView.findViewById(R.id.counterTextView)
        val divTextView: TextView = itemView.findViewById(R.id.divTextView)
        val maxSymbolsTextView: TextView = itemView.findViewById(R.id.maxSymbolsTextView)
    }

    inner class DropdownViewHolder(itemView: View): BaseHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val dropdown: Spinner = itemView.findViewById(R.id.dropdown)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class RadioBtnViewHolder(itemView: View): BaseHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val radioGroup: RadioGroup = itemView.findViewById(R.id.radioGroup)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class CheckboxViewHolder(itemView: View): BaseHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val checkboxGroup: CheckBoxGroup = itemView.findViewById(R.id.checkboxGroup)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class HeaderViewHolder(itemView: View): BaseHolder(itemView) {
        val titleView: TextView = itemView.findViewById(R.id.titleTextView)
        init {
            @SuppressLint("SetTextI18n")
            titleView.text = "Виберіть відповоді та бали" // TODO
        }
    }

    private object ViewTypes {
        const val text = 0
        const val textArea = 1
        const val dropdown = 2
        const val radio = 3
        const val checkbox = 4
        const val header = -1
    }

    private fun configureSpinner(spinnerSelectTopic: Spinner, array: List<String>, onItemSelected: (Int) -> Unit){
        spinnerSelectTopic.adapter = ArrayAdapter<String>(
                spinnerSelectTopic.context,
                R.layout.spinner_item,
                R.id.spinner_item_text_view,
                array
        )

        colorizeSpinnerItem(spinnerSelectTopic)
        spinnerSelectTopic.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                colorizeSpinnerItem(spinnerSelectTopic)
            }
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onItemSelected(position)
                colorizeSpinnerItem(spinnerSelectTopic)
            }
        }
    }

    private fun colorizeSpinnerItem(spinnerSelectTopic: Spinner){
        var textColor = Color.BLACK
        var backColor = Color.WHITE
        if (test.themeId >= 0)
            FormThemes.getThemes()[test.themeId].let {
                textColor = it.answerColor
                backColor = it.backgroundColor
            }

        spinnerSelectTopic
                .findViewById<TextView?>(R.id.spinner_item_text_view)
                ?.setTextColor(textColor)
        spinnerSelectTopic
                .findViewById<RelativeLayout?>(R.id.backRL)
                ?.setBackgroundColor(backColor)
    }

    fun notifyDataSetChanged(newTest: Test) {
        this.test = newTest
        notifyDataSetChanged()
    }


    private fun RadioGroup.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return
        (0 until childCount)
                .map { this.getChildAt(it) }
                .filterIsInstance<AppCompatRadioButton>()
                .forEach {
                    it.setTextColor(theme.answerColor)

                    val colorStateList = ColorStateList(
                            arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                            intArrayOf(theme.questionColor, theme.questionColor)
                    )
                    CompoundButtonCompat.setButtonTintList(it, colorStateList)

                    it.invalidate()
                }
    }
    private fun CheckBoxGroup.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return
        (0 until childCount)
                .map { this.getChildAt(it) }
                .filterIsInstance<AppCompatCheckBox>()
                .forEach {
                    it.setTextColor(theme.answerColor)

                    val colorStateList = ColorStateList(
                            arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                            intArrayOf(theme.questionColor, theme.questionColor)
                    )
                    CompoundButtonCompat.setButtonTintList(it, colorStateList)

                    it.invalidate()
                }
    }

    private fun setUpperHintColor(textInputLayout: TextInputLayout, color: Int) {
        try {
            val field = textInputLayout::class.java.getDeclaredField("mFocusedTextColor")
            field.isAccessible = true
            val states = arrayOf(intArrayOf())
            val colors = intArrayOf(color)
            val myList = ColorStateList(states, colors)
            field.set(textInputLayout, myList)

            val method = textInputLayout::class.java.getDeclaredMethod("updateLabelState", Boolean::class.javaPrimitiveType)
            method.isAccessible = true
            method.invoke(textInputLayout, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}