package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.adapters.testAdapter.TestAdapter
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.BaseFunctions
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.questions.QText
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.utils.FormThemes
import com.vadim.hasdfa.justlearn.utils.Statuses
import kotlinx.android.synthetic.main.activity_create_test.*

/**
* Created by Raksha Vadim on 09.08.17, 01:42.
*/
class CreateTestActivity: ActivityBase() {

    private var newTest: Test = Test()
    private var mAdapter: TestAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_test)
        newTest.title = intent.extras.getString("title")
        newTest.groups = intent.extras.getStringArray("topic").toMutableList()
        newTest.testStatus = Statuses.Editing

        setSupportActionBar(findViewById(R.id.toolbar))

        mRecyclerView.layoutManager = LinearLayoutManager(this)

        mAdapter = TestAdapter(newTest) {
            if (it) {
                saveImageView.setImageDrawable(drawable(R.drawable.ic_back_active))
            } else
                saveImageView.setImageDrawable(drawable(R.drawable.ic_back_inactive))
        }
        mRecyclerView.adapter = mAdapter

        addQuestionFab.setOnClickListener {
            val intent = if (isTablet) {
                Intent(this, CreateQuestionActivity::class.java)
            } else {
                Intent(this, SelectTypeActivity::class.java)
            }
            val bundle = Bundle()
            bundle.putParcelable("q", QText())
            startActivityForResult(intent, BaseFunctions.IntentCodes.REQUEST_GET_Q)
        }
        saveImageView.setImageDrawable(drawable(R.drawable.ic_save_disable))
        saveImageView.setOnClickListener {
            if (newTest.questions.size > 0) {
                checkData {
                    startActivityForResult(Intent(this, PoinitingTestActivity::class.java)
                            .putExtra("test", newTest), 999)
                }
            } else {
                Toast.makeText(this, str(R.string.add_more_questions), Toast.LENGTH_SHORT).show()
            }
        }
        editThemeImageView.setOnClickListener {
            startActivityForResult(
                    Intent(
                            this, ChangeThemeActivity::class.java
                    ).putExtra("selected", newTest.themeId),
                    BaseFunctions.IntentCodes.REQUEST_GET_T
            )
        }
        testSettings.setOnClickListener {
            startActivityForResult(
                    Intent(this, TestSettingsActivity::class.java)
                            .putExtra("test", newTest),
                    BaseFunctions.IntentCodes.REQUEST_GET_S
            )
        }

        mRecyclerView.clearOnScrollListeners()
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0) {
                    addQuestionFab.hide()
                } else {
                    addQuestionFab.show()
                }

                super.onScrolled(recyclerView, dx, dy)
            }
        })

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    override fun onBackPressed() {
        showAlert(str(R.string.leave_without_saving), onOk = {
            super.onBackPressed()
            supportFinishAfterTransition()
        }, onCancel = {

        })
    }

    override fun onStart() {
        super.onStart()
        mAdapter?.notifyDataSetChanged()
    }

    private fun checkData(function: () -> Unit) {
        var isSuccess = true
        var errorDescription = ""

        if (newTest.title.isEmpty()) {
            isSuccess = false
            errorDescription = str(R.string.q_title_is_empty)
        }
        if (newTest.questions.size < 1) {
            isSuccess = false
            errorDescription = str(R.string.qarray_is_empty)
        }

        if (isSuccess) {
            function()
        } else {
            showError(errorDescription)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == 999) {
            supportFinishAfterTransition()
            return
        }

        if (resultCode == Activity.RESULT_OK && requestCode == BaseFunctions.IntentCodes.REQUEST_GET_Q) {
            if (data?.hasExtra("result") == true) {
                val q: QBase = data.getParcelableExtra("result")
                Log.d("myLog", "RESULT_OK\nQ:$q")

                val index = newTest.questions.map { it.id }.indexOf(q.id)
                if (index >= 0)
                    newTest.questions[index] = q
                else
                    newTest.questions.add(q)

                mAdapter?.notifyDataSetChanged(newTest)

                if (newTest.questions.isNotEmpty()) {
                    saveImageView.setImageDrawable(drawable(R.drawable.ic_back_active))
                }
                return
            } else showError(str(R.string.error_adding_new_q), {}).also { return }
        } else if (resultCode != Activity.RESULT_OK && requestCode != BaseFunctions.IntentCodes.REQUEST_GET_T && requestCode != BaseFunctions.IntentCodes.REQUEST_GET_S)
            showError(str(R.string.error_adding_new_q), {}).also { return }

        if (resultCode == Activity.RESULT_OK && requestCode == BaseFunctions.IntentCodes.REQUEST_GET_T) {
            data?.let {
                newTest.themeId = it.getIntExtra("result", -1)
                mRecyclerView.adapter.notifyDataSetChanged()
                if (newTest.themeId >= 0) {
                    FormThemes.getThemes()[newTest.themeId].let {
                        mRecyclerView.setBackgroundColor(it.backgroundColor)
                    }
                }
            }.also { return }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == BaseFunctions.IntentCodes.REQUEST_GET_S) {
            data?.let {
                newTest = it.getParcelableExtra("test")
                mRecyclerView.adapter.notifyDataSetChanged()
            }.also { return }
        }
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable("test", newTest)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            newTest = it.getParcelable("test")
        }
    }
}