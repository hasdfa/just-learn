package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.vadim.hasdfa.justlearn.controller.fragments.createTest.SelectThemeFragment
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.ActivityBase

/**
* Created by Raksha Vadim on 13.08.17, 21:53.
*/

class ChangeThemeActivity: ActivityBase(), SelectThemeFragment.SelectedThemeListener {
    override fun onSelect(position: Int) {
        setResult(Activity.RESULT_OK, Intent().putExtra("result", position))
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)

        supportActionBar?.let {
            it.title = str(R.string.change_test_theme)
            it.setDisplayHomeAsUpEnabled(true)
        }

        if (savedInstanceState == null) {
            val fragment = SelectThemeFragment()
            fragment.arguments = intent.extras
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.mFrame, fragment)
                    .commit()
        }
    }

}