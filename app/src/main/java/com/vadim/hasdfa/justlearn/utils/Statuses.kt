package com.vadim.hasdfa.justlearn.utils

import java.util.*

/**
 * Created by Raksha Vadim on 13.03.2018.
 * With love.
 * In project JustLearn.
 */
enum class Statuses {
    Editing,
    Enabled,
    Recorded,
    Review,
    Checked,
    Unknown;

    val localeName: String
        get() {
            return when (Locale.getDefault().language) {
                "ru" -> {
                    when (this) {
                        Statuses.Editing -> "Редактирование"
                        Statuses.Enabled -> "Доступно"
                        Statuses.Recorded -> "Отправленно"
                        Statuses.Review -> "На проверке"
                        Statuses.Checked -> "Проверенно"
                        Statuses.Unknown -> "Неизвесно"
                    }
                }
                "uk" -> {
                    when (this) {
                        Statuses.Editing -> "Редагування"
                        Statuses.Enabled -> "Доступно"
                        Statuses.Recorded -> "Відправлено"
                        Statuses.Review -> "На перевірці"
                        Statuses.Checked -> "Перевірено"
                        Statuses.Unknown -> "Невідомо"
                    }
                }
                else -> this.name
            }
        }
}