package com.vadim.hasdfa.justlearn.model.questions

import android.os.Parcel
import android.os.Parcelable
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType

/**
 * Question type, which describes
 * question view presentation as text box
 * witch must contains from 5 to 250 symbols
 *
 * Has parcelable implementation from superclass
 *
 * @see {@link com.vadim.hasdfa.justlearn.model.questions.utils.QBase}
 */
class QTextArea: QBase {
    /**
     * Realize of abstract field type
     */
    override var type = QType.TextArea

    /**
     * Call superclass`s constructor
     */
    constructor(): super()

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel): super(parcel)

    /**
     * Parcelable implementation
     */
    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable implementation
     */
    companion object CREATOR : Parcelable.Creator<QTextArea> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): QTextArea {
            return QTextArea(parcel)
        }

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<QTextArea?> {
            return arrayOfNulls(size)
        }
    }
}