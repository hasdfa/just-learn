package com.vadim.hasdfa.justlearn.controller.activity.browseTest

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.vadim.hasdfa.justlearn.controller.adapters.mainActivity.MainTestsAdapter
import com.vadim.hasdfa.justlearn.controller.adapters.mainActivity.StudentAdapter
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.UserData

/**
 * Created by Raksha Vadim on 15.03.2018.
 * With love.
 * In project JustLearn.
 */
class ShowTestGroup: ActivityBase() {

    val title by lazy {
        intent.getStringExtra("title")
    }

    val tests by lazy {
        intent.getParcelableArrayListExtra<Test>("tests")
    }

    val uids by lazy { tests.map { it.uniqueID } }

    val mRecyclerView by lazy {
        val rv = RecyclerView(this)
        rv.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT)
        rv.layoutManager = LinearLayoutManager(this)
        return@lazy rv
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mRecyclerView)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val mAdapter: RecyclerView.Adapter<*>
        if (title == "ЗНО") {
            mAdapter = MainTestsAdapter(UserData.student!!) { showError(it) }
            mAdapter.validate = { it.uniqueID in uids }
            mAdapter.isPrevZNO = true
        } else {
            mAdapter = StudentAdapter(UserData.student!!) { showError(it) }
            mAdapter.isShowHeader = false
            mAdapter.validate = { it.uniqueID in uids }
        }

        mRecyclerView.adapter = mAdapter
    }

}