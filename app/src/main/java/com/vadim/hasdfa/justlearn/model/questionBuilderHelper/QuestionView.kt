package com.vadim.hasdfa.justlearn.model.questionBuilderHelper

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.model.questions.utils.QType


/**
 * QuestionView use to show list of fields in
 * {@link com.vadim.hasdfa.justlearn.controller.activity.createTest.CreateTestActivity}
 *
 */
class QuestionView(): Parcelable {
    /**
     * Header value of question field
     */
    var headerText: String = ""

    /**
     * Hint of question field
     */
    var value: String = ""

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel): this() {
        headerText = parcel.readString()
        value = parcel.readString()
    }

    constructor(headerText: String): this() {
        this.headerText = headerText
    }

    constructor(headerText: String, value: String): this(headerText) {
        this.value = value
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(headerText)
        parcel.writeString(value)
    }

    /**
     * Parcelable implementation
     */
    override fun describeContents() = 0

    /**
     * Parcelable implementation
     */
    companion object CREATOR: Parcelable.Creator<QuestionView> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): QuestionView {
            return QuestionView(parcel)
        }

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<QuestionView?> {
            return arrayOfNulls(size)
        }


        /**
         * Add base elements for create test
         *
         * @see {@link com.vadim.hasdfa.justlearn.controller.adapters.TestAdapter}
         */
        internal fun addBaseData(context: Context, qType: QType): ArrayList<QuestionView> {
            val array: ArrayList<QuestionView> = ArrayList()
            val string: (Int) -> String = { context.resources.getString(it) }

            array.add(QuestionView(string(R.string.question_content)))

            when (qType) {
                QType.Dropdown -> {
                    setup(array, string(R.string.dropdown_item))
                }
                QType.RadioBtn -> {
                    setup(array, string(R.string.radio_item))
                }
                QType.Checkbox -> {
                    setup(array, string(R.string.checkbox_item))
                }
                else -> println("It is other")
            }

            array.add(QuestionView(string(R.string.help_text)))

            return array
        }

        /**
         * Setup method for specific question types
         *
         * @see {@link #addBaseData(QType, ArrayList<Boolean>)}
         */
        private fun setup(array: ArrayList<QuestionView>, text: String) {
            array.add(QuestionView(text))
            array.add(QuestionView(text))
            array.add(QuestionView())
        }
    }
}