package com.vadim.hasdfa.justlearn.controller.activity

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.view.View
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.fragments.createTest.SelectThemeFragment
import com.vadim.hasdfa.justlearn.controller.fragments.main.MainFragment
import com.vadim.hasdfa.justlearn.controller.fragments.main.MainTestsFragment
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.UserData
import com.vadim.hasdfa.justlearn.view.BottomNavigationBehavior
import kotlinx.android.synthetic.main.activity_main.*




class MainActivity: ActivityBase(), SelectThemeFragment.SelectedThemeListener {
    override fun onSelect(position: Int) { TODO() }

    enum class Tabs { Profile, Tests, Settings }
    private var selectedTab = Tabs.Profile

    private val fragment1 by lazy { MainFragment() }
    private val fragment2 by lazy { MainTestsFragment() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = ""
        supportActionBar?.hide()
        mBottomNavigation.visibility = View.INVISIBLE

        if (savedInstanceState == null) {
            replaceFragment(fragment1)
        }
        UserData.didChange = {
            if (it == UserData.UserType.Student)
                mBottomNavigation.visibility = View.VISIBLE
            else {
                mBottomNavigation.visibility = View.INVISIBLE

            }
        }

        val layoutParams = mBottomNavigation.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationBehavior()

        mBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_profile -> {
                    if (selectedTab == Tabs.Profile) return@setOnNavigationItemSelectedListener false
                    selectedTab = Tabs.Profile

                    replaceFragment(fragment1)
                }
                R.id.action_test -> {
                    if (selectedTab == Tabs.Tests) return@setOnNavigationItemSelectedListener false
                    selectedTab = Tabs.Tests

                    replaceFragment(fragment2)
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

}