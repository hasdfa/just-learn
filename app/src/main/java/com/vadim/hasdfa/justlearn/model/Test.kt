package com.vadim.hasdfa.justlearn.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.utils.Statuses
import com.vadim.hasdfa.justlearn.utils.SubjectId
import com.vadim.hasdfa.justlearn.utils.readQuestion

/**
 * Model object for represent test
 *
 * Has parcelable implementation
 *
 *
 * @see {https://kotlinlang.org/docs/reference/data-classes.html}
 */
@IgnoreExtraProperties
data class Test(
        /**
         * UUID generated for each unique test
         */
        var uniqueID: String = "",

        /**
         * User-friendly name for test
         */
        var title: String = "",

        /**
         * User group (topic)
         */
        var groups: MutableList<String> = mutableListOf(),

        /**
         * Will be used
         */
        var testStatus: Statuses = Statuses.Unknown,

        /**
         * Has one theme from {@link com.vadim.hasdfa.justlearn.utils.FormThemes}
         */
        var themeId: Int = -1,

        /**
         * List of questions
         */
        var questions: MutableList<QBase> = mutableListOf(),

        /**
         * Some test categories
         *
         * @see {@link com.vadim.hasdfa.justlearn.utils.SubjectId}
         */
        var subjectId: SubjectId = SubjectId.Undefined,

        /**
         * Show that is this tests a ZNO
         */
        var isZno: Boolean = false
): Parcelable {

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel?): this() {
        parcel?.let {
            uniqueID = parcel.readString()
            title = parcel.readString()
            parcel.readStringList(groups)
            testStatus = Statuses.valueOf(parcel.readString())
            themeId = parcel.readInt()
            subjectId = SubjectId.valueOf(parcel.readString())

            val typeList = mutableListOf<Int>()
            parcel.readList(typeList, ClassLoader.getSystemClassLoader())

            questions = mutableListOf()
            typeList.forEach {
                questions.add(parcel.readQuestion(it))
            }
        }
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(p: Parcel?, p1: Int) {
        p?.let {
            it.writeString(uniqueID)
            it.writeString(title)
            it.writeStringList(groups)
            it.writeString(testStatus.name)
            it.writeInt(themeId)
            it.writeString(subjectId.name)

            it.writeList(questions.map { QBase.getPositionByQ(it) })
            questions.forEach { q ->
                it.writeParcelable(q, 0)
            }
        }
    }

    /**
     * Parcelable implementation
     */
    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable implementation
     */
    companion object CREATOR: Parcelable.Creator<Test> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel) = Test(parcel)

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int) = arrayOfNulls<Test?>(size)
    }
}