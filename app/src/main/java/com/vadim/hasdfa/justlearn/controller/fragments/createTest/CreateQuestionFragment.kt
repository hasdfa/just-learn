package com.vadim.hasdfa.justlearn.controller.fragments.createTest

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import com.vadim.hasdfa.justlearn.model.questionBuilderHelper.QuestionView
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import kotlinx.android.synthetic.main.fragment_create_question.*

/**
* Created by Raksha Vadim on 10.08.17, 00:10.
*/
class CreateQuestionFragment: FragmentBase() {
    private var mGLManager: GridLayoutManager? = null
    var mAdapter: QuestionAdapter? = null

    private var dataList: ArrayList<QuestionView> = ArrayList()
    private lateinit var newQuestion: QBase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_create_question, container, false)
        val question = arguments?.getParcelable<QBase>("q")
        if (question == null || question.type == QType.Null) {
            activity?.finish()
            return null
        } else if (question.question.isNotEmpty() && question.helpText.isNotEmpty()) {
            val list = mutableListOf<QuestionView>()
            list += QuestionView(str(R.string.question_content), question.question)

            list += when (question) {
                is QDropdown -> question.items.mapIndexed { index, it -> QuestionView("${str(R.string.dropdown_item)} #$index", it) }
                is QRadioButtons -> question.items.mapIndexed { index, it -> QuestionView("${str(R.string.radio_item)} #$index", it) }
                is QCheckbox -> question.items.mapIndexed { index, it -> QuestionView("${str(R.string.checkbox_item)} #$index", it) }
                else -> emptyList()
            }

            list += QuestionView(str(R.string.help_text), question.helpText)

            dataList = ArrayList(list)
        }
        newQuestion = question
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val columns = if (isBigTablet) 2 else 1
        mGLManager = GridLayoutManager(context, columns)
        mRecyclerView.layoutManager = mGLManager

        if (dataList.isEmpty())
            dataList = QuestionView.addBaseData(context(), newQuestion.type)
        mAdapter = QuestionAdapter()
        mRecyclerView.adapter = mAdapter
    }

    fun getData(): QBase {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity?.window?.decorView?.rootView?.windowToken, 0)

        with (newQuestion) {
            question = dataList.first().value
            helpText = dataList.last().value

            when (type) {
                QType.Text -> return QText().also {
                    it.question = question
                    it.helpText = helpText
                }
                QType.TextArea -> return QTextArea().also {
                    it.question = question
                    it.helpText = helpText
                }
                QType.Dropdown -> {
                    val q = QDropdown().also {
                        it.question = question
                        it.helpText = helpText
                    }
                    for (i in 1 until dataList.size - 2)
                        q.items.add(dataList[i].value)

                    return q
                }
                QType.RadioBtn -> {
                    val q = QRadioButtons().also {
                        it.question = question
                        it.helpText = helpText
                    }
                    for (i in 1 until dataList.size - 2)
                        q.items.add(dataList[i].value)

                    return q
                }
                QType.Checkbox -> {
                    val q = QCheckbox().also {
                        it.question = question
                        it.helpText = helpText
                    }
                    for (i in 1 until dataList.size - 2)
                        q.items.add(dataList[i].value)

                    return q
                }
                else -> throw NotImplementedError()
            }
        }
    }


    inner class QuestionAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            fun createView(@LayoutRes layout: Int): View {
                return inflater.inflate(layout, parent, false)
            }

            return when (viewType) {
                ViewTypes.default -> DefaultViewHolder(createView(R.layout.item_question_default))
                ViewTypes.extended -> ExtendedItemViewHolder(createView(R.layout.item_question_extended))
                ViewTypes.addItem -> AddItemViewHolder(createView(R.layout.item_question_add))
                else -> throw Exception("Error when inflating question field")
            }
        }

        private val bindException = Exception("Error when bind question field")
        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val viewType = getItemViewType(position)
            if (viewType == ViewTypes.default) {
                val qView = dataList[position]
                if (holder is DefaultViewHolder) {
                    holder.contentInputLayout.hint = qView.headerText
                    if (qView.value.isNotEmpty())
                        holder.contentEditText.setText(qView.value)

                    holder.contentEditText.addTextChangedListener(object: TextWatcher {
                        override fun afterTextChanged(text: Editable?) {}
                        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                            dataList[holder.adapterPosition].value = p0.toString()
                        }
                    })
                } else throw bindException
            } else if (viewType == ViewTypes.extended) {
                val qView = dataList[position]
                if (holder is ExtendedItemViewHolder) {
                    holder.contentInputLayout.hint = qView.headerText + " #$position"

                    if (qView.value.isNotEmpty()) {
                        holder.contentEditText.setText(qView.value)
                    }
                    holder.contentEditText.addTextChangedListener(object: TextWatcher {
                        override fun afterTextChanged(text: Editable?) {}
                        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                            dataList[holder.adapterPosition].value = p0.toString()
                        }
                    })
                    if (holder.adapterPosition == 1 && dataList.size == 4) {
                        with (holder.removeItemImageView) {
                            isEnabled = false
                            setImageDrawable(drawable(R.drawable.ic_remove_uneanabled))
                        }
                    } else {
                        with (holder.removeItemImageView) {
                            isEnabled = true
                            setImageDrawable(drawable(R.drawable.ic_remove))
                        }
                    }
                    holder.removeItemImageView.setOnClickListener {
                        val pos = holder.adapterPosition
                        dataList.removeAt(pos)
                        notifyItemRemoved(pos)
                        notifyItemRangeChanged(pos - 1, dataList.size)
                    }
                } else throw bindException
            } else if (viewType == ViewTypes.addItem) {
                if (holder is AddItemViewHolder)
                    holder.addCardView.setOnClickListener {
                        mAdapter?.let {
                            val item = when {
                                newQuestion.type == QType.Dropdown -> context().resources.getString(R.string.dropdown_item)
                                newQuestion.type == QType.RadioBtn -> context().resources.getString(R.string.radio_item)
                                newQuestion.type == QType.Checkbox -> context().resources.getString(R.string.checkbox_item)
                                else -> throw NotImplementedError()
                            }

                            val pos = holder.adapterPosition
                            dataList.add(pos, QuestionView(item))
                            notifyItemInserted(pos)
                            notifyItemRangeChanged(pos-1, dataList.size)
                        }
                    }
                else throw bindException
            } else throw bindException
        }

        override fun getItemViewType(position: Int): Int {
            if (position == 0 || dataList.size-1 == position) {
                return ViewTypes.default
            }
            with(newQuestion) {
                if (position == dataList.size-2 && (
                        type == QType.Dropdown || type == QType.RadioBtn || type == QType.Checkbox))
                    return ViewTypes.addItem

                return when (type) {
                    QType.Text     -> ViewTypes.default
                    QType.TextArea -> ViewTypes.default
                    QType.Dropdown -> ViewTypes.extended
                    QType.RadioBtn -> ViewTypes.extended
                    QType.Checkbox -> ViewTypes.extended
                    else           -> throw Exception("Error when getting view type")
                }
            }
        }

        override fun getItemCount(): Int = dataList.size

        inner class DefaultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val contentInputLayout: TextInputLayout = itemView.findViewById(R.id.contentInputLayout)
            val contentEditText: TextInputEditText = itemView.findViewById(R.id.contentEditText)
        }
        inner class ExtendedItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val contentInputLayout: TextInputLayout = itemView.findViewById(R.id.contentInputLayout)
            val contentEditText: TextInputEditText = itemView.findViewById(R.id.contentEditText)
            var removeItemImageView: ImageView = itemView.findViewById(R.id.removeItem)
        }
        inner class AddItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val addCardView: CardView = itemView.findViewById(R.id.addCardView)
        }
    }
    private object ViewTypes {
        const val default = 0
        const val extended = 1
        const val addItem = 2
    }


    // On Restore Instance
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            dataList = it.getParcelableArrayList("dataList")
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("dataList", dataList)
    }
}