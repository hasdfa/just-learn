package com.vadim.hasdfa.justlearn.controller.adapters.mainActivity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.checkTests.CheckTestActivity
import com.vadim.hasdfa.justlearn.controller.activity.profile.ProfileActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Teacher
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.FDatabaseChildEventListener
import java.util.*

/**
* Created by Raksha Vadim on 07.08.17, 23:51.
*/

open class TeacherAdapter(private var teacher: Teacher, val onError: (String) -> Unit): RecyclerView.Adapter<TeacherAdapter.ViewHolder>() {

    private var mCommentKeys: ArrayList<String> = ArrayList()

    private fun Int.color(context: Context?)
            = if (context != null) ContextCompat.getColor(context, this)
    else Color.WHITE

    init {
        FirebaseDatabaseHelper.getTeachersTest(object: FDatabaseChildEventListener<Test> {
            override fun onCancelled(error: DatabaseError?) { error?.toException()?.printStackTrace() }
            override fun onChildAdded(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                teacher.testCreated.add(0, result)
                mCommentKeys.add(0, dataSnapshot.key)

                notifyItemInserted(1)
                notifyItemRangeChanged(0, teacher.testCreated.lastIndex)
            }
            override fun onChildChanged(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                val pos = mCommentKeys.indexOf(previousChildName)

                if (pos > -1) {
                    teacher.testCreated[pos] = result
                    mCommentKeys[pos] = dataSnapshot.key
                    notifyItemChanged(pos+1)
                    notifyItemRangeChanged(pos+1, teacher.testCreated.lastIndex)
                } else
                    notifyDataSetChanged()
            }
            override fun onChildRemoved(result: Test, dataSnapshot: DataSnapshot) {
                val pos = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos > -1) {
                    teacher.testCreated.removeAt(pos)
                    mCommentKeys.removeAt(pos)
                    notifyItemRemoved(pos + 1)
                    notifyItemChanged(0)
                } else Log.e("myLog", "ERROR")
            }
            override fun onChildMoved(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                val pos1 = mCommentKeys.indexOf(previousChildName)
                val pos2 = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos1 == pos2) return

                if (pos1 > -1 && pos2 > -1) {
                    Collections.swap(teacher.testCreated, pos1, pos2)
                    Collections.swap(mCommentKeys, pos1, pos2)

                    if (pos1 < pos2)
                        notifyItemMoved(pos1 + 1, pos2 + 1)
                    else
                        notifyItemMoved(pos2 + 1, pos1 + 1)
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflate: (Int) -> ViewHolder = {
            ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(it, parent, false))
        }
        return if (viewType == 1)
            inflate(R.layout.profile_teacher_item)
        else
            inflate(R.layout.test_short_item)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == 0) {
            with(teacher) {
                holder.profileNameTextView?.text = name
                holder.profileTestsCreated?.text = "${testCreated.size}"
            }

            holder.profileSettingsImageView?.setColorFilter(R.color.colorSecondaryText.color(holder.itemView.context))
            holder.profileSettingsImageView?.setOnClickListener {
                val context = holder.profileNameTextView?.context
                context?.startActivity(Intent(context, ProfileActivity::class.java))
            }
        } else {
            val test = teacher.testCreated[position - 1]

            holder.testTitle?.text = test.title
            holder.testTopic?.text = Arrays.toString(test.groups.toTypedArray())

            holder.testStartButton!!.isEnabled = true
            holder.testStartButton!!.text = holder.testStartButton?.context?.resources?.getString(R.string.check)
            holder.testStartButton!!.setTextColor(Color.parseColor("#607D8B"))

            holder.testStartButton!!.setOnClickListener {
                Log.d("myLog", "Tapped: ${test.uniqueID}")
                with(it.context as ActivityBase) {
                    val intent = Intent(this, CheckTestActivity::class.java)
                    intent.putExtra("selectedTest", test)

                    startActivity(intent)
                }
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 1
            else -> 2
        }
    }

    override fun getItemCount(): Int {
        return teacher.testCreated.size + 1
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val profileNameTextView: TextView? = itemView.findViewById(R.id.nameTextView)
        val profileTestsCreated: TextView? = itemView.findViewById(R.id.testsCreatedCount)
        val profileSettingsImageView: ImageView? = itemView.findViewById(R.id.settingsImageView)

        var testTitle: TextView? = itemView.findViewById(R.id.testTitle)
        var testTopic: TextView? = itemView.findViewById(R.id.topicTextView)
        var testStatus: TextView? = itemView.findViewById(R.id.testStatus)
        var testStartButton: Button? = itemView.findViewById(R.id.btnStartTest)
    }

}