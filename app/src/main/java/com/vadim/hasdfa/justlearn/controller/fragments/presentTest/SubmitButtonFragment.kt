package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.AppCompatButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.submit_button.*

/**
* Created by Raksha Vadim on 28.08.17.
*/
class SubmitButtonFragment: QuestionFragmentBase() {

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        throw NotImplementedError("Must not implements")
    }

    var isButtonEnabled: Boolean = false
    set(value) {
        field = value

        if (submitButton == null) return
        submitButton.isEnabled = value

        FormThemes.getThemes().getOrNull(themeId)?.let {
            if (value) {
                submitButton.setTextColor(it.buttonTextColor)
                submitButton.setSupportBackgroundTint(ColorStateList.valueOf(it.buttonColor))
            } else {
                submitButton.setTextColor(it.progressbarFillColor)
                submitButton.setSupportBackgroundTint(ColorStateList.valueOf(it.progressbarColor))
            }
        }
    }

    private fun AppCompatButton.setSupportBackgroundTint(list: ColorStateList) {
        ViewCompat.setBackgroundTintList(this, list)
    }


    interface QuestionSubmitDataCallback {
        fun onSubmitPressed()
    }

    var mCallback: QuestionSubmitDataCallback? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.submit_button, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        submitButton.setOnClickListener {
            mCallback?.onSubmitPressed()
        }
        isButtonEnabled = isButtonEnabled
    }
}