//package com.vadim.hasdfa.justlearn.controller.adapters.testAdapter
//
//import android.os.Handler
//import android.os.Looper
//import android.support.v7.widget.RecyclerView
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.TextView
//import com.vadim.hasdfa.justlearn.R
//import com.vadim.hasdfa.justlearn.model.Answer
//import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
//import com.vadim.hasdfa.justlearn.model.Test
//import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
//
///**
// * Created by Raksha Vadim on 05.12.2017.
// * With love.
// * In project JustLearn.
// */
//class CheckStudentAnswersAdapter(testUID: String, var onError: (String?) -> Unit): RecyclerView.Adapter<CheckStudentAnswersAdapter.ViewHolder>() {
//
//    var data: FirebaseAnswer = emptyList()
//    var test: Test = Test()
//
//    init {
//        FirebaseDatabaseHelper.getStudentAnswers(testUID, onError) {
//            data = it
//            notifyAfterLoadIfNeed()
//        }
//        FirebaseDatabaseHelper.getTestsById(testUID, "", onError) {
//            test = it
//            notifyAfterLoadIfNeed()
//        }
//    }
//
//    private fun notifyAfterLoadIfNeed() {
//        Handler(Looper.getMainLooper()).post {
//            if (test.questions.count() != data.count() && data.count() != 0 && test.questions.count() != 0) {
//                onError("Invalid count of tests: ${test.questions.size} and data: ${data.size}")
//            }
//            notifyDataSetChanged()
//        }
//    }
//
//    override fun getItemCount(): Int {
//        return minOf(data.size, test.questions.size)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
////        val answer = data[position]
////        val question = test.questions[position]
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(
//                LayoutInflater.from(parent.context)
//                    .inflate(R.layout.test_short_item, parent, false)
//        )
//    }
//
//    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
//        var testTitle: TextView? = itemView.findViewById(R.id.testTitle)
//    }
//
//}