package com.vadim.hasdfa.justlearn.controller.activity.browseTest

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.CardView
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.fragments.presentTest.*
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.activity_present_test.*
import java.util.*
import kotlin.collections.ArrayList

/**
* Created by Raksha Vadim on 12.08.17, 01:42.
*/
class ShowTestActivity: ActivityBase() {

    var test: Test = Test()

    private var testTheme: FormThemes.ThemeBase? = null

    private val imm by lazy {
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private var submitted = hashMapOf<Int, Boolean>()

    private val path by lazy {
        intent.getStringExtra("path") ?: throw NoSuchElementException()
    }
    private val mAdapter by lazy {
        ViewPageAdapter(supportFragmentManager)
    }


    fun onSubmit(b: Boolean, i: Int, function: (Boolean) -> Unit) {
        submitted[i] = b

        val progress = submitted.values.count { it }
        progressBar.progress = progress

        Log.d("myLog", "onSubmit[$i] = $b")
        function(progress == progressBar.max)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_present_test)

        supportActionBar?.hide()

        setEnabled(upArrowCardView, false)
        setEnabled(downArrowCardView, false)


        mViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        setEnabled(upArrowCardView, false)
                        setEnabled(downArrowCardView, true)
                    }
                    (mViewPager.adapter?.count ?: 0) - 1 -> {
                        setEnabled(downArrowCardView, false)
                        setEnabled(upArrowCardView, true)
                    }
                    else -> {
                        setEnabled(upArrowCardView, true)
                        setEnabled(downArrowCardView, true)
                    }
                }
            }
        })

        upArrowImageView.setOnClickListener {
            imm.hideSoftInputFromWindow(mainBack.windowToken, 0)

            val item = mViewPager.currentItem - 1

            if (item >= 0) {
                mViewPager.post {
                    mViewPager.setCurrentItem(item, true)
                }
            }
        }
        downArrowImageView.setOnClickListener {
            imm.hideSoftInputFromWindow(mainBack.windowToken, 0)

            val item = mViewPager.currentItem + 1

            if (item <= mAdapter.count - 1) {
                mViewPager.post {
                    mViewPager.setCurrentItem(item, true)
                }
            }
        }

        closeTest.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        showAlert(str(R.string.leave_without_saving), onOk = {
            super.onBackPressed()
            supportFinishAfterTransition()
        }, onCancel = {
            it.dismiss()
        })
    }

    override fun onStart() {
        super.onStart()
        FirebaseDatabaseHelper.getTest4Students(path, {
            it?.let {
                test = it
                test.uniqueID = path.split("/").last()

                progressBar.max = test.questions.size
                progressBar.progress = 0

                if (test.themeId >= 0) {
                    testTheme = FormThemes.getThemes()[test.themeId]
                    testTheme?.let {
                        mainBack.setBackgroundColor(it.backgroundColor)
                        divider.setBackgroundColor(it.colorPrimary)


                        progressBar.progressDrawable
                                .setColorFilter(it.progressbarFillColor, android.graphics.PorterDuff.Mode.MULTIPLY)

                        upArrowImageView.setBackgroundColor(it.buttonColor)
                        downArrowImageView.setBackgroundColor(it.buttonColor)

                        upArrowImageView.setColorFilter(it.buttonTextColor)
                        downArrowImageView.setColorFilter(it.buttonTextColor)

                        closeTest.setColorFilter(it.buttonColor)

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                            window.statusBarColor = testTheme?.backgroundColor ?: Color.WHITE
                        }
                    }
                }

                setEnabled(upArrowCardView, false)
                setEnabled(downArrowCardView, true)

                mViewPager.adapter = mAdapter
            }
        }) { showError(it) { finish() } }
    }

    private fun setEnabled(cardView: CardView, boolean: Boolean){
        if (cardView.isEnabled == boolean) return

        cardView.isEnabled = boolean

        var drawable: ColorDrawable? = ColorDrawable()
        if (!boolean) {
            drawable?.color = Color.WHITE
            drawable?.alpha = 175
        } else {
            drawable = null
        }

        cardView.foreground = drawable
    }


    private val checkAndSaveResults: (List<Pair<Answer, Error?>>) -> Unit = {
        // check answers
        val answers = ArrayList<Answer>()
        var isAllGood = true
        var errorString: String? = null

        val showError: (String?) -> Unit = {
            runOnUiThread {
                load(false)
                showError(it, {
                    finish()
                })
            }
        }

        it.forEach { (answer, error) ->
            if(error != null) {
                isAllGood = false
                errorString = error.message
                return@forEach
            }
            if (answer.answerText.isNullOrEmpty() && answer.answerArray == null && answer.answerArray?.isEmpty() == true) {
                isAllGood = false
                return@forEach
            }

            answers += answer
        }
        if (isAllGood) {
            load(true)
            FirebaseDatabaseHelper.saveTestResults(FirebaseAnswer.create(answers), test.uniqueID, {
                // Success
                runOnUiThread {
                    load(false)
                    showAlert(str(R.string.answer_saved), onOk = {
                        finish()
                    }, onDismiss = {
                        finish()
                    })
                }
            }, {
                showError(it.message)
            })
        } else {
            showError(errorString)
        }
    }

    inner class ViewPageAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
        val fragmentsMap = LinkedHashMap<Int, QuestionFragmentBase>()
        private var submitFragment: SubmitButtonFragment? = null

        override fun getItem(position: Int): QuestionFragmentBase {

            // Submit button fragment
            if (position == test.questions.size) {
                val fragment = SubmitButtonFragment()
                fragment.arguments = Bundle().also {
                    it.putInt("theme", test.themeId)
                }
                fragment.mCallback = object: SubmitButtonFragment.QuestionSubmitDataCallback {
                    override fun onSubmitPressed() {
                        checkAndSaveResults(fragmentsMap.map { it.value.getData() })
                    }
                }
                submitFragment = fragment
                return fragment
            }

            val q = test.questions[position]
            val fragment: QuestionFragmentBase
            fragment = when (q.type) {
                QType.Text     -> TextFragment()
                QType.TextArea -> TextAreaFragment()
                QType.Dropdown -> DropdownFragment()
                QType.RadioBtn -> RadiobuttonsFragment()
                QType.Checkbox -> CheckboxesFragment()
                QType.Grid     -> TODO()
                QType.Stack    -> TODO()
                QType.Null     -> throw Exception("Unexpected Question Type: Null")
            }
            fragment.onSubmitAction = {
                onSubmit(it, position) {
                    submitFragment?.isButtonEnabled = it
                }
            }
            fragment.arguments = Bundle().also {
                it.putParcelable("q", q)
                it.putInt("theme", test.themeId)
            }

            fragmentsMap[position] = fragment
            return fragment
        }

        override fun getCount(): Int = test.questions.size + 1
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let {
            it.putParcelable("test", test)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            test = it.getParcelable("test")
        }
    }

}
