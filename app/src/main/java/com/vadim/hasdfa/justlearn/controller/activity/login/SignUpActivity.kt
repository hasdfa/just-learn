package com.vadim.hasdfa.justlearn.controller.activity.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.MainActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Teacher
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : ActivityBase() {

    var selectedTopic = ""
    val array: Array<String> by lazy { resources.getStringArray(R.array.topics) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        supportActionBar?.hide()
        actionBar?.hide()

        selectedTopic = array[0]
        configureSpinner()

        submitButton.setOnClickListener {
            load(true)
            checkData {
                createUser()
            }
        }

        signInButton.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out)
            finish()
        }

        loading.visibility = View.INVISIBLE
    }

    private fun configureSpinner(){
        spinnerSelectTopic.adapter = ArrayAdapter<String>(
                this,
                R.layout.spinner_item,
                R.id.spinner_item_text_view,
                array
        )
        change1Item()


        spinnerSelectTopic.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                change1Item()
            }
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedTopic = array[position]

                change1Item()
            }
        }
    }

    private fun change1Item() {
        var textColor = Color.WHITE
        var backColor = colorPrimary
        if (selectedTopic == array[0]) {
            textColor = Color.DKGRAY
            backColor = colorPrimaryLight
        }
        spinnerSelectTopic
                .findViewById<TextView?>(R.id.spinner_item_text_view)
                ?.setTextColor(textColor)
        spinnerSelectTopic
                .findViewById<RelativeLayout?>(R.id.backRL)
                ?.setBackgroundColor(backColor)
    }

    private fun createUser(){
        FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(
                        emailEditText.text.toString(),
                        passwordEditText.text.toString()
                ).addOnCompleteListener {
            if (it.isComplete && it.isSuccessful) {
                // Configure user
                val displayName = nameEditText.text.toString()

                val user = it.result.user

                val child: String
                val value: Any
                if (selectedTopic == array[1]) {
                    val teacher = Teacher()
                    teacher.email = user.email ?: ""
                    teacher.name = displayName
                    teacher.isAccepted = false

                    value = teacher
                    child = "teachers"
                } else {
                    val studentObj = Student()
                    studentObj.uid = user.uid
                    studentObj.email = user.email ?: ""
                    studentObj.name = displayName
                    studentObj.topic = selectedTopic
                    studentObj.topic = selectedTopic

                    value = studentObj
                    child = "students"
                }
                user.sendEmailVerification().addOnCompleteListener {
                    if (!it.isComplete && !it.isSuccessful) {
                        it.exception?.printStackTrace()
                    }
                }

                FirebaseDatabase.getInstance()
                        .reference
                        .child(child)
                        .child(user.uid)
                        .setValue(value)

                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                it.exception?.let {
                    it.printStackTrace()
                    showError(it.message, {load(false)})
                }
            }
            load(false)
        }
    }

    private fun checkData(function: () -> Unit) {
        var isCorrect = true
        var errorDescription = ""


        if (selectedTopic == array[0]) {
            isCorrect = false
            errorDescription = str(R.string.must_select_correct_topic)
        }
        if (selectedTopic.isEmpty()) {
            isCorrect = false
            errorDescription = str(R.string.must_select_topic)
        }
        if (passwordEditText.text.toString().length < 6) {
            isCorrect = false
            errorDescription = str(R.string.password_m_containts_more_than_5)
        }
        if (passwordEditText.text.toString().isEmpty()) {
            isCorrect = false
            errorDescription = str(R.string.pasword_is_empty)
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches()) {
            isCorrect = false
            errorDescription = str(R.string.email_is_invalid)
        }
        if (nameEditText.text.toString().split(" ").size != 2) {
            isCorrect = false
            errorDescription = str(R.string.incorrect_name)
        }
        if (nameEditText.text.toString().isEmpty()) {
            isCorrect = false
            errorDescription = str(R.string.empty_name)
        }

        if (isCorrect) {
            function()
        } else {
            showError(errorDescription, {load(false)})
        }
    }

    override fun load(isLoading: Boolean) {
        super.load(isLoading)
        submitButton.isEnabled = !isLoading
        signInButton.isEnabled = !isLoading
    }


    private val userNameKey = "uName"
    private val emailKey = "email"
    private val passwordKey = "pass"
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.let {
            it.putString(userNameKey, nameEditText.text.toString())
            it.putString(emailKey, emailEditText.text.toString())
            it.putString(passwordKey, passwordEditText.text.toString())
        }
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            nameEditText.setText(it.getString(userNameKey)    ?: "")
            emailEditText.setText(it.getString(emailKey)      ?: "")
            passwordEditText.setText(it.getString(passwordKey) ?: "")
        }
    }
}
