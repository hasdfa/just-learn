package com.vadim.hasdfa.justlearn.controller.adapters.testAdapter

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.view.ViewCompat
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.widget.AppCompatCheckBox
import android.support.v7.widget.AppCompatRadioButton
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.RadioGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.createTest.CreateQuestionActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.BaseFunctions
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import com.vadim.hasdfa.justlearn.utils.Statuses
import com.vadim.hasdfa.justlearn.view.CheckBoxGroup

/**
* Created by Raksha Vadim on 12.08.17, 01:39.
* With love.
* In project JustLearn.
*/
class TestAdapter(private var test: Test, val onChange: (Boolean) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && test.testStatus == Statuses.Editing)
            return ViewTypes.header // header

        val q: QBase = if (test.testStatus == Statuses.Editing) test.questions[position-1] else test.questions[position-1]

        return when (q.type) {
            QType.Text     -> ViewTypes.text
            QType.TextArea -> ViewTypes.textArea
            QType.Dropdown -> ViewTypes.dropdown
            QType.RadioBtn -> ViewTypes.radio
            QType.Checkbox -> ViewTypes.checkbox
            else -> throw Exception("Type not found")
        }
    }

    private fun createViewCreator(parent: ViewGroup?): (Int) -> View {
        return fun (@LayoutRes l: Int): View {
            return LayoutInflater.from(parent?.context).inflate(l, parent, false)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val createView = createViewCreator(parent)

        return when (viewType) {
            ViewTypes.header -> HeaderViewHolder(createView(R.layout.question_preview_header))
            ViewTypes.text -> TextViewHolder(createView(R.layout.question_text))
            ViewTypes.textArea -> TextAreaViewHolder(createView(R.layout.question_text_area))
            ViewTypes.dropdown -> DropdownViewHolder(createView(R.layout.question_dropdown))
            ViewTypes.radio -> RadioBtnViewHolder(createView(R.layout.question_radiobtns))
            ViewTypes.checkbox -> CheckboxViewHolder(createView(R.layout.question_checkboxes))
            else -> throw ClassNotFoundException("Type not found")
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val context = holder.itemView.context as ActivityBase

        if (position == 0 && test.testStatus == Statuses.Editing) {
            if (holder is HeaderViewHolder) {
                if (test.themeId >= 0) {
                    FormThemes.getThemes()[test.themeId].let {
                        holder.titleView.setTextColor(it.questionColor)
                    }
                }
            }
            return
        }

        val question: QBase = if (test.testStatus == Statuses.Editing) test.questions[position-1] else test.questions[position-1]
        fun styleCardView() {
            (holder.itemView as? CardView)?.let {
                it.setCardBackgroundColor(Color.alpha(0))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.elevation = 0f
                }
                it.radius = 0f
            }
        }
        if (test.testStatus == Statuses.Editing) {
            holder.itemView.findViewById<ImageView>(R.id.changeImageView).setOnClickListener {
                val intent = Intent(context, CreateQuestionActivity::class.java)
                val bundle = Bundle()
                bundle.putParcelable("q", question)
                intent.putExtras(bundle)
                context.startActivityForResult(intent, BaseFunctions.IntentCodes.REQUEST_GET_Q)
            }
            holder.itemView.findViewById<ImageView>(R.id.removeImageView).setOnClickListener {
                val index = test.questions.indexOfFirst { it.id == question.id }
                val add = if (test.testStatus == Statuses.Editing) 1 else 0
                if (index >= 0) {
                    test.questions.removeAt(index)
                    if (test.questions.size > 0) {
                        notifyItemRemoved(index + add)
                    } else {
                        notifyItemRangeRemoved(0, 2)
                    }
                }
            }
        }

        if (holder is TextViewHolder) {
            val q = question as QText
            holder.headerTextView.hint = q.question
            holder.helperTextView.text = q.helpText

            holder.contentTextView.requestFocus()
            if (test.themeId >= 0) {
                FormThemes.getThemes()[test.themeId].let {
                    holder.contentTextView.setTextColor(it.answerColor)
                    holder.helperTextView.setTextColor(it.questionColor)
                    holder.contentTextView.setHintTextColor(it.questionColor)
                    setUpperHintColor(holder.headerTextView, it.questionColor)

                    ViewCompat.setBackgroundTintList(holder.contentTextView, ColorStateList.valueOf(it.questionColor))
                    styleCardView()
                }
            }
        } else if (holder is TextAreaViewHolder) {
            val q = question as QTextArea
            holder.headerTextView.hint = q.question
            holder.helperTextView.text = q.helpText
            holder.contentTextView.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(editable: Editable?) {
                    editable?.let {
                        holder.symbolsTextView.text = "${it.length}"
                    }
                }
            })
            holder.contentTextView.clearFocus()
            if (test.themeId >= 0) {
                FormThemes.getThemes()[test.themeId].let {
                    holder.contentTextView.setTextColor(it.answerColor)
                    holder.helperTextView.setTextColor(it.questionColor)
                    holder.contentTextView.setHintTextColor(it.questionColor)
                    setUpperHintColor(holder.headerTextView, it.questionColor)
                    holder.symbolsTextView.setTextColor(it.answerColor)
                    holder.divTextView.setTextColor(it.answerColor)
                    holder.maxSymbolsTextView.setTextColor(it.answerColor)

                    ViewCompat.setBackgroundTintList(holder.contentTextView, ColorStateList.valueOf(it.questionColor))
                    styleCardView()
                }
            }
        } else if (holder is DropdownViewHolder) {
            val q = question as QDropdown
            holder.headerTextView.text = q.question
            configureSpinner(holder.dropdown, q.items, {})
            holder.helperTextView.text = q.helpText
            if (test.themeId >= 0) {
                FormThemes.getThemes()[test.themeId].let {
                    holder.helperTextView.setTextColor(it.questionColor)
                    holder.headerTextView.setTextColor(it.questionColor)
                    styleCardView()
                }
            }
        } else if (holder is RadioBtnViewHolder) {
            val q = question as QRadioButtons
            holder.headerTextView.text = q.question
            holder.helperTextView.text = q.helpText

            var theme: FormThemes.ThemeBase? = null
            if (test.themeId >= 0) {
                FormThemes.getThemes()[test.themeId].let {
                    holder.helperTextView.setTextColor(it.questionColor)
                    holder.headerTextView.setTextColor(it.questionColor)
                    styleCardView()
                    theme = it
                }
            }
            holder.radioGroup.removeAllViews()
            for (i in 0 until q.items.size) {
                val radioButton = AppCompatRadioButton(holder.itemView?.context)
                radioButton.text = q.items[i]
                radioButton.id = i
                radioButton.textSize = context.resources.getDimension(R.dimen.answer_text_size)
                val rParams = RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT)
                holder.radioGroup.addView(radioButton, rParams)
            }
            holder.radioGroup.setTheme(theme)
        } else if (holder is CheckboxViewHolder) {
            val q = question as QCheckbox
            var theme: FormThemes.ThemeBase? = null

            holder.headerTextView.text = q.question
            holder.helperTextView.text = q.helpText
            if (test.themeId >= 0) {
                FormThemes.getThemes()[test.themeId].let {
                    holder.helperTextView.setTextColor(it.questionColor)
                    holder.headerTextView.setTextColor(it.questionColor)
                    styleCardView()
                    theme = it
                }
            }
            holder.checkboxGroup.removeAllViews()
            for (i in 0 until q.items.size) {
                val checkBox = AppCompatCheckBox(holder.itemView?.context)
                checkBox.text = q.items[i]
                checkBox.id = i
                checkBox.textSize = context.resources.getDimension(R.dimen.answer_text_size)
                holder.checkboxGroup.addCheckBox(checkBox)
            }
            holder.checkboxGroup.setTheme(theme)
        } else throw Exception("Holder child wasn't found")
    }

    private var _bounded = 0

    override fun getItemCount(): Int {
        val size = test.questions.size
        val count = if (size != 0 && test.testStatus == Statuses.Editing) {
            size + 1
        } else
            size

        if (count != _bounded) {
            onChange(count != 0)
            _bounded = count
        }

        return count
    }

    inner class TextViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val headerTextView: TextInputLayout = itemView.findViewById(R.id.headerTextView)
        val contentTextView: TextInputEditText = itemView.findViewById(R.id.contentTextView)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class TextAreaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val headerTextView: TextInputLayout = itemView.findViewById(R.id.headerTextView)
        val contentTextView: TextInputEditText = itemView.findViewById(R.id.contentTextView)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
        val symbolsTextView: TextView = itemView.findViewById(R.id.counterTextView)
        val divTextView: TextView = itemView.findViewById(R.id.divTextView)
        val maxSymbolsTextView: TextView = itemView.findViewById(R.id.maxSymbolsTextView)
    }

    inner class DropdownViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val dropdown: Spinner = itemView.findViewById(R.id.dropdown)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class RadioBtnViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val radioGroup: RadioGroup = itemView.findViewById(R.id.radioGroup)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class CheckboxViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val headerTextView: TextView = itemView.findViewById(R.id.headerTextView)
        val checkboxGroup: CheckBoxGroup = itemView.findViewById(R.id.checkboxGroup)
        val helperTextView: TextView = itemView.findViewById(R.id.helperTextView)
    }

    inner class HeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val titleView: TextView = itemView.findViewById(R.id.titleTextView)
    }

    private object ViewTypes {
        const val text = 0
        const val textArea = 1
        const val dropdown = 2
        const val radio = 3
        const val checkbox = 4
        const val header = -1
    }

    private fun configureSpinner(spinnerSelectTopic: Spinner, array: List<String>, onItemSelected: (Int) -> Unit){
        spinnerSelectTopic.adapter = ArrayAdapter<String>(
                spinnerSelectTopic.context,
                R.layout.spinner_item,
                R.id.spinner_item_text_view,
                array
        )

        colorizeSpinnerItem(spinnerSelectTopic)
        spinnerSelectTopic.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                colorizeSpinnerItem(spinnerSelectTopic)
            }
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onItemSelected(position)
                colorizeSpinnerItem(spinnerSelectTopic)
            }
        }
    }

    private fun colorizeSpinnerItem(spinnerSelectTopic: Spinner){
        var textColor = Color.BLACK
        var backColor = Color.WHITE
        if (test.themeId >= 0)
            FormThemes.getThemes()[test.themeId].let {
                textColor = it.answerColor
                backColor = it.backgroundColor
            }

        spinnerSelectTopic
                .findViewById<TextView?>(R.id.spinner_item_text_view)
                ?.setTextColor(textColor)
        spinnerSelectTopic
                .findViewById<RelativeLayout?>(R.id.backRL)
                ?.setBackgroundColor(backColor)
    }

    fun notifyDataSetChanged(newTest: Test) {
        this.test = newTest
        notifyDataSetChanged()
    }


    private fun RadioGroup.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return
        (0 until childCount)
                .map { this.getChildAt(it) }
                .filterIsInstance<AppCompatRadioButton>()
                .forEach {
                    it.setTextColor(theme.answerColor)

                    val colorStateList = ColorStateList(
                            arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                            intArrayOf(theme.questionColor, theme.questionColor)
                    )
                    CompoundButtonCompat.setButtonTintList(it, colorStateList)

                    it.invalidate()
                }
    }
    private fun CheckBoxGroup.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return
        (0 until childCount)
                .map { this.getChildAt(it) }
                .filterIsInstance<AppCompatCheckBox>()
                .forEach {
                    it.setTextColor(theme.answerColor)

                    val colorStateList = ColorStateList(
                            arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                            intArrayOf(theme.questionColor, theme.questionColor)
                    )
                    CompoundButtonCompat.setButtonTintList(it, colorStateList)

                    it.invalidate()
                }
    }

    private fun setUpperHintColor(textInputLayout: TextInputLayout, color: Int) {
        try {
            val field = textInputLayout::class.java.getDeclaredField("mFocusedTextColor")
            field.isAccessible = true
            val states = arrayOf(intArrayOf())
            val colors = intArrayOf(color)
            val myList = ColorStateList(states, colors)
            field.set(textInputLayout, myList)

            val method = textInputLayout::class.java.getDeclaredMethod("updateLabelState", Boolean::class.javaPrimitiveType)
            method.isAccessible = true
            method.invoke(textInputLayout, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}