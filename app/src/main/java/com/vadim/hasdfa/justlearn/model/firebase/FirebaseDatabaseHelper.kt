package com.vadim.hasdfa.justlearn.model.firebase

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.*
import com.vadim.hasdfa.justlearn.core.Application.Companion.runOnMainThread
import com.vadim.hasdfa.justlearn.model.*
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FDatabaseChildEventListener
import com.vadim.hasdfa.justlearn.utils.Statuses
import com.vadim.hasdfa.justlearn.utils.readFirebaseTest
import java.util.*
import kotlin.NoSuchElementException
import kotlin.concurrent.thread

/**
 * Object, which contains method to connect to database
 * and send/receive data
 */
object FirebaseDatabaseHelper {

    /**
     * Map of listeners as values, and paths in database as keys
     */
    private val mListenerMap: HashMap<String, Any> = hashMapOf()

    /**
     * Put listener in mListenerMap and add it to Firebase database
     */
    private fun<V> HashMap<String, V>.putDBListener(key: String, value: V): V? {
        val db = FirebaseDatabase.getInstance().reference

        if (contains(key)) {
            val listener = this[key]
            when (listener) {
                is ValueEventListener -> db.removeEventListener(listener)
                is ChildEventListener -> db.removeEventListener(listener)
            }
            remove(key)
        }
        when (value) {
            is ValueEventListener -> db.child(key).addValueEventListener(value)
            is ChildEventListener -> db.child(key).addChildEventListener(value)
        }

        return put(key, value)
    }

    /**
     * Find all listeners in map, determines the type
     * and remove it from map and from database
     *
     * @return true if map does not exists all listeners
     */
    internal fun clearAllListeners(): Boolean {
        val db = FirebaseDatabase.getInstance().reference
        val startCount = mListenerMap.count()

        for ((key, value) in mListenerMap) {
            when (value) {
                is ValueEventListener -> db.removeEventListener(value)
                is ChildEventListener -> db.removeEventListener(value)
                else -> throw NoSuchElementException("$key: $value")
            }
        }
        return (mListenerMap.count() == 0 && startCount > 0).also {
            if (it) mListenerMap.forEach { Log.d("myLog", "ERROR: ${it.key}") }
        }
    }

    /**
     * Remove all listeners for path
     *
     * @param path Path to collection in database
     * @return true if map does not exists listeners with this path
     */
    private fun clearListenersFrom(path: String): Boolean {
        val db = FirebaseDatabase.getInstance().reference

        for ((key, value) in mListenerMap.filter { it.key.startsWith(path) }) {
            when (value) {
                is ValueEventListener -> db.removeEventListener(value)
                is ChildEventListener -> db.removeEventListener(value)
                else -> throw NoSuchElementException("$key: $value")
            }
            mListenerMap.remove(key)
        }
        return mListenerMap.count { it.key.startsWith(path) } == 0
    }


    /**
     * Save array of answers, to results/${testUID}/${studentUID}
     * and add, that test was sent to student with status
     */
    fun saveTestResults(answer: FirebaseAnswer, testUUID: String, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        thread {
            val db = FirebaseDatabase.getInstance().reference
            val user = FirebaseAuth.getInstance().currentUser

            answer.answers = answer.answers.map {
                val question = it.first.question
                when (question) {
                    is QDropdown -> {
                        val v = if (it.first.answerText == question.answer) question._value else "0"
                        FirebaseAnswer.Companion.CustomPair(it.first, v)
                    }
                    is QRadioButtons -> {
                        val v = if (it.first.answerText == question.answer) question._value else "0"
                        FirebaseAnswer.Companion.CustomPair(it.first, v)
                    }
                    is QCheckbox -> {
                        val good = question.answers
                        val userAnswers = it.first.answerArray ?: mutableListOf()
                        val mark = ((good.size - good.subtract(userAnswers).size).toDouble() / good.size.toDouble()).toBigDecimal() * question._value!!.toBigDecimal()

                        FirebaseAnswer.Companion.CustomPair(it.first, mark.toString())
                    }
                    else -> it
                }
            }.toMutableList()

            if (answer.answers.all { it.second != null }) {
                answer.status = Statuses.Checked
            }
            db.child("results")
                    .child(testUUID)
                    .child(user?.uid)
                    .setValue(answer)
                    .addOnCompleteListener {
                        runOnMainThread {
                            if (it.isSuccessful)
                                onSuccess()
                            else
                                it.exception?.let(onFailure)
                        }
                    }
        }
    }


    /**
     * Save array of answers, to results/${testUID}/${studentUID}
     * and add, that test was sent to student with status
     */
    fun saveResults(answer: FirebaseAnswer, student: Student, testUUID: String, onSuccess: () -> Unit, onFailure: (Exception) -> Unit) {
        thread {
            val db = FirebaseDatabase.getInstance().reference

            answer.status = Statuses.Checked

            db.child("results")
                    .child(testUUID)
                    .child(student.uid)
                    .setValue(answer)
                    .addOnCompleteListener {
                        runOnMainThread {
                            if (it.isSuccessful)
                                onSuccess()
                            else
                                it.exception?.let(onFailure)
                        }
                    }
        }
    }


    fun checkIfTestExists(student: Student, test: Test, hasChild: (Statuses) -> Unit, onFailure: (String?) -> Unit) {
        thread {
            val listener = object: ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) { onFailure(p0?.message) }
                override fun onDataChange(p0: DataSnapshot?) {
                    hasChild(Statuses.valueOf(p0?.value?.toString() ?: "Enabled"))
                }
            }
            FirebaseAuth.getInstance().currentUser?.uid?.let {
                mListenerMap.putDBListener("results/${test.uniqueID}/$it/status", listener)
            }
        }
    }

    fun postNewTest(t: Test, functionSuccess: () -> Unit, functionFailure: (String?) -> Unit) {
        thread {
            val db = FirebaseDatabase.getInstance().reference

            val teacherUID = FirebaseAuth.getInstance().currentUser?.uid ?: ""
            val firebaseTest = FirebaseTest(t.title, teacherUID, t.themeId, t.questions, t.groups, t.subjectId)

            val testUniqueID = UUID.randomUUID().toString()

            val ref = db.child("tests")
                    .child(testUniqueID)
                    .setValue(firebaseTest)
            ref.addOnSuccessListener {
                runOnMainThread(functionSuccess)
            }.addOnFailureListener {
                runOnMainThread {
                    functionFailure(it.message)
                }
            }
        }
    }

    fun getTests4Students(student: Student, childEventListener: FDatabaseChildEventListener<Test>) {

        val listener = object : ChildEventListener {
            val added = mutableListOf<String>()

            override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                val test = dataSnapshot?.readFirebaseTest()?.test(dataSnapshot.key)
                if (test != null && student.topic in test.groups) {
                    childEventListener.onChildMoved(test, dataSnapshot, previousChildName)
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                val test = dataSnapshot?.readFirebaseTest()?.test(dataSnapshot.key)
                if (test != null && student.topic in test.groups) {
                    if (test.uniqueID in added)
                        childEventListener.onChildChanged(test, dataSnapshot, previousChildName)
                    else
                        onChildAdded(dataSnapshot, previousChildName)
                } else if (test?.uniqueID in added) {
                    onChildRemoved(dataSnapshot)
                }
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                val test = dataSnapshot?.readFirebaseTest()?.test(dataSnapshot.key)
                if (test != null && student.topic in test.groups) {
                    childEventListener.onChildAdded(test, dataSnapshot, previousChildName)
                    added += test.uniqueID
                }
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                val test = dataSnapshot?.readFirebaseTest()?.test(dataSnapshot.key)
                if (test != null && test.uniqueID in added) {
                    childEventListener.onChildRemoved(test, dataSnapshot)
                    added -= test.uniqueID
                }
            }

            override fun onCancelled(error: DatabaseError?) {
                childEventListener.onCancelled(error)
            }
        }
        mListenerMap.putDBListener("tests", listener)
    }

    fun getTest4Students(testRef: String, functionSuccess: (Test?) -> Unit, functionFailure: (String?) -> Unit) {
        thread {
            val listener = object: ValueEventListener{
                override fun onCancelled(databaseError: DatabaseError?) { functionFailure(databaseError?.toException()?.message) }
                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.let {
                        testByDataSnapshot(it, {
                            runOnMainThread { functionSuccess(it) }
                        })
                    }
                }
            }

            mListenerMap.putDBListener("tests/$testRef", listener)
        }
    }

    fun getTestsById(testId: String, group: String, onFailure: (String?) -> Unit, onSuccess: (Test) -> Unit) {
        val listener = object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) { onFailure(error?.toException()?.message) }
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot != null) {
                    testByDataSnapshot(dataSnapshot, {
                        if (it != null) onSuccess(it)
                        else onFailure("Test is null")
                    })
                } else {
                    onFailure("Wrong test")
                }
            }
        }

        mListenerMap.putDBListener("tests/$group/$testId", listener)
    }

    fun testByDataSnapshot(dataSnapshot: DataSnapshot, functionSuccess: (Test?) -> Unit) {
        thread {
            var test: Test? = null
            dataSnapshot.getValue(Test::class.java)?.let {
                test = it

                test?.let { t ->
                    val get = getValueFast(dataSnapshot)

                    var i = -1
                    val parsedQuestions = t.questions.map {
                        i++
                        when (t.questions[i].type) {
                            QType.Text     -> get(i, QText::class.java)
                            QType.TextArea -> get(i, QTextArea::class.java)
                            QType.Dropdown -> get(i, QDropdown::class.java)
                            QType.RadioBtn -> get(i, QRadioButtons::class.java)
                            QType.Checkbox -> get(i, QCheckbox::class.java)
                            QType.Grid     -> null
                            QType.Stack    -> null
                            QType.Null     -> throw Exception("Unknown type")
                        }
                    }
                    t.questions = parsedQuestions.filterNotNull().toMutableList()
                }
            }
            functionSuccess(test)
        }
    }

    private fun getValueFast(dS: DataSnapshot): (Int, Class<out QBase>) -> QBase {
        return fun(i: Int, c: Class<out QBase>): QBase =
                dS.child("questions").child("$i").getValue(c) ?: throw Exception("Unknown type")
    }

    fun getTeachersTest(childEventListener: FDatabaseChildEventListener<Test>)  {
        val teacherUid = FirebaseAuth.getInstance().currentUser?.uid ?: ""

        val childWrapperListener = object: ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) { childEventListener.onCancelled(p0) }
            override fun onChildMoved(dataSnapshot: DataSnapshot?, p1: String?) {
                val test = dataSnapshot?.readFirebaseTest()
                if (test?.teacherUID == teacherUid)
                    childEventListener.onChildMoved(test.test(uniqueID = dataSnapshot.key), dataSnapshot, p1)
            }
            override fun onChildChanged(dataSnapshot: DataSnapshot?, p1: String?) {
                val test = dataSnapshot?.readFirebaseTest()
                if (test?.teacherUID == teacherUid)
                    childEventListener.onChildChanged(test.test(uniqueID = dataSnapshot.key), dataSnapshot, p1)
            }
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                val test = dataSnapshot?.readFirebaseTest()
                if (test?.teacherUID == teacherUid)
                    childEventListener.onChildAdded(test.test(uniqueID = dataSnapshot.key), dataSnapshot, p1)
            }
            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                val test = dataSnapshot?.readFirebaseTest()
                if (test?.teacherUID == teacherUid)
                    childEventListener.onChildRemoved(test.test(uniqueID = dataSnapshot.key), dataSnapshot)
            }
        }
        mListenerMap.putDBListener("tests", childWrapperListener)
    }

    fun getUserData(user: FirebaseUser, userComplete: (Student) -> Unit, teacherComplete: (Teacher) -> Unit, nothing: () -> Unit) {
        thread {
            val listener = object: ValueEventListener {
                override fun onCancelled(databaseError: DatabaseError?) {
                    databaseError?.toException()?.printStackTrace()
                }
                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    val student = dataSnapshot?.getValue(Student::class.java)
                    if (student != null) {
                        checkName(user, student.name)
                        runOnMainThread { userComplete(student) }
                    } else {
                        val listener2 = object: ValueEventListener {
                            override fun onDataChange(studentDataSnapshot: DataSnapshot?) {
                                val teacher = studentDataSnapshot?.getValue(Teacher::class.java)
                                if (teacher != null) {
                                    checkName(user, teacher.name)
                                    runOnMainThread { teacherComplete(teacher) }
                                } else {
                                    runOnMainThread(nothing)
                                }
                            }
                            override fun onCancelled(databaseError: DatabaseError?) {
                                databaseError?.toException()?.printStackTrace()
                            }
                        }
                        mListenerMap.putDBListener("teachers/${user.uid}", listener2)
                    }
                }
            }

            mListenerMap.putDBListener("students/${user.uid}", listener)
        }
    }

    internal fun getCount(path: String, onResult: (Int) -> Unit) {
        val onResultWrapper: (Int) -> Unit = {
            onResult(it)
            clearListenersFrom(path)
        }

        val listener = object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                onResultWrapper(-1)
            }
            override fun onDataChange(p0: DataSnapshot) {
                onResultWrapper(p0.children.count())
            }
        }
        mListenerMap.putDBListener(path, listener)
    }

    internal fun getTestsCount(uniqueId: String, onResult: (Int) -> Unit) {
        getCount("results/$uniqueId", onResult)
    }

    internal fun getStudentsWhichAnswers(uniqueId: String, childEventListener: ChildEventListener) {
        mListenerMap.putDBListener("results/$uniqueId", childEventListener)
    }

    internal fun getStudentAnswers(testUUD: String, student: Student, onFailure: (String?) -> Unit, onSuccess: (FirebaseAnswer) -> Unit) {
        val listener = object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) { onFailure(error?.toException()?.message) }
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val data = dataSnapshot?.getValue(FirebaseAnswer::class.java)
                if (data != null) {
                    data.let(onSuccess)
                } else {
                    onFailure("Answers list is null")
                }
                clearListenersFrom("results/$testUUD/${student.uid}")
            }
        }

        mListenerMap.putDBListener("results/$testUUD/${student.uid}", listener)
    }

    internal fun getStudentByKey(uud: String, onSuccess: (Student) -> Unit) {
        val listener = object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                dataSnapshot?.let {
                    mListenerMap.remove("students/$uud")

                    val student = dataSnapshot.getValue(Student::class.java) ?: return
                    onSuccess(student)
                }
            }
        }

        mListenerMap.putDBListener("students/$uud", listener)
    }


    private fun getTestsUIDForGroup(group: String, onFailure: (String?) -> Unit, onSuccess: (List<String>) -> Unit) {
        val listener = object: ValueEventListener {
            override fun onCancelled(error: DatabaseError?) { onFailure(error?.message) }
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                dataSnapshot?.let {
                    val testIdList = (dataSnapshot.value as? Map<*, *>)?.map { it.key.toString() } ?: emptyList()
                    onSuccess(testIdList)
                }
            }
        }

        mListenerMap.putDBListener("groups/$group/tests", listener)
    }

    private fun checkName(user: FirebaseUser, name: String) {
        if (user.displayName != name) {
            user.updateProfile(UserProfileChangeRequest
                    .Builder()
                    .setDisplayName(name)
                    .build()
            )
        }
    }
}