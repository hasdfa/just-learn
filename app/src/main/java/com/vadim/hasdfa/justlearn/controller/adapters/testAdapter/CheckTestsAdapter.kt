package com.vadim.hasdfa.justlearn.controller.adapters.testAdapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.Statuses
import java.util.*

/**
 * Created by Raksha Vadim on 05.12.2017.
 * With love.
 * In project JustLearn.
 */

typealias StudentWithId = Triple<String, Student, Statuses>

class CheckTestsAdapter(val test: Test, var onError: (String?) -> Unit, val onSelect: (Student, Statuses) -> Unit): RecyclerView.Adapter<CheckTestsAdapter.ViewHolder>() {

    var onLoaded: (() -> Unit)? = null

    val data: ArrayList<StudentWithId> = arrayListOf()

    private infix fun ArrayList<Triple<String, Student, Statuses>>.positionOf(key: String?): Int
            = indexOfFirst { it.first == key }

    init {
        FirebaseDatabaseHelper.getTestsCount(test.uniqueID) {
            if (it <= 0)
                onError("На цей тест ще ніхто не відповів")
        }

        FirebaseDatabaseHelper.getStudentsWhichAnswers(test.uniqueID, object: ChildEventListener {
            override fun onCancelled(error: DatabaseError?) { onError(error?.message) }
            override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                dataSnapshot?.let {
                    with(this@CheckTestsAdapter) {
                        Log.d("myLog", "onChildMoved:$previousChildName")

                        val pos1 = data positionOf previousChildName
                        val pos2 = data positionOf dataSnapshot.key
                        if (pos1 == pos2 || pos1 == -1 || pos2 == 1) return

                        Collections.swap(data, pos1, pos2)
                        notifyItemMoved(pos1 + 1, pos2 + 1)
                    }
                }
            }
            override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                dataSnapshot?.let {
                    val answer = it.getValue(FirebaseAnswer::class.java)
                    val key = it.key

                    val pos = when(previousChildName) {
                        null -> data.size
                        else -> data positionOf previousChildName
                    } - 1

                    FirebaseDatabaseHelper.getStudentByKey(key) {
                        data[pos] = Triple(key, it, answer?.status ?: Statuses.Unknown)
                        notifyItemChanged(pos + 1)
                    }
                }
            }
            override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
                dataSnapshot?.let {
                    val answer = it.getValue(FirebaseAnswer::class.java)

                    val key = it.key
                    FirebaseDatabaseHelper.getStudentByKey(key) {
                        data.add(0, Triple(key, it, answer?.status ?: Statuses.Unknown))

                        notifyItemInserted(1)

                        onLoaded?.invoke().also { onLoaded = null }
                    }
                }
            }
            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                dataSnapshot?.let {
                    val pos = data positionOf dataSnapshot.key
                    if (pos > -1) {
                        data.removeAt(pos)
                        notifyItemRemoved(pos + 1)
                    }
                }
            }
        })
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (_, student, status) = data[position]

        with(holder) {
            title.text = student.name

            if (status == Statuses.Checked) {
                button.text = holder.view.context.resources.getString(R.string.review)
            } else
                button.text = holder.view.context.resources.getString(R.string.check)

            subTitle.visibility = View.INVISIBLE
            subSubTitle.visibility = View.INVISIBLE

            button.isEnabled = true
            button.setTextColor(ContextCompat.getColor(button.context, R.color.colorGreen))
            button.setOnClickListener {
                onSelect(student, status)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.test_short_item, parent, false)
        )
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        var title: TextView = itemView.findViewById(R.id.testTitle)
        var subTitle: TextView = itemView.findViewById(R.id.topicTextView)
        var subSubTitle: TextView = itemView.findViewById(R.id.testStatus)
        var button: Button = itemView.findViewById(R.id.btnStartTest)
    }

}