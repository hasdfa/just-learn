package com.vadim.hasdfa.justlearn.utils

import com.vadim.hasdfa.justlearn.R
import java.util.*

/**
 * Enum with all test categories
 */
enum class SubjectId {
    English, Geometry, Algebra, Programming, Informatics, Physics,
    UkraineLiterature, UkraineLanguage, WorldLiterature,
    HistoryOfUkraine, WorldHistory, ScienceOfLaw,
    Chemistry, Biology,

    /**
     * If type does not exists
     */
    Undefined;

    val drawableId
        get() = when (this) {
                SubjectId.English -> R.drawable.ic_english
                SubjectId.Geometry -> R.drawable.ic_geometry
                SubjectId.Algebra -> R.drawable.ic_algebra
                SubjectId.Programming -> R.drawable.ic_prog
                SubjectId.Informatics -> R.drawable.ic_it
                SubjectId.Physics -> R.drawable.ic_physics
                SubjectId.UkraineLiterature -> R.drawable.ic_ukr_lit
                SubjectId.UkraineLanguage -> R.drawable.ic_ukr_lang
                SubjectId.WorldLiterature -> R.drawable.ic_world_lit
                SubjectId.HistoryOfUkraine -> R.drawable.ic_ukr_history
                SubjectId.WorldHistory -> R.drawable.ic_world_history
                SubjectId.ScienceOfLaw -> R.drawable.ic_law
                SubjectId.Chemistry -> R.drawable.ic_subject
                SubjectId.Biology -> R.drawable.ic_subject
                SubjectId.Undefined -> R.drawable.ic_subject
            }

    val localeName: String
        get() = when (Locale.getDefault().language) {
            "ru" -> {
                when(this) {
                    SubjectId.English -> "Английский язык"
                    SubjectId.Geometry -> "Геометрия"
                    SubjectId.Algebra -> "Алгебра"
                    SubjectId.Programming -> "Программирование"
                    SubjectId.Informatics -> "Информатика"
                    SubjectId.Physics -> "Физика"
                    SubjectId.UkraineLiterature -> "Украинская литература"
                    SubjectId.UkraineLanguage -> "Украинский язык"
                    SubjectId.WorldLiterature -> "Зарубежная литература"
                    SubjectId.HistoryOfUkraine -> "История Украины"
                    SubjectId.WorldHistory -> "Всемирная история"
                    SubjectId.ScienceOfLaw -> "Право"
                    SubjectId.Chemistry -> "Химия"
                    SubjectId.Biology -> "Биология"
                    SubjectId.Undefined -> "Неизвестно"
                }
            }
            "uk" -> {
                when(this) {
                    SubjectId.English -> "Англійська мова"
                    SubjectId.Geometry -> "Геометрія"
                    SubjectId.Algebra -> "Алгебра"
                    SubjectId.Programming -> "Програмування"
                    SubjectId.Informatics -> "Інформатика"
                    SubjectId.Physics -> "Фізика"
                    SubjectId.UkraineLiterature -> "Українська література"
                    SubjectId.UkraineLanguage -> "Українська мова"
                    SubjectId.WorldLiterature -> "Світова література"
                    SubjectId.HistoryOfUkraine -> "Істория України"
                    SubjectId.WorldHistory -> "Всесвітня історія"
                    SubjectId.ScienceOfLaw -> "Право"
                    SubjectId.Chemistry -> "Хімія"
                    SubjectId.Biology -> "Біологія"
                    SubjectId.Undefined -> "Невідомо"
                }
            }
            else -> {
                when (this) {
                    Undefined -> "Subject"
                    else -> name
                }
            }
        }
}