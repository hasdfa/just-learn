package com.vadim.hasdfa.justlearn.controller.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.vadim.hasdfa.justlearn.controller.activity.login.SignInActivity
import com.vadim.hasdfa.justlearn.controller.activity.login.SignUpActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase

/**
* Created by Raksha Vadim on 05.08.17, 20:16.
* With love.
* In project JustLearn.
*/

class SplashScreen: ActivityBase() {
    private val spKey = "start"
    private val spBoolKey = "is_first"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firstOpen { isFirstOpen ->
            if (isFirstOpen) FirebaseDatabase.getInstance().setPersistenceEnabled(true)
            val nextActivity = when {
                FirebaseAuth.getInstance().currentUser != null
                             -> MainActivity::class.java
                isFirstOpen  -> SignUpActivity::class.java
                else         -> SignInActivity::class.java
            }

            startActivity(Intent(this, nextActivity))
            supportFinishAfterTransition()
        }
    }

    private fun firstOpen(function: (Boolean) -> Unit) {
        var isFirst = false
        val sp = getSharedPreferences(spKey, Context.MODE_PRIVATE)
        if (sp.getBoolean(spBoolKey, true)) {
            isFirst = true
            sp.edit().putBoolean(spBoolKey, false).apply()
        }
        function(isFirst)
    }
}