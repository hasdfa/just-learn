package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.BaseFunctions.IntentCodes
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.utils.SubjectId
import kotlinx.android.synthetic.main.activity_test_settings.*
import java.util.*

/**
 * Created by Raksha Vadim on 15.03.2018.
 * With love.
 * In project JustLearn.
 */
class TestSettingsActivity: ActivityBase() {

    val test by lazy {
        intent.getParcelableExtra<Test>("test")
    }

    val mRecyclerView by lazy {
        val rv = RecyclerView(this)
        rv.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT)
        rv.layoutManager = LinearLayoutManager(this)
        return@lazy rv
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_settings)

        title = str(R.string.test_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        changeTestData.setOnClickListener {
            startActivityForResult(
                    Intent(this, PreviewCreateTestActivity::class.java)
                            .putExtra("test", test),
                    IntentCodes.REQUEST_GET_T
            )
        }
        val values = SubjectId.values().toMutableList()
        Collections.swap(values, 0, values.indexOf(SubjectId.Undefined))

        val aAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values.map { it.localeName })
        selectSubjectDropdown.adapter = aAdapter

        if (test.subjectId != SubjectId.Undefined) {
            selectSubjectDropdown.setSelection(values.indexOf(test.subjectId))
        } else {
            selectSubjectDropdown.setSelection(values.indexOf(SubjectId.Undefined))
        }

        selectSubjectDropdown.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {  }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                test.subjectId = values[position]
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK, Intent().putExtra("test", test))
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IntentCodes.REQUEST_GET_T && data != null && data.hasExtra("test")) {
            val test = data.getParcelableExtra<Test>("test")
            this.test.title = test.title
            this.test.groups = test.groups
        } else showError(str(R.string.could_not_change_test))
    }

}