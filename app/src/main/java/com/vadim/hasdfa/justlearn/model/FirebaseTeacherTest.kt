package com.vadim.hasdfa.justlearn.model

import com.google.firebase.database.IgnoreExtraProperties

/**
 * Model object for represent test for firebase database
 *
 * @see {@link https://kotlinlang.org/docs/reference/data-classes.html}
 * @see {@link com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper}
 */
@IgnoreExtraProperties
data class FirebaseTeacherTest(
        /**
         * User-friendly name for test
         */
        var title: String,

        /**
         * User group (topic)
         */
        var topics: List<String>,

        /**
         * Date, when this test wac created or modified
         */
        var creationDate: Long,

        /**
         * Some test categories
         *
         * @see {@link com.vadim.hasdfa.justlearn.model.SubjectIds}
         */
        var subjectId: String
)