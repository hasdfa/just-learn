package com.vadim.hasdfa.justlearn.model

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.utils.SubjectId
import com.vadim.hasdfa.justlearn.utils.readQuestion
import java.util.*

/**
 * Model object for represent test for firebase database
 *
 * @see {@link com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper}
 */
data class FirebaseTest(
        /**
         * User-friendly name for test
         */
        var title: String = "",

        /**
         * Teacher UID related with teacher account
         */
        var teacherUID: String = "",

        /**
         * Has one theme from {@link com.vadim.hasdfa.justlearn.utils.FormThemes}
         */
        var themeId: Int = -1,

        /**
         * List of question, which contains this test
         */
        var questions: MutableList<QBase> = mutableListOf(),

        /**
         * List of groups, which is main for this test
         */
        var groups: MutableList<String> = mutableListOf(),

        /**
         * Some test categories
         *
         * @see {@link com.vadim.hasdfa.justlearn.utils.SubjectId}
         */
        var subjectId: SubjectId = SubjectId.Undefined,

        /**
         * Date, when this test wac created or modified
         */
        var creationDate: Date = Date(),

        /**
         * Show that is this tests a ZNO
         */
        var isZno: Boolean = false
): Parcelable {
    fun test(uniqueID: String) = Test(
            uniqueID = uniqueID,
            title = title,
            themeId = themeId,
            questions = questions.toMutableList(),
            groups = groups,
            subjectId = subjectId,
            isZno = isZno
    )

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel?): this() {
        parcel?.let {
            title = parcel.readString()
            teacherUID = parcel.readString()
            themeId = parcel.readInt()

            val typeList = mutableListOf<Int>()
            parcel.readList(typeList, ClassLoader.getSystemClassLoader())

            questions = mutableListOf()
            typeList.forEach {
                questions.add(parcel.readQuestion(it))
            }
        }
        Log.d("myLog", questions.toString())
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(p: Parcel?, p1: Int) {
        p?.let {
            it.writeString(title)
            it.writeString(teacherUID)
            it.writeInt(themeId)

            it.writeList(questions.map { QBase.getPositionByQ(it) })
            questions.forEach { q ->
                it.writeParcelable(q, 0)
            }
        }
    }

    /**
     * Parcelable implementation
     */
    override fun describeContents() = 0

    /**
     * Parcelable implementation
     */
    companion object CREATOR: Parcelable.Creator<FirebaseTest> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel) = FirebaseTest(parcel)

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int) = arrayOfNulls<FirebaseTest?>(size)
    }
}