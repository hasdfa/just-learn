package com.vadim.hasdfa.justlearn.model.questions

import android.os.Parcel
import android.os.Parcelable
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType

/**
 * Question type, which describes
 * question view presentation as text box
 * witch must contains from 3 to 50 symbols
 *
 * Has parcelable implementation from superclass
 *
 * @see {@link com.vadim.hasdfa.justlearn.model.questions.utils.QBase}
 */
class QText: QBase{
    /**
     * Realize of abstract field type
     */
    override var type = QType.Text

    /**
     * Call superclass`s constructor
     */
    constructor(): super()

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel): super(parcel)

    /**
     * Parcelable implementation
     */
    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable implementation
     */
    companion object CREATOR: Parcelable.Creator<QText> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): QText {
            return QText(parcel)
        }

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<QText?> {
            return arrayOfNulls(size)
        }
    }
}