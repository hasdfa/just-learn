package com.vadim.hasdfa.justlearn.view

import android.app.Activity
import android.support.v4.widget.ContentLoadingProgressBar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.vadim.hasdfa.justlearn.R

/**
* Created by hasdfa on 23.01.2018.
*/
object LoadingPreview {

    fun addToParentOf(activity: Activity): View {
        val root = activity.window.decorView.findViewById<ViewGroup>(android.R.id.content)
        for (i in 0 until root.childCount) {
            val view = root.getChildAt(i)
            if (view is RelativeLayout && view.getChildAt(0) is ContentLoadingProgressBar) {
                root.removeView(view)
                break
            }
        }
        val relativeLayout = LayoutInflater.from(activity).inflate(R.layout.progress, root, false)
        root.addView(relativeLayout)
        return relativeLayout
    }

}