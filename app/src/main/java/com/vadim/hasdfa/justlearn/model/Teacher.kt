package com.vadim.hasdfa.justlearn.model

/**
 * Model object for represent teacher
 *
 * {@value #email} email Email of teacher
 * {@value #name} name Teacher username
 * {@value #isAccepted} isAccepted Is allow to create tests for these teacher
 * {@value #testCreated} testCreated list of created questions
 *
 * @see {@link https://kotlinlang.org/docs/reference/data-classes.html}
 */
data class Teacher(
        /**
         * Email of teacher
         */
        var email: String = "",

        /**
         * Teacher username
         */
        var name: String = "",

        /**
         * Is allow to create tests for these teacher
         */
        var isAccepted: Boolean = false,

        /**
         * List of created questions
         */
        var testCreated: MutableList<Test> = mutableListOf()
)