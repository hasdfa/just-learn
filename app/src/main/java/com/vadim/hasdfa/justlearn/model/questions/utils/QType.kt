package com.vadim.hasdfa.justlearn.model.questions.utils

/**
 * Enum class< which describes all question types
 */
enum class QType {
    Text, TextArea, Dropdown, RadioBtn, Checkbox, Grid, Stack,

    /**
     * If type does not exists
     */
    Null
}