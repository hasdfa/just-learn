package com.vadim.hasdfa.justlearn.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

/**
 * Model object for represent student
 *
 * @see {https://kotlinlang.org/docs/reference/data-classes.html}
 */
@IgnoreExtraProperties
data class Student(
        @Exclude
        var uid: String = "",

        /**
         * Email of student
         */
        var email: String = "",

        /**
         * Student username
         */
        var name: String = "",

        /**
         * User group (topic)
         */
        var topic: String = "",

        /**
         * List of sent tests
         */
        var tests: MutableList<Test> = mutableListOf()
) : Parcelable {
        constructor(parcel: Parcel): this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readTestsArrayList())

        /**
         * Parcelable implementation
         */
        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(uid)
                parcel.writeString(email)
                parcel.writeString(name)
                parcel.writeString(topic)
                parcel.writeTypedList(tests)
        }

        /**
         * Parcelable implementation
         */
        override fun describeContents(): Int = 0

        /**
         * Parcelable implementation
         */
        companion object CREATOR : Parcelable.Creator<Student> {
                /**
                 * Parcelable implementation
                 */
                override fun createFromParcel(parcel: Parcel): Student {
                        return Student(parcel)
                }

                /**
                 * Parcelable implementation
                 */
                override fun newArray(size: Int): Array<Student?> {
                        return arrayOfNulls(size)
                }

                /**
                 * Parcelable implementation
                 */
                private fun Parcel.readTestsArrayList(): ArrayList<Test> {
                        val arrayList = ArrayList<Test>()
                        this.readTypedList(arrayList, Test.CREATOR)
                        return arrayList
                }
        }
}