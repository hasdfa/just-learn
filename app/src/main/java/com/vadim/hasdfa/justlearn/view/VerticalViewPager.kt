package com.vadim.hasdfa.justlearn.view

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

/**
 * Uses a combination of a PageTransformer and swapping X & Y coordinates
 * of touch events to create the illusion of a vertically scrolling ViewPager.
 *
 * Requires API 11+
 *
 * @author Brett
 */
class VerticalViewPager: ViewPager {

    constructor(context: Context): super(context)
    constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet)

    /**
     * The majority of the magic happens here
     */
    init {
        setPageTransformer(true, VerticalPageTransformer())
        overScrollMode = View.OVER_SCROLL_NEVER
    }

    /**
     * Inner class, which present {@link android.support.v4.view.ViewPager},
     * pages in vertical direction
     */
    private inner class VerticalPageTransformer: ViewPager.PageTransformer {

        /**
         * Method, which setups transformations for each page.
         * Namely:
         * <li>
         *     <ul>Counteract the default slide transition</ul>
         *     <ul>Set Y position to swipe in from top</ul>
         *     <ul>Fade (0.5f) pages, which is quite visible</ul>
         * </li>
         *
         * @param view page view
         * @param position page position in {@link android.support.v4.view.ViewPager}
         */
        override fun transformPage(view: View, position: Float) {
            val posAverage = 4

            // [-posAverage,posAverage]
            if (position <= posAverage || position > -posAverage) {
                val alpha = when {
                    position == 0f -> 1f
                    position < 0f -> 1f + (position / posAverage)
                    else -> 1f - (position / posAverage)
                }

                view.alpha = alpha

                view.translationX = view.width * -position

                val yPosition = position * view.height
                view.translationY = yPosition

            } else // (-Infinity,-5]&[5,+Infinity]
                view.alpha = 0f
        }
    }

    /**
     * Swaps the X and Y coordinates of your touch event.
     *
     * @param ev motion event, in which will be replaced coordinates
     */
//    private fun swapXY(ev: MotionEvent): MotionEvent {
//        val width = width.toFloat()
//        val height = height.toFloat()
//
//        val newX = (ev.y / height * width)
//        val newY = (ev.x / width * height)
//
//        ev.setLocation(newX, newY)
//
//        return ev
//    }

    /**
     * Parent method of {@link android.support.v4.view.ViewPager},
     * which allows to enable or disable swipe to change page
     *
     * @param event
     * @return false
     */
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean = false

    /**
     * Parent method of {@link android.support.v4.view.ViewPager},
     * that allows to enable or disable swipe to change page
     *
     * @param event
     * @return false
     */
    override fun onInterceptTouchEvent(event: MotionEvent): Boolean = false
}