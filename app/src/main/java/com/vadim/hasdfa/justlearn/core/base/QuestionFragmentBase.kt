package com.vadim.hasdfa.justlearn.core.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase

/**
* Created by Raksha Vadim on 14.08.17.
*/
abstract class QuestionFragmentBase: FragmentBase() {
    /**
     * Base callback function, which must be realized in all children
     */
    abstract fun getData(): Pair<Answer, Error?>

    /**
     * Base answer, which must hold every QuestionFragmentBase
     */
    protected var answer: Answer = Answer()

    /**
     * Callback on submit action from holder
     */
    var onSubmitAction: (Boolean) -> Unit = { throw NotImplementedError() }

    /**
     * Calls, when UI changed
     */
    fun submit() {
        onSubmitAction(getData().second == null)
    }

    protected fun onCreateView(view: View?): View? = view

    /**
     * Must be passed from holder
     */
    protected val mQuestion: QBase by lazy {
        arguments!!.getParcelable<QBase>("q")
    }

    /**
     * Must be passed from holder
     */
    protected val themeId: Int by lazy {
        arguments!!.getInt("theme", -1)
    }

    /**
     * Base method for onViewCreated()
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        answer.question = mQuestion
        answer.type = mQuestion.type

        view.findViewById<View>(R.id.editMenu)?.let {
            it.visibility = View.INVISIBLE
        }
    }

    /**
     * Lifecycle method, which save answer in bundle
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("saved-answer", answer)
    }

    /**
     * Lifecycle method, which restore answer from bundle
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            answer = it.getParcelable("saved-answer")
        }
    }

    fun showMore() {
        when (mQuestion) {
            is QText -> showQuestionDialog()
            is QTextArea -> showQuestionDialog()
            is QDropdown -> showQuestionDialog((mQuestion as QDropdown).items)
            is QRadioButtons -> showQuestionDialog((mQuestion as QRadioButtons).items)
            is QCheckbox -> showQuestionDialog((mQuestion as QCheckbox).items)
        }
    }

    fun showQuestionDialog() {
        val builder = AlertDialog.Builder(context())
        var alert: AlertDialog? = null
        builder.setTitle("Перегляд")

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.scrolable_item_show_question, null, false) as ViewGroup
        builder.setView(layout)

        layout.findViewById<TextView>(R.id.questionTextView).text = mQuestion.question
        layout.findViewById<TextView>(R.id.questionTextView).maxLines = 0

        layout.removeView(layout.findViewById<TextView>(R.id.answerTextView))

        builder.setPositiveButton(context?.resources?.getString(R.string.close)) { _, _ ->
            alert?.dismiss()
        }

        alert = builder.create()
        alert.show()
    }

    fun showQuestionDialog(answers: List<String>) {
        val builder = AlertDialog.Builder(context())
        var alert: AlertDialog? = null
        builder.setTitle("Перегляд")

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.scrolable_item_show_question, null, false)
        builder.setView(layout)

        layout.findViewById<TextView>(R.id.questionTextView).text = mQuestion.question
        layout.findViewById<TextView>(R.id.questionTextView).maxLines = 0


        layout.findViewById<TextView>(R.id.answerTextView).text = answers.joinToString(separator = "\n\n")
        layout.findViewById<TextView>(R.id.answerTextView).maxLines = 0


        builder.setPositiveButton(context?.resources?.getString(R.string.close)) { _, _ ->
            alert?.dismiss()
        }

        alert = builder.create()
        alert.show()
    }

    /**
     * Style card to the selected theme
     * (remove shadows and corner radius)
     */
    fun styleCardView() {
        val action: (CardView) -> Unit = {
            it.setCardBackgroundColor(Color.alpha(0))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                it.elevation = 0f
            }
            it.radius = 0f
        }

        (view as? CardView)?.let {
            action(it)
            return
        }
        ((view as? ViewGroup)?.getChildAt(0) as? CardView)?.let(action)
    }
}
