package com.vadim.hasdfa.justlearn.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import java.util.*

/**
 * Model object for represent test for firebase database
 *
 * Has parcelable implementation
 *
 * @see {@link https://kotlinlang.org/docs/reference/data-classes.html}
 * @see {@link com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper}
 */
data class Answer(
        /**
         * Question
         */
        var question: QBase = QBase(),

        /**
         * Answer
         *
         * @use if type is QTest, or QTestArea, or QDropDown, or QRadioButtons
         * @see @link{com.vadim.hasdfa.justlearn.model.questions}
         */
        var answerText: String? = null,

        /**
         * Answer
         *
         * @use if type is QCheckbox
         * @see {@link com.vadim.hasdfa.justlearn.model.questions.QCheckbox}
         */
        var answerArray: MutableList<String>? = null,

        /**
         * Question type
         *
         * @see {@link com.vadim.hasdfa.justlearn.model.questions.utils.QType}
         */
        var type: QType = QType.Null
): Parcelable {

    @Exclude
    var id: Int = -1

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel): this() {
        type = QType.valueOf(parcel.readString())
        question = parcel.readParcelable(QBase.getClassLoaderByType(type))
        answerText = parcel.readString()
        answerArray = parcel.readStringArrayList()
        id = parcel.readInt()
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type.name)
        parcel.writeParcelable(question, 0)
        parcel.writeString(answerText)
        parcel.writeStringList(answerArray)
        parcel.writeInt(id)
    }

    /**
     * Parcelable implementation
     */
    override fun describeContents() = 0

    /**
     * Parcelable implementation
     */
    companion object CREATOR : Parcelable.Creator<Answer> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): Answer = Answer(parcel)

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<Answer?> = arrayOfNulls(size)

        /**
         * Parcelable implementation
         */
        private fun Parcel.readStringArrayList(): ArrayList<String> {
            val arrayList = ArrayList<String>()
            this.readStringList(arrayList)
            return arrayList
        }
    }

    /**
     * toString() method, which define type of question and show answer for this type
     */
    override fun toString(): String {
        var result = "$question,$type:"
        if (answerText != null) {
            result += answerText!!
        } else if (answerArray != null) {
            result += answerArray!!
        }
        return result
    }
}