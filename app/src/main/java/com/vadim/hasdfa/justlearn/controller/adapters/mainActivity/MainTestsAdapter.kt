package com.vadim.hasdfa.justlearn.controller.adapters.mainActivity

import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.makeramen.roundedimageview.RoundedImageView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.browseTest.ShowTestGroup
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.FDatabaseChildEventListener
import com.vadim.hasdfa.justlearn.utils.FontHelper
import com.vadim.hasdfa.justlearn.utils.minus
import com.vadim.hasdfa.justlearn.utils.plus
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Raksha Vadim on 15.03.2018.
 * With love.
 * In project JustLearn.
 */
class MainTestsAdapter(val student: Student, val onError: (String?) -> Unit): RecyclerView.Adapter<MainTestsAdapter.ViewHolder>() {

    private val mCommentKeys: MutableList<String> = arrayListOf()
    private val tests = mutableListOf<Test>()

    var validate: (Test) -> Boolean = { true }

    private val groups
        get() = if (isPrevZNO)
            tests.filter { it.isZno }.groupBy { it.subjectId }.map { it.key to it.value }
        else
            tests.filter { it.isZno.not() }.groupBy { it.subjectId }.map { it.key to it.value }

    private val hasZNO
        get() = isPrevZNO.not() && tests.count { it.isZno } > 0

    var isPrevZNO = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
        = ViewHolder(
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_tests, parent, false)
    )

    override fun getItemCount(): Int {
        return groups.count() + hasZNO
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var sendingTests = emptyList<Test>()
        var title = "Error 404"

        holder.cardViewMain.setOnClickListener {
            val intent = Intent(it.context, ShowTestGroup::class.java)
            intent.putExtra("title", title)
            intent.putParcelableArrayListExtra("tests", ArrayList(sendingTests))
            it.context.startActivity(intent)
        }

        if (isPrevZNO.not() && (position == 0 && hasZNO)) {
            holder.hideTitle()
            holder.hideTape()
            holder.imageBackground.setImageDrawable(ContextCompat.getDrawable(holder.view.context, R.drawable.ic_zno))
            title = "ЗНО"
            sendingTests = tests.filter { it.isZno }
            return
        }

        val (group, tests) = groups[position - hasZNO]
        sendingTests = tests
        title = group.localeName

        holder.title.text = group.localeName
        holder.imageBackground.setImageDrawable(ContextCompat.getDrawable(holder.view.context, group.drawableId))
        holder.hideTape()
    }

    init {
        tests.clear()

        FirebaseDatabaseHelper.getTests4Students(student, object: FDatabaseChildEventListener<Test> {
            override fun onCancelled(error: DatabaseError?) { onError(error?.message) }
            override fun onChildMoved(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                val pos1 = mCommentKeys.indexOf(previousChildName)
                val pos2 = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos1 == pos2) return

                if (pos1 > -1 && pos2 > -1) {
                    Collections.swap(tests, pos1, pos2)
                    Collections.swap(mCommentKeys, pos1, pos2)

                    if (pos1 < pos2)
                        notifyItemMoved(pos1 + hasZNO, pos2 + hasZNO)
                    else
                        notifyItemMoved(pos2 + hasZNO, pos1 + hasZNO)
                }
            }
            override fun onChildChanged(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                val pos = when(dataSnapshot.key) {
                    null -> tests.size - 1
                    else -> mCommentKeys.indexOf(dataSnapshot.key)
                }

                tests[pos] = result
                mCommentKeys[pos] = dataSnapshot.key

                notifyItemChanged(pos)
            }
            override fun onChildAdded(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                tests.add(0, result)
                mCommentKeys.add(0, dataSnapshot.key)

                notifyItemInserted(0)
            }
            override fun onChildRemoved(result: Test, dataSnapshot: DataSnapshot) {
                if (validate(result).not()) return

                val pos = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos > -1) {
                    tests.removeAt(pos)
                    mCommentKeys.removeAt(pos)
                    notifyItemRemoved(pos)
                }
            }
        })
    }

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val cardViewMain = view.findViewById<CardView>(R.id.cardViewMain)
        val imageBackground = view.findViewById<RoundedImageView>(R.id.imageBackground)

        private val imageTape = view.findViewById<RoundedImageView>(R.id.imageTape).also { img ->
            val r = Random()
            val drawable: (Int) -> Drawable? = { ContextCompat.getDrawable(img.context, it) }
            img.setImageDrawable(drawable(when(r.nextInt(5)) {
                0 -> R.drawable.ic_lenta_blue
                1 -> R.drawable.ic_lenta_pink
                2 -> R.drawable.ic_lenta_cloud
                3 -> R.drawable.ic_lenta_teal
                else -> R.drawable.ic_lenta
            }))
        }

        val fieldLeft = view.findViewById<TextView>(R.id.fieldLeft)
        val fieldRight = view.findViewById<TextView>(R.id.fieldRight)

        private val cardViewTitle = view.findViewById<CardView>(R.id.cardViewTitle)
        val title = view.findViewById<TextView>(R.id.title).also {
            it.typeface = FontHelper.badScriptRegular(it.context)
        }

        fun hideTitle() {
            cardViewTitle.visibility = View.INVISIBLE
        }
        fun showTitle() {
            cardViewTitle.visibility = View.VISIBLE
        }

        fun hideTape() {
            imageTape.visibility = View.INVISIBLE
        }
        fun showTape() {
            imageTape.visibility = View.VISIBLE
        }
    }
}