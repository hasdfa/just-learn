package com.vadim.hasdfa.justlearn.controller.adapters.checkTest

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.RecyclerView
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer.Companion.CustomPair
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer.Companion.cto
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.bd
import com.vadim.hasdfa.justlearn.utils.try_bd
import java.math.BigDecimal


/**
* Created by Raksha Vadim on 12.08.17, 01:39.
* With love.
* In project JustLearn.
*/
class CheckStudentAnswers(testUID: String, student: Student, val onError: (String?) -> Unit): RecyclerView.Adapter<CheckStudentAnswers.BaseHolder>() {

    var firebaseAnswer: FirebaseAnswer = FirebaseAnswer()
    var answersList: MutableList<CustomPair> = mutableListOf()

    var afterUpdate: (Boolean) -> Unit = {}

    init {
        FirebaseDatabaseHelper.getStudentAnswers(testUID, student, onFailure = onError, onSuccess = {
            firebaseAnswer = it
            it.answers.forEachIndexed { index, pair ->
                pair.first.id = index
                if (pair.second.isNullOrEmpty())
                    answersList.add(pair)
            }
            if (answersList.size == 0)
                onError("Помилка, відповіді не знайдені")
            notifyDataSetChanged()
        })
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckStudentAnswers.BaseHolder {
        return BaseHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_check_test, parent, false)
        )
    }

    fun value(str: CharSequence) = when (str) {
        "⅓" -> BigDecimal("0.33")
        "½" -> BigDecimal("0.5")
        "¼" -> BigDecimal("0.25")
        else -> bd(str)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CheckStudentAnswers.BaseHolder, position: Int) {
        val context = holder.itemView.context

        val (answer, _) = answersList[position]


        holder.itemView.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, holder.value.text.split("/").first())
        }
        holder.question.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, holder.value.text.split("/").first())
        }
        holder.answer.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, holder.value.text.split("/").first())
        }
        holder.value.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, holder.value.text.split("/").first())
        }

        holder.question.text = answer.question.question

        holder.answer.text = if (answer.answerArray != null)
            answer.answerArray?.joinToString(separator = ",\n")
        else
            answer.answerText

        holder.value.text = "/${answer.question._value}"

        val colorPrimary =  ContextCompat.getColor(context, R.color.colorPrimary)
        val colorPrimaryDark =  ContextCompat.getColor(context, R.color.colorPrimaryDark)

        val array = arrayOf(holder.btn1, holder.btn2, holder.btn3, holder.btn4, holder.btn5)

        fun onClick(view: View, action: (View) -> Unit) { view.setOnClickListener(action) }

        var selected = -1
        fun clearAll(out: Int) {
            if (selected == out) return
            selected = out
            array.forEachIndexed { index, it ->
                if (it.isEnabled) {
                    if (index == out) {
                        it.setBackgroundColor(colorPrimaryDark)
                        if (index != 5) {
                            val value = value(it.text).toString()
                            holder.value.text = "$value/${answer.question._value}"
                            answersList[position] = answer cto value
                        }
                    } else
                        it.setBackgroundColor(colorPrimary)
                    afterUpdate(answersList.all { it.second != null && try_bd(it.second!!) != null && bd(it.second!!) >= bd("0") })
                }
            }
        }
        with (holder) {
            onClick(btn1, { clearAll(0) })
            onClick(btn2, { clearAll(1) })
            onClick(btn3, { clearAll(2) })
            onClick(btn4, { clearAll(3) })
            onClick(btn5, { clearAll(4) })

            onClick(btn_custom) {
                val builder = AlertDialog.Builder(ContextThemeWrapper(context, R.style.AlertDialogCustom))
                builder.setTitle("Введіть кількість балів")

                val input = EditText(context)
                input.setTextColor(Color.WHITE)
                input.setHintTextColor(Color.WHITE)
                input.hint = "Введіть значення"
                (context as? ActivityBase)
                        ?.setInputTextLayoutColor(Color.WHITE, null, input)

                input.inputType = EditorInfo.TYPE_CLASS_NUMBER
                input.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT)
                input.setPadding(32, input.paddingTop + 8, 24, input.paddingBottom + 8)
                builder.setView(input)

                builder.setPositiveButton("OK") { _, _ ->
                    val num = try_bd(input.text)
                    if (num != null) {
                        if (num > bd(answer.question._value!!)) {
                            onError("Забагато балів")
                        } else {
                            value.text = "$num/${answer.question._value}"
                            answersList[position] = answer cto num.toString()
                            clearAll(5)
                        }
                    } else {
                        onError("Невірне число. Спробуйте ввести щось інше")
                    }
                }
                builder.setNegativeButton(context.resources.getString(R.string.cancel), null)

                val alertDialog = builder.create()
                alertDialog.show()
            }

            val bd = bd(answer.question._value!!)
            if (bd < bd("2")) {
                btn5.isEnabled = false
                btn5.setBackgroundColor(Color.GRAY)
            }
            if (bd < bd("1")) {
                btn4.isEnabled = false
                btn4.setBackgroundColor(Color.GRAY)
            }
            if (bd < bd("0.5")) {
                btn3.isEnabled = false
                btn3.setBackgroundColor(Color.GRAY)
            }
            if (bd < bd("0.33")) {
                btn2.isEnabled = false
                btn2.setBackgroundColor(Color.GRAY)
            }
            if (bd < bd("0.25")) {
                btn1.isEnabled = false
                btn1.setBackgroundColor(Color.GRAY)
            }
        }
    }

    override fun getItemCount(): Int {
        return answersList.size
    }


    open inner class BaseHolder(view: View): RecyclerView.ViewHolder(view) {
        val question = view.findViewById<TextView>(R.id.questionTextView)
        val answer = view.findViewById<TextView>(R.id.answerTextView)
        val value = view.findViewById<AppCompatButton>(R.id.value)

        val btn1 = view.findViewById<AppCompatButton>(R.id.btn1)
        val btn2 = view.findViewById<AppCompatButton>(R.id.btn2)
        val btn3 = view.findViewById<AppCompatButton>(R.id.btn3)
        val btn4 = view.findViewById<AppCompatButton>(R.id.btn4)
        val btn5 = view.findViewById<AppCompatButton>(R.id.btn5)
        val btn_custom = view.findViewById<AppCompatButton>(R.id.btn_custom)
    }
}