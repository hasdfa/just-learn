package com.vadim.hasdfa.justlearn.controller.adapters.mainActivity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.browseTest.ShowTestActivity
import com.vadim.hasdfa.justlearn.controller.activity.checkTests.ShowStudentTestActivity
import com.vadim.hasdfa.justlearn.controller.activity.profile.ProfileActivity
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.FDatabaseChildEventListener
import com.vadim.hasdfa.justlearn.utils.Statuses
import com.vadim.hasdfa.justlearn.utils.minus
import com.vadim.hasdfa.justlearn.utils.plus
import java.util.*
import kotlin.collections.ArrayList

/**
* Created by Raksha Vadim on 07.08.17, 23:51.
* With love.
* In project JustLearn.
*/
open class StudentAdapter(private var student: Student, val onError: (String?) -> Unit
): RecyclerView.Adapter<StudentAdapter.ViewHolder>() {

    private var mCommentKeys: ArrayList<String> = ArrayList()
    private var tests: MutableList<Test> = mutableListOf()

    var isShowHeader = true
    var validate: (Test) -> Boolean = { true }

    private fun Int.color(context: Context?)
            = if (context != null) ContextCompat.getColor(context, this)
                else Color.WHITE

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == 1 && isShowHeader)
            ViewHolder(inflater.inflate(
                    R.layout.profile_student_item, parent,
                    false
                )
            )
        else //if (viewType == 2)
            ViewHolder(inflater.inflate(
                    R.layout.test_short_item, parent,
                    false
                )
            )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == 0 && isShowHeader) {
            with(student) {
                holder.profileNameTextView?.text = name
            }

            holder.profileSettingsImageView?.setColorFilter(R.color.colorSecondaryText.color(holder.itemView.context))
            holder.profileSettingsImageView?.setOnClickListener {
                val context = holder.profileNameTextView?.context
                context?.startActivity(Intent(context, ProfileActivity::class.java))
            }
        } else {
            val test = tests[position - isShowHeader]

            val context = holder.testStartButton?.context
            holder.testTitle?.text = test.title
            holder.testTopic?.text = student.topic
            holder.testStatus?.text = test.testStatus.localeName
            holder.testStartButton?.isEnabled = true
            holder.testStartButton?.setTextColor(R.color.colorGreen.color(context))


            val notEnabled = arrayOf(Statuses.Editing, Statuses.Review, Statuses.Recorded)
            if (test.testStatus in notEnabled) {
                holder.testStartButton?.isEnabled = false
                holder.testStartButton?.setTextColor(Color.WHITE)
            }

            if (test.testStatus == Statuses.Checked) {
                holder.testStartButton?.text = holder.itemView.context.resources.getString(R.string.review)
            }

            holder.testStartButton?.setOnClickListener {
                if (test.testStatus == Statuses.Checked) {
                    val intent = Intent(context, ShowStudentTestActivity::class.java)
                    intent.putExtra("testUID", test.uniqueID)
                    intent.putExtra("student", Student(uid = FirebaseAuth.getInstance().currentUser?.uid!!))

                    context?.startActivity(intent)
                } else if (test.testStatus !in notEnabled) {
                    val intent = Intent(context, ShowTestActivity::class.java)
                    intent.putExtra("path", mCommentKeys[holder.adapterPosition - isShowHeader])
                    intent.putExtra("theme", test.themeId)

                    context?.startActivity(intent)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && isShowHeader)
            return 1
        return 2
    }

    override fun getItemCount(): Int {
        return tests.size + isShowHeader
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val profileNameTextView: TextView? = itemView.findViewById(R.id.nameTextView)
        val profileSettingsImageView: ImageView? = itemView.findViewById(R.id.settingsImageView)

        var testTitle: TextView? = itemView.findViewById(R.id.testTitle)
        var testTopic: TextView? = itemView.findViewById(R.id.topicTextView)
        var testStatus: TextView? = itemView.findViewById(R.id.testStatus)
        var testStartButton: Button? = itemView.findViewById(R.id.btnStartTest)
    }

    init {
        FirebaseDatabaseHelper.getTests4Students(student, object: FDatabaseChildEventListener<Test> {
            override fun onCancelled(error: DatabaseError?) { onError(error?.message) }
            override fun onChildMoved(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                val pos1 = mCommentKeys.indexOf(previousChildName)
                val pos2 = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos1 == pos2) return

                if (pos1 > -1 && pos2 > -1) {
                    Collections.swap(tests, pos1, pos2)
                    Collections.swap(mCommentKeys, pos1, pos2)

                    if (pos1 < pos2)
                        notifyItemMoved(pos1 + 1, pos2 + 1)
                    else
                        notifyItemMoved(pos2 + 1, pos1 + 1)
                }
            }
            override fun onChildChanged(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                val pos = when(dataSnapshot.key) {
                    null -> tests.size - 1
                    else -> mCommentKeys.indexOf(dataSnapshot.key)
                }

                tests[pos] = result
                mCommentKeys[pos] = dataSnapshot.key

                notifyItemChanged(pos+1)
            }
            override fun onChildAdded(result: Test, dataSnapshot: DataSnapshot, previousChildName: String?) {
                if (validate(result).not()) return

                tests.add(0, result)
                mCommentKeys.add(0, dataSnapshot.key)

                FirebaseDatabaseHelper.checkIfTestExists(student, result, hasChild = { status ->
                    result.testStatus = status
                    notifyDataSetChanged()
                }, onFailure = onError)

                notifyItemInserted(1)
            }
            override fun onChildRemoved(result: Test, dataSnapshot: DataSnapshot) {
                if (validate(result).not()) return

                val pos = mCommentKeys.indexOf(dataSnapshot.key)
                if (pos > -1) {
                    tests.removeAt(pos)
                    mCommentKeys.removeAt(pos)
                    notifyItemRemoved(pos + 1)
                }
            }
        })
    }

}

