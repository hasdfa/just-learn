package com.vadim.hasdfa.justlearn.controller.fragments.checkTest

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.adapters.checkTest.CheckStudentAnswers
import com.vadim.hasdfa.justlearn.controller.adapters.checkTest.ShowStudentAnswers
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.utils.Statuses
import kotlinx.android.synthetic.main.activity_create_test.*

/**
 * Created by Raksha Vadim on 10.12.2017.
 * With love.
 * In project JustLearn.
 */
class CheckTestFragment: FragmentBase() {

    private var testUID: String = ""
    private var student: Student = Student()
    private var isShow = false

    private var mAdapter: RecyclerView.Adapter<*>? = null

    var isEnabled = false

    val delegate: (Student, Statuses) -> Unit = { it, status ->
        student = it
        mAdapter = if (status == Statuses.Checked) {
            saveImageView?.visibility = View.VISIBLE
            ShowStudentAnswers(testUID, it) { showError(it){ activity?.supportFinishAfterTransition() } }
        } else {
            CheckStudentAnswers(testUID, it) { showError(it) { activity?.supportFinishAfterTransition() } }
                    .also {
                        doStuff(it)
                    }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            // Reuse same layouts
         = inflater.inflate(R.layout.activity_create_test, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val activity = (activity as ActivityBase)
        activity.setSupportActionBar(activity.findViewById(R.id.toolbar))
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mRecyclerView.layoutManager = LinearLayoutManager(context)

        mAdapter = if (isShow) {
            ShowStudentAnswers(testUID, student) { showError(it) }
        } else {
            CheckStudentAnswers(testUID, student) { showError(it) }.also {
                doStuff(it)
            }
        }
        mRecyclerView.adapter = mAdapter

        editThemeImageView.visibility = View.INVISIBLE
        testSettings.visibility = View.INVISIBLE
        addQuestionFab.visibility = View.INVISIBLE
    }

    fun doStuff(adapter: CheckStudentAnswers) {
        saveImageView.visibility = View.VISIBLE
        adapter.afterUpdate = {
            isEnabled = it
            if (it) {
                saveImageView.setImageDrawable(drawable(R.drawable.ic_save))
            } else {
                saveImageView.setImageDrawable(drawable(R.drawable.ic_save_disable))
            }
        }
        saveImageView.setOnClickListener {
            if (isEnabled) {
                load(true)
                val firebaseAnswer = adapter.firebaseAnswer
                val changedAnswers = adapter.answersList.map { it.first.id }

                firebaseAnswer.answers = firebaseAnswer.answers.map { (answer, value) ->
                    if (answer.id in changedAnswers) {
                        adapter.answersList.first { it.first.id == answer.id }
                    } else {
                        FirebaseAnswer.Companion.CustomPair(answer, value)
                    }
                }.toMutableList()

                FirebaseDatabaseHelper.saveResults(firebaseAnswer, student, testUID, onFailure = { showError(it.message) }, onSuccess = {
                    load(false)
                    showAlert(str(R.string.answer_saved), onOk = {
                        activity?.supportFinishAfterTransition()
                    })
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = savedInstanceState ?: arguments ?: return Unit.also {
            showError(context().resources.getString(R.string.invalid_data))
        }

        student = bundle.getParcelable("student")
        testUID = bundle.getString("testUID")
        isShow = bundle.getBoolean("isShow", false)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("testUID", testUID)
        outState.putParcelable("student", student)
        outState.putBoolean("isShow", isShow)
    }
}