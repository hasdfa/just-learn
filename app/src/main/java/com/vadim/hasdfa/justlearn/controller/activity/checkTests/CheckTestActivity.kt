package com.vadim.hasdfa.justlearn.controller.activity.checkTests

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.adapters.testAdapter.CheckTestsAdapter
import com.vadim.hasdfa.justlearn.controller.fragments.checkTest.CheckTestFragment
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.utils.Statuses
import kotlinx.android.synthetic.main.activity_check_tests.*

/**
 * Display all students, which passed test
 * including master-detail pattern
 */
class CheckTestActivity: ActivityBase() {

    private val selectedTest: Test by lazy {
        intent.getParcelableExtra<Test>("selectedTest") ?: throw KotlinNullPointerException()
    }
    private var mAdapter: CheckTestsAdapter? = null

    private var delegate: ((Student, Statuses) -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_tests)

        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mRecyclerView.layoutManager = LinearLayoutManager(this)

        if (isTablet && savedInstanceState == null) {
            val fragment = CheckTestFragment()

            val arguments = Bundle()
            arguments.putString("testUID", selectedTest.uniqueID)
            fragment.arguments = arguments

            delegate = fragment.delegate

            supportFragmentManager.beginTransaction()
                    .add(R.id.mFrame, fragment)
                    .commit()
        }
    }

    override fun onStart() {
        super.onStart()
        mAdapter = CheckTestsAdapter(selectedTest, onError = {
            showError(it) { this@CheckTestActivity.supportFinishAfterTransition() }
        }, onSelect = { it, status ->
            if (isTablet) {
                delegate?.invoke(it, status)
            } else {
                if (status == Statuses.Checked) {
                    val intent = Intent(this, ShowStudentTestActivity::class.java)
                    intent.putExtra("testUID", selectedTest.uniqueID)
                    intent.putExtra("student", it)

                    startActivity(intent)
                } else {
                    val intent = Intent(this, CheckStudentTestActivity::class.java)
                    intent.putExtra("testUID", selectedTest.uniqueID)
                    intent.putExtra("student", it)

                    startActivity(intent)
                }
            }
        })
        mRecyclerView.adapter = mAdapter
    }


    override fun onPause() {
        super.onPause()
        mAdapter = null
    }
}