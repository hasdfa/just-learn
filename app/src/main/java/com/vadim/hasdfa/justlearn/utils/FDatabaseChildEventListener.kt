package com.vadim.hasdfa.justlearn.utils

import android.os.Parcelable
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError

/**
 * Created by Raksha Vadim on 17.01.2018.
 * With love.
 * In project JustLearn.
 */
interface FDatabaseChildEventListener<in T: Parcelable> {
    fun onChildAdded(result: T, dataSnapshot: DataSnapshot, previousChildName: String?)

    fun onChildChanged(result: T, dataSnapshot: DataSnapshot,  previousChildName: String?)

    fun onChildRemoved(result: T, dataSnapshot: DataSnapshot)

    fun onChildMoved(result: T, dataSnapshot: DataSnapshot,  previousChildName: String?)

    fun onCancelled(error: DatabaseError?)
}