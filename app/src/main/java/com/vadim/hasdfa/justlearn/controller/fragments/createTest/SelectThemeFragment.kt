package com.vadim.hasdfa.justlearn.controller.fragments.createTest

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.fragment_create_question.*

/**
* Created by Raksha Vadim on 13.08.17, 17:46.
*/
class SelectThemeFragment: FragmentBase() {
    interface SelectedThemeListener {
        fun onSelect(position: Int)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is SelectedThemeListener) {
            mCallback = context
        } else throw Exception("Context must implement SelectedThemeListener")
    }

    var mCallback: SelectedThemeListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_create_question, container, false)

    var selectedItem = -1
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedItem = arguments?.getInt("selected", -1) ?: -1

        val glm = getAdaptiveGridLM()
        mRecyclerView.layoutManager = glm
        mRecyclerView.adapter = ThemesAdapter()
    }


    inner class ThemesAdapter : RecyclerView.Adapter<ThemesAdapter.ViewHolder>() {
        private val themes: List<FormThemes.ThemeBase> by lazy {
            FormThemes.getThemes()
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val theme = themes[position]
            with(holder) {
                if (selectedItem == position)
                    itemView.setBackgroundColor(color(com.vadim.hasdfa.justlearn.R.color.colorDivider))
                kotlin.with(theme) {
                    qTextColor.setTextColor(questionColor)
                    aTextColor.setTextColor(answerColor)
                    itemCardView.setBackgroundColor(backgroundColor)
                    primaryColor.setBackgroundColor(colorPrimary)
                    if (colorPrimary == backgroundColor) {
                        primaryColorBorder.visibility = android.view.View.VISIBLE
                    }
                    clickable.setOnClickListener {
                        mCallback?.onSelect(holder.adapterPosition)
                    }
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_theme, parent, false)
            )
        }

        override fun getItemCount(): Int = themes.size

        inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val qTextColor: TextView = view.findViewById(R.id.qTextColor)
            val aTextColor: TextView = view.findViewById(R.id.aTextColor)
            val primaryColor: View = view.findViewById(R.id.primaryColor)
            val primaryColorBorder: View = view.findViewById(R.id.primaryColorBorder)
            val itemCardView: CardView = view.findViewById(R.id.itemCardView)
            var clickable: RelativeLayout = view.findViewById(R.id.clickable)
        }
    }
}