package com.vadim.hasdfa.justlearn.controller.fragments.main

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.LayoutParams
import android.support.v7.widget.RecyclerView.LayoutParams.MATCH_PARENT
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.controller.adapters.mainActivity.MainTestsAdapter
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import com.vadim.hasdfa.justlearn.model.UserData

/**
 * Created by Raksha Vadim on 15.03.2018.
 * With love.
 * In project JustLearn.
 */
class MainTestsFragment: FragmentBase() {

    val student
        get() = UserData.student!!

    val mRecyclerView by lazy {
        val rv = RecyclerView(context)
        rv.layoutParams = LayoutParams(MATCH_PARENT,  MATCH_PARENT)
        rv.layoutManager = LinearLayoutManager(context)
        return@lazy rv
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return mRecyclerView
    }

    override fun onResume() {
        super.onResume()
        val mAdapter = MainTestsAdapter(student) { showError(it) }
        load(true)

        mRecyclerView.adapter = mAdapter
        mAdapter.validate = {
            this@MainTestsFragment.load(false)
            true
        }
    }

}