package com.vadim.hasdfa.justlearn.controller.fragments.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.createTest.PreviewCreateTestActivity
import com.vadim.hasdfa.justlearn.controller.activity.login.SignInActivity
import com.vadim.hasdfa.justlearn.controller.adapters.mainActivity.StudentAdapter
import com.vadim.hasdfa.justlearn.controller.adapters.mainActivity.TeacherAdapter
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.Teacher
import com.vadim.hasdfa.justlearn.model.UserData
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import kotlinx.android.synthetic.main.activity_test_main.*

/**
 * Created by Raksha Vadim on 02.03.2018.
 * With love.
 * In project JustLearn.
 */
class MainFragment: FragmentBase() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_test_main, container, false)
    }

    private var mAdapter: RecyclerView.Adapter<*>? = null

    override fun onResume() {
        super.onResume()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            load(true)
            FirebaseDatabaseHelper.getUserData(user, {
                getStudent(it)
                addTestFab.hide()
            }, {
                getTeacher(it)
                addTestFab.show()
            }, { /* error */
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(context(), SignInActivity::class.java))
                activity?.finish()
            })
        } else {
            startActivity(Intent(context(), SignInActivity::class.java))
            activity?.finish()
            return
        }

        if (UserData.userType == UserData.UserType.Teacher)
            addTestFab.show()
        else
            addTestFab.hide()
    }


    private fun getTeacher(teacher: Teacher) {
        UserData.set(teacher)

        if (teacher.isAccepted) {
            setTestCreateEnabled()
        } else {
            addTestFab.visibility = View.INVISIBLE
        }

        mAdapter = TeacherAdapter(teacher, { showError(it) { activity?.supportFinishAfterTransition() } })
        mRecyclerView.layoutManager = LinearLayoutManager(context())
        mRecyclerView.adapter = mAdapter
        mAdapter?.notifyDataSetChanged()

        mRecyclerView.clearOnScrollListeners()
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0) {
                    addTestFab.hide()
                } else {
                    addTestFab.show()
                }

                super.onScrolled(recyclerView, dx, dy)
            }
        })

        load(false)
    }

    private fun getStudent(student: Student) {
        UserData.set(student)

        mAdapter = StudentAdapter(student)  { showError(it) }
        mRecyclerView.layoutManager = LinearLayoutManager(context())
        mRecyclerView.adapter = mAdapter
        mAdapter?.notifyDataSetChanged()

        load(false)
    }


    private fun setTestCreateEnabled(){
        addTestFab.visibility = View.VISIBLE
        addTestFab.setOnClickListener {
            startActivity(Intent(context(), PreviewCreateTestActivity::class.java))
        }
    }

}