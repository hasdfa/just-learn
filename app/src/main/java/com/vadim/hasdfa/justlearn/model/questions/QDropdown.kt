package com.vadim.hasdfa.justlearn.model.questions

import android.os.Parcel
import android.os.Parcelable
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType

/**
 * Question type, which describes
 * question view presentation as dropdown list
 * with single selection
 *
 * Has parcelable implementation from superclass
 *
 * @see {@link com.vadim.hasdfa.justlearn.model.questions.utils.QBase}
 */
class QDropdown: QBase {
    /**
     * Realize of abstract field type
     */
    override var type = QType.Dropdown

    /**
     * Multiple cases for answer
     */
    var items: MutableList<String> = mutableListOf()

    /**
     * Answer for question
     */
    var answer: String = ""

    /**
     * Call superclass`s constructor
     */
    constructor(): super()

    /**
     * Parcelable implementation
     */
    constructor(parcel: Parcel): super(parcel) {
        parcel.readStringList(items)
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeStringList(items)
    }

    /**
     * Parcelable implementation
     */
    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable implementation
     */
    companion object CREATOR : Parcelable.Creator<QDropdown> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): QDropdown {
            return QDropdown(parcel)
        }

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<QDropdown?> {
            return arrayOfNulls(size)
        }
    }
}