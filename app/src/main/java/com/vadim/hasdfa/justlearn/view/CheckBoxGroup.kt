package com.vadim.hasdfa.justlearn.view

import android.content.Context
import android.support.v7.widget.AppCompatCheckBox
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout


/**
 * A LinearLayout which contains a list of AppCompatCheckBox,
 * including:
 *      Only list of checkboxes
 *      Allows dynamic add on checked listener
 */
class CheckBoxGroup: LinearLayout {

    /**
     * Callback, which invokes when check box check or uncheck
     */
    private var mCallback: ((AppCompatCheckBox, Boolean) -> Unit)? = null

    constructor(context: Context): super(context)
    constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet)

    /**
     * An init method, that calls after constructor
     */
    init {
        this.orientation = VERTICAL
    }

    /**
     * Method, which allows add to this view new elements {@link android.support.v7.widget.AppCompatCheckBox}
     *
     * @param checkBox element, which will be added to this view
     */
    internal fun addCheckBox(checkBox: AppCompatCheckBox) {
        val lp = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        lp.setMargins(8, 12, 12, 8)
        this.addView(checkBox, lp)
        checkBox.setOnCheckedChangeListener { _, b -> mCallback?.invoke(checkBox, b) }
    }

    /**
     * Method, which set callback method with new
     *
     * @param mListener callback lambda function
     */
    internal fun onCheckListener(mListener: (AppCompatCheckBox, Boolean) -> Unit) {
        mCallback = mListener
    }
}