package com.vadim.hasdfa.justlearn.utils

import android.os.Parcel
import com.google.firebase.database.DataSnapshot
import com.vadim.hasdfa.justlearn.model.FirebaseTest
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import java.math.BigDecimal



/**
 * Created by Raksha Vadim on 03.03.2018.
 * With love.
 * In project JustLearn.
 */

fun bd(str: String): BigDecimal
        = str.toBigDecimal()
fun bd(cs: CharSequence): BigDecimal
        = bd(cs.toString())

fun try_bd(str: String): BigDecimal?
        = str.toBigDecimalOrNull()
fun try_bd(cs: CharSequence): BigDecimal?
        = try_bd(cs.toString())

operator fun Int.plus(boolean: Boolean): Int = this + if (boolean) 1 else 0
operator fun Int.minus(boolean: Boolean): Int = this - if (boolean) 1 else 0


fun Int.bd() = this.toBigDecimal()
fun Double.bd() = this.toBigDecimal()


fun<T> Array<T>.copyFrom(from: Int)
    = this.copyOfRange(from, this.size - 1)

fun Parcel.readQuestion(which: Int): QBase {
    return when (which) {
        0 -> readParcelable<QText>(QText::class.java.classLoader)
        1 -> readParcelable<QTextArea>(QTextArea::class.java.classLoader)
        2 -> readParcelable<QDropdown>(QDropdown::class.java.classLoader)
        3 -> readParcelable<QRadioButtons>(QRadioButtons::class.java.classLoader)
        4 -> readParcelable<QCheckbox>(QCheckbox::class.java.classLoader)
        else -> throw NotImplementedError()
    }
}

@Suppress("UNCHECKED_CAST")
fun DataSnapshot.readFirebaseTest(): FirebaseTest? {
    val data = (value as? HashMap<String, Any>) ?: return null
    val test = FirebaseTest()
    test.title = data["title"].toString()
    test.teacherUID = data["teacherUID"].toString()
    test.themeId = data["themeId"]?.toString()?.toInt() ?: -1
    test.groups = (data["groups"] as? MutableList<String>) ?: mutableListOf()
    test.subjectId = SubjectId.valueOf(data["subjectId"]?.toString() ?: "Undefined")
    test.isZno = data["isZno"]?.toString()?.toBoolean() ?: false

    test.questions = (data["questions"] as Collection<Any?>).filter { it != null }.map { it!! }.map {
        val _data = it as HashMap<String, Any>

        val question = _data["question"].toString()
        val helpText = _data["helpText"].toString()
        val _value = _data["_value"].toString()
        val type = _data["type"].toString()

        when (QType.valueOf(type)) {
            QType.Text -> {
                val q = QText()
                q.question = question
                q.helpText = helpText
                q._value = _value
                return@map q
            }
            QType.TextArea -> {
                val q = QTextArea()
                q.question = question
                q.helpText = helpText
                q._value = _value
                return@map q
            }
            QType.Dropdown -> {
                val q = QDropdown()
                q.question = question
                q.helpText = helpText
                q._value = _value
                q.items = _data["items"] as MutableList<String>
                return@map q
            }
            QType.RadioBtn -> {
                val q = QRadioButtons()
                q.question = question
                q.helpText = helpText
                q._value = _value
                q.items = _data["items"] as MutableList<String>
                return@map q
            }
            QType.Checkbox -> {
                val q = QCheckbox()
                q.question = question
                q.helpText = helpText
                q._value = _value
                q.items = _data["items"] as MutableList<String>
                return@map q
            }
            QType.Grid -> null
            QType.Stack -> null
            else -> throw NotImplementedError()
        }
    }.filter { it != null }.map { it!! }.toMutableList()
    return test
}