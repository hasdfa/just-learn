package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.CardView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.QText
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.question_text.*

/**
 * Fragment, which displays text input field
 *
 * @see QType.Text
 * @see QText
 */
class TextFragment: QuestionFragmentBase() {

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        var error: Error? = null

        if (answer.answerText?.isEmpty() != false) { // true or null
            error = Error(context().resources.getString(R.string.answer_not_selected, mQuestion.question))
        } else if (answer.answerText.isNullOrEmpty()) {
            error = Error(context().resources.getString(R.string.answer_more_than, 1))
        }

        return Pair(answer, error)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.question_text, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mQuestion.type != QType.Text) throw Exception("Incorrect type")

        view.setOnClickListener { showMore() }
        headerTextView.setOnClickListener {
            showMore()
        }

        headerTextView.hint = mQuestion.question
        helperTextView.text = mQuestion.helpText
        contentTextView.requestFocus()

        contentTextView.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                answer.answerText = s?.toString()
                submit()
            }
        })

        view.post {
            val theme = FormThemes.getThemes().getOrNull(themeId)
            theme?.let {
                setInputTextLayoutColor(it.questionColor, headerTextView, contentTextView)

                contentTextView.setTextColor(it.answerColor)
                helperTextView.setTextColor(it.questionColor)
                contentTextView.setHintTextColor(it.questionColor)
                setUpperHintColor(headerTextView, it.questionColor)

                ViewCompat.setBackgroundTintList(contentTextView, ColorStateList.valueOf(it.questionColor))
            }

            (view as? CardView)?.let {
                it.setCardBackgroundColor(Color.alpha(0))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.elevation = 0f
                }
                it.cardElevation = 0f
                it.radius = 0f
            }
        }
    }
}