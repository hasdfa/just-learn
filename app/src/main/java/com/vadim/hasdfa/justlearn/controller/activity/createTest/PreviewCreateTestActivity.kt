package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.adapters.testAdapter.PreviewCreateTestAdapter
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.utils.copyFrom
import kotlinx.android.synthetic.main.preview_create_activity.*

/**
 * Created by Raksha Vadim on 21.01.2018.
 * With love.
 * In project JustLearn.
 */
class PreviewCreateTestActivity: ActivityBase() {

    private val groups by lazy { resources.getStringArray(R.array.topics).copyFrom(2) }
    private val mAdapter by lazy { PreviewCreateTestAdapter(groups, setEnabled) }

    private val setEnabled: (Boolean) -> Unit = {
        createButton.isEnabled = it
        createButton.setTextColor(if(it) colorGreen else colorAccent)
    }

    private val test: Test? by lazy {
        intent.getParcelableExtra<Test?>("test")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.preview_create_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = str(R.string.select_your_topic)

        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = mAdapter

        setEnabled(true)

        if (test != null) {
            mAdapter.changeTitle(test!!.title)
            mAdapter.selectGroups(test!!.groups)

            createButton.setOnClickListener {
                val (title, target) = mAdapter.getResults()
                checkData(title, {
                    /* Success */
                    test?.title = title
                    test?.groups = target.toMutableList()
                    setResult(Activity.RESULT_OK, Intent().putExtra("test", test))
                    supportFinishAfterTransition()
                })
            }

            crateTestLabel.text = str(R.string.change_test)
            createButton.text = str(R.string.save)
        } else {
            createButton.setOnClickListener {
                val (title, target) = mAdapter.getResults()
                checkData(title, {
                    /* Success */
                    val intent = Intent(this, CreateTestActivity::class.java)
                    intent.putExtra("title", title)
                    intent.putExtra("topic", target)
                    startActivity(intent)
                    supportFinishAfterTransition()
                })
            }
        }
    }

    override fun onBackPressed() {
        showAlert(str(R.string.leave_without_saving), onOk = {
            super.onBackPressed()
        })
    }


    private fun checkData(title: String, success: () -> Unit) {
        var isSuccess = true
        var errorDescription = ""

        if (title.isEmpty()) {
            isSuccess = false
            errorDescription = str(R.string.q_is_empty)
        }

        if (isSuccess) {
            success()
        } else {
            showError(errorDescription, {  })
        }
    }
}