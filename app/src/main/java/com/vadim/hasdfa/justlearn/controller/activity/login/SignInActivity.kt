package com.vadim.hasdfa.justlearn.controller.activity.login

import android.content.Intent
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.MainActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : ActivityBase() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        supportActionBar?.hide()
        actionBar?.hide()

        submitButton.setOnClickListener {
            load(true)
            checkData {
                signIn()
            }
        }

        signUpButton.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            overridePendingTransition(R.anim.push_left_in_xz, R.anim.push_left_out_xz)
            finish()
        }
    }

    private fun signIn() {
        val signIn = FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(
                        emailEditText.text.toString(),
                        passwordEditText.text.toString()
                )
        signIn.addOnCompleteListener {
            if (it.isComplete) {
                if (!it.isSuccessful) {
                    it.exception?.let {
                        it.printStackTrace()
                        showError(it.message, {load(false)})
                    }
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            } else {
                it.exception?.printStackTrace()
                showError(it.exception?.message, {load(false)})
            }
            load(false)
        }
        signIn.addOnFailureListener {
            it.printStackTrace()
            showError(it.message, {load(false)})
        }
    }

    private fun checkData(function: () -> Unit) {
        var isCorrect = true
        var errorDescription = ""

        if (passwordEditText.text.toString().length < 6) {
            isCorrect = false
            errorDescription = str(R.string.password_m_containts_more_than_5)
        }
        if (passwordEditText.text.toString().isEmpty()) {
            isCorrect = false
            errorDescription = str(R.string.pasword_is_empty)
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches()) {
            isCorrect = false
            errorDescription = str(R.string.email_is_invalid)
        }

        if (isCorrect) {
            function()
        } else {
            showError(errorDescription) { load(false) }
        }
    }

    override fun load(isLoading: Boolean) {
        super.load(isLoading)
        submitButton.isEnabled = !isLoading
        signUpButton.isEnabled = !isLoading
    }

    private val emailKey = "email"
    private val passwordKey = "pass"
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let {
            it.putString(emailKey, emailEditText.text.toString())
            it.putString(passwordKey, passwordEditText.text.toString())
        }
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            emailEditText.setText(it.getString(emailKey)      ?: "")
            passwordEditText.setText(it.getString(passwordKey) ?: "")
        }
    }
}