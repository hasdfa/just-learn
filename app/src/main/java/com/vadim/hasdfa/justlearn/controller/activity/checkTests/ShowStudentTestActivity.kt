package com.vadim.hasdfa.justlearn.controller.activity.checkTests

import android.os.Bundle
import android.view.View
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.fragments.checkTest.CheckTestFragment
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Student

/**
 * Created by Raksha Vadim on 16.03.2018.
 * With love.
 * In project JustLearn.
 */
class ShowStudentTestActivity: ActivityBase() {

    private val testUID by lazy {
        intent.getStringExtra("testUID")
    }
    private val selectedStudent by lazy {
        intent.getParcelableExtra<Student>("student")
    }

    val fragment by lazy { CheckTestFragment() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)
        load(false)

        if (savedInstanceState == null) {
            fragment.arguments = Bundle().also {
                it.putString("testUID", testUID)
                it.putParcelable("student", selectedStudent)
                it.putBoolean("isShow", true)
            }

            supportFragmentManager.beginTransaction()
                    .replace(R.id.mFrame, fragment)
                    .commit()
        }
    }

    override fun onResume() {
        super.onResume()
        findViewById<View>(R.id.saveImageView)?.visibility = View.INVISIBLE
    }

}