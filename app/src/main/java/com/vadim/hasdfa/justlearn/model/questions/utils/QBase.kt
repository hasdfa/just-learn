package com.vadim.hasdfa.justlearn.model.questions.utils

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.vadim.hasdfa.justlearn.model.questions.*
import java.util.*

/**
 * Base abstract question type, which implements base functionality
 *
 * Has parcelable implementation
 *
 * @see {@link https://kotlinlang.org/docs/reference/data-classes.html}
 */
@IgnoreExtraProperties
open class QBase: Parcelable {
    /**
     * Default field, that contains question type
     */
    open var type: QType = QType.Null

    /**
     * Question title
     */
    var question: String = ""

    /**
     * Question value
     */
    var _value: String? = null

    /**
     * Help text, which must help to user
     * answer the question more correctly
     */
    var helpText: String = ""

    /**
     * Empty constructor
     */
    constructor() {
        id = UUID.randomUUID().toString()
    }

    /**
     * Realize of abstract field type
     */
    protected constructor(parcel: Parcel?): this() {
        parcel?.let {
            it.readString()?.let { question = it }
            it.readString()?.let { helpText = it }
            it.readString()?.let { type = QType.valueOf(it) }
            _value = it.readString()?.toBigDecimalOrNull()?.toString()
            id = it.readString()
        }
        if (parcel == null) id = null
    }

    /**
     * toString() method which describe self by different question type
     */
    override fun toString(): String {
        var str = "$question;$helpText;$type;=$_value"
        if (this is QDropdown)
            str += "$items"
        if (this is QRadioButtons)
            str += "$items"
        if (this is QCheckbox)
            str += "$items"
        return str
    }

    /**
     * Parcelable implementation
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(question)
        parcel.writeString(helpText)
        parcel.writeString(this.type.name)
        parcel.writeString(_value.toString())
        parcel.writeString(id)
    }

    @Exclude
    fun getValue()
            = _value?.toBigDecimal()

    @Exclude
    var id: String?

    /**
     * Parcelable implementation
     */
    override fun describeContents() = 0

    /**
     * Parcelable implementation
     */
    companion object CREATOR: Parcelable.Creator<QBase> {
        /**
         * Parcelable implementation
         */
        override fun createFromParcel(parcel: Parcel): QBase {
            return QBase(parcel)
        }

        /**
         * Parcelable implementation
         */
        override fun newArray(size: Int): Array<QBase?> {
            return arrayOfNulls(size)
        }

        /**
         * Get object by position in QType enum
         *
         * @see {@link com.vadim.hasdfa.justlearn.model.questions.utils.QType}
         */
        fun getQByPosition(p: Int): QBase {
            return when (p) {
                0 -> QText()
                1 -> QTextArea()
                2 -> QDropdown()
                3 -> QRadioButtons()
                4 -> QCheckbox()
                else -> throw NotImplementedError()
            }
        }

        fun getClassLoaderByPosition(p: Int): ClassLoader {
            return when (p) {
                0 -> QText::class.java.classLoader
                1 -> QTextArea::class.java.classLoader
                2 -> QDropdown::class.java.classLoader
                3 -> QRadioButtons::class.java.classLoader
                4 -> QCheckbox::class.java.classLoader
                else -> throw NotImplementedError()
            }
        }


        fun getPositionByQ(q: QBase): Int {
            return when (q) {
                is QText -> 0
                is QTextArea -> 1
                is QDropdown -> 2
                is QRadioButtons -> 3
                is QCheckbox -> 4
                else -> throw NotImplementedError()
            }
        }

        fun getClassLoaderByType(type: QType): ClassLoader {
            return when (type) {
                QType.Text -> QText::class.java.classLoader
                QType.TextArea -> QTextArea::class.java.classLoader
                QType.Dropdown-> QDropdown::class.java.classLoader
                QType.RadioBtn -> QRadioButtons::class.java.classLoader
                QType.Checkbox -> QCheckbox::class.java.classLoader
                else -> throw NotImplementedError()
            }
        }
    }
}