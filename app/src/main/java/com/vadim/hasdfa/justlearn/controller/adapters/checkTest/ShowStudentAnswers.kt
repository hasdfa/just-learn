package com.vadim.hasdfa.justlearn.controller.adapters.checkTest

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.FirebaseAnswer
import com.vadim.hasdfa.justlearn.model.Student
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper


/**
* Created by Raksha Vadim on 12.08.17, 01:39.
* With love.
* In project JustLearn.
*/
class ShowStudentAnswers(testUID: String, student: Student, val onError: (String?) -> Unit): RecyclerView.Adapter<ShowStudentAnswers.BaseHolder>() {

    var firebaseAnswer: FirebaseAnswer = FirebaseAnswer()

    init {
        FirebaseDatabaseHelper.getStudentAnswers(testUID, student, onFailure = onError, onSuccess = {
            firebaseAnswer = it
            notifyDataSetChanged()
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowStudentAnswers.BaseHolder {
        return BaseHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_show_test, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ShowStudentAnswers.BaseHolder, position: Int) {
        val (answer, value) = firebaseAnswer.answers[position]

        holder.question.text = answer.question.question

        holder.answer.text = if (answer.answerArray != null)
            answer.answerArray?.joinToString(separator = ",\n")
        else
            answer.answerText

        holder.value.text = "$value/${answer.question._value}"

        holder.itemView.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, value)
        }
        holder.question.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, value)
        }
        holder.answer.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, value)
        }
        holder.value.setOnClickListener {
            ShowStudentAnswers.showAnswerDialog(it.context, answer, value)
        }

        if (value!!.toBigDecimal() < answer.question._value!!.toBigDecimal()) {
            if (value.toBigDecimal() == 0.toBigDecimal()) {
                holder.value.setBackgroundColor(Color.RED)
            } else {
                holder.value.setBackgroundColor(Color.argb(255, 252, 191, 92))
            }
        }
    }

    companion object {
        fun showAnswerDialog(context: Context, answer: Answer, value: String?) {
            val builder = AlertDialog.Builder(context)
            var alert: AlertDialog? = null
            builder.setTitle("Перегляд")

            val layout = LayoutInflater.from(context)
                    .inflate(R.layout.scrolable_item_show_test, null, false)
            builder.setView(layout)

            if (value != null && value.isNotEmpty()) {
                if (value.toBigDecimal() < answer.question._value!!.toBigDecimal()) {
                    if (value.toBigDecimal() == 0.toBigDecimal()) {
                        layout.findViewById<AppCompatButton>(R.id.value).setBackgroundColor(Color.RED)
                    } else {
                        layout.findViewById<AppCompatButton>(R.id.value).setBackgroundColor(Color.argb(255, 252, 191, 92))
                    }
                }
                val text = "$value/${answer.question._value}"
                layout.findViewById<AppCompatButton>(R.id.value).text = text
            } else {
                layout.findViewById<AppCompatButton>(R.id.value).visibility = View.INVISIBLE
            }

            layout.findViewById<TextView>(R.id.questionTextView).text = answer.question.question
            layout.findViewById<TextView>(R.id.questionTextView).maxLines = 0

            layout.findViewById<TextView>(R.id.answerTextView).text = if (answer.answerArray != null)
                answer.answerArray?.joinToString(separator = ",\n")
            else
                answer.answerText
            layout.findViewById<TextView>(R.id.answerTextView).maxLines = 0


            builder.setPositiveButton(context.resources.getString(R.string.close)) { _, _ ->
                alert?.dismiss()
            }

            alert = builder.create()
            alert.show()
        }
    }

    override fun getItemCount(): Int {
        return firebaseAnswer.answers.size
    }


    open inner class BaseHolder(view: View): RecyclerView.ViewHolder(view) {
        val question = view.findViewById<TextView>(R.id.questionTextView)
        val answer = view.findViewById<TextView>(R.id.answerTextView)
        val value = view.findViewById<AppCompatButton>(R.id.value)
    }
}