package com.vadim.hasdfa.justlearn.controller.fragments.createTest

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_select_type.*

/**
* Created by Raksha Vadim on 09.08.17, 00:54.
*/
class SelectQuestionTypeFragment : FragmentBase() {
    private var mCallback: OnItemSelected? = null
    interface OnItemSelected {
        fun onSelected(p: Int)
    }

    private var selectedPosition = 0

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnItemSelected)
            mCallback = context
        else throw Exception("You must implement onItemSelected")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_select_type, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isTablet) select(selectedPosition)

        selectedItem1.setOnClickListener { onItemSelected(0) }
        selectedItem2.setOnClickListener { onItemSelected(1) }
        selectedItem3.setOnClickListener { onItemSelected(2) }
        selectedItem4.setOnClickListener { onItemSelected(3) }
        selectedItem5.setOnClickListener { onItemSelected(4) }
    }
    private fun onItemSelected(p: Int) {
        if (isTablet) select(p)
        selectedPosition = p

        if (mCallback != null) {
            mCallback?.onSelected(p)
        }
    }

    private val selectableBackground: Drawable by lazy {
        selectedItem1?.background ?: ColorDrawable(Color.WHITE)
    }

    private fun select(i: Int) {
        if (i!=0) selectedItem1.background = selectableBackground
        if (i!=1) selectedItem2.background = selectableBackground
        if (i!=2) selectedItem3.background = selectableBackground
        if (i!=3) selectedItem4.background = selectableBackground
        if (i!=4) selectedItem5.background = selectableBackground

        when (i) {
            0 -> selectedItem1.setBackgroundColor(colorPrimarySelected)
            1 -> selectedItem2.setBackgroundColor(colorPrimarySelected)
            2 -> selectedItem3.setBackgroundColor(colorPrimarySelected)
            3 -> selectedItem4.setBackgroundColor(colorPrimarySelected)
            4 -> selectedItem5.setBackgroundColor(colorPrimarySelected)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            selectedPosition = it.getInt("selectedPosition", 0)
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("selectedPosition", selectedPosition)
    }
}