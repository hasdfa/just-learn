package com.vadim.hasdfa.justlearn.utils

import android.graphics.Color
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme1
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme10
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme11
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme12
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme13
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme14
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme15
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme16
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme17
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme18
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme19
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme2
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme20
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme21
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme22
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme23
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme24
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme25
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme3
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme4
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme5
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme6
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme7
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme8
import com.vadim.hasdfa.justlearn.utils.FormThemes.Theme9
import com.vadim.hasdfa.justlearn.utils.FormThemes.ThemeBase

/**
 * Object, which describes base theme, which is
 * inner abstract class of this object
 *
 * Now has 25 realised themes:
 * @see Theme1
 * @see Theme2
 * @see Theme3
 * @see Theme4
 * @see Theme5
 * @see Theme6
 * @see Theme7
 * @see Theme8
 * @see Theme9
 * @see Theme10
 * @see Theme11
 * @see Theme12
 * @see Theme13
 * @see Theme14
 * @see Theme15
 * @see Theme16
 * @see Theme17
 * @see Theme18
 * @see Theme19
 * @see Theme20
 * @see Theme21
 * @see Theme22
 * @see Theme23
 * @see Theme24
 * @see Theme25
 *
 * @see ThemeBase
*/
object FormThemes {

    /**
     * Method, which return all themes as list
     */
    internal fun getThemes(): List<ThemeBase>
        = listOf(
                Theme1(), Theme2(), Theme3(), Theme4(), Theme5(),
                Theme6(), Theme7(), Theme8(), Theme9(), Theme10(),
                Theme11(), Theme12(), Theme13(), Theme14(), Theme15(),
                Theme16(), Theme17(), Theme18(), Theme19(), Theme20(),
                Theme21(), Theme22(), Theme23(), Theme24(), Theme25()
        )

    /**
     * Base theme, which describes base theme fields
     *
     */
    interface ThemeBase {
        /**
         * Primary color of theme
         */
        val colorPrimary: Int

        /**
         * Color of text in question view
         */
        val questionColor: Int

        /**
         * Color of text in answer view
         */
        val answerColor: Int

        /**
         * Background view color
         */
        val backgroundColor: Int

        /**
         * Checkboxes & Radio & Submit buttons background color
         */
        val buttonColor: Int

        /**
         * Checkboxes & Radio & Submit buttons text color
         */
        val buttonTextColor: Int

        /**
         * Progress bar background color
         */
        val progressbarColor: Int

        /**
         * Progress bar fill color
         */
        val progressbarFillColor: Int

        /**
         * Function, which repeats value for all channels
         *
         * For example:
         * repeatedValue(255)
         *      = Color.rgb(255, 255, 255)
         */
        fun repeatedValue(v: Int): Int = Color.rgb(v, v, v)
    }

    /**
     * Theme 1
     *
     * @see ThemeBase
     */
    internal class Theme1: ThemeBase {
        override val colorPrimary = Color.rgb(131, 184, 184)
        override val questionColor = Color.rgb(131, 184, 184)
        override val answerColor = Color.BLACK
        override val backgroundColor = Color.WHITE
        override val buttonColor = Color.rgb(131, 184, 184)
        override val buttonTextColor = Color.rgb(49, 86, 87)
        override val progressbarColor = Color.rgb(171, 172, 177)
        override val progressbarFillColor = Color.rgb(24, 27, 40)
    }

    /**
     * Theme 2
     *
     * @see ThemeBase
     */
    internal class Theme2: ThemeBase {
        override val colorPrimary = Color.rgb(187, 83, 84)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(29, 91, 44)
        override val backgroundColor = Color.rgb(158, 203, 143)
        override val buttonColor = Color.rgb(187, 83, 84)
        override val buttonTextColor = Color.rgb(66, 26, 27)
        override val progressbarColor = Color.rgb(109, 159, 105)
        override val progressbarFillColor = Color.rgb(29, 91, 44)
    }

    /**
     * Theme 3
     *
     * @see ThemeBase
     */
    internal class Theme3: ThemeBase {
        override val colorPrimary = Color.WHITE
        override val questionColor = Color.rgb(23, 81, 158)
        override val answerColor = Color.rgb(192, 49, 55)
        override val backgroundColor = Color.rgb(253, 211, 114)
        override val buttonColor = Color.WHITE
        override val buttonTextColor = repeatedValue(128)
        override val progressbarColor = Color.rgb(228, 152, 89)
        override val progressbarFillColor = Color.rgb(192, 49, 55)
    }

    /**
     * Theme 4
     *
     * @see ThemeBase
     */
    internal class Theme4: ThemeBase {
        override val colorPrimary = Color.WHITE
        override val questionColor = Color.WHITE
        override val answerColor = repeatedValue(51)
        override val backgroundColor = Color.rgb(241, 114, 89)
        override val buttonColor = Color.WHITE
        override val buttonTextColor = Color.rgb(128, 128, 128)
        override val progressbarColor = Color.rgb(167, 88, 73)
        override val progressbarFillColor = Color.rgb(29, 91, 44)
    }

    /**
     * Theme 5
     *
     * @see ThemeBase
     */
    internal class Theme5: ThemeBase {
        override val colorPrimary = Color.rgb(255, 98, 66)
        override val questionColor = Color.rgb(108, 75, 26)
        override val answerColor = Color.rgb(101, 75, 59)
        override val backgroundColor = Color.rgb(235, 224, 215)
        override val buttonColor = Color.rgb(255, 98, 66)
        override val buttonTextColor = Color.rgb(152, 0, 0)
        override val progressbarColor = Color.rgb(179, 169, 146)
        override val progressbarFillColor = Color.rgb(85, 75, 26)
    }

    /**
     * Theme 6
     *
     * @see ThemeBase
     */
    internal class Theme6: ThemeBase {
        override val colorPrimary = Color.rgb(254, 187, 156)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(244, 218, 185)
        override val backgroundColor = Color.rgb(80, 200, 160)
        override val buttonColor = Color.rgb(254, 187, 156)
        override val buttonTextColor = Color.rgb(145, 48, 0)
        override val progressbarColor = Color.rgb(139, 201, 172)
        override val progressbarFillColor = Color.rgb(255, 219, 207)
    }

    /**
     * Theme 7
     *
     * @see ThemeBase
     */
    internal class Theme7: ThemeBase {
        override val colorPrimary = Color.rgb(227, 230, 229)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(253, 253, 164)
        override val backgroundColor = Color.rgb(0, 165, 201)
        override val buttonColor = Color.rgb(227, 230, 229)
        override val buttonTextColor = Color.rgb(96, 106, 102)
        override val progressbarColor = Color.rgb(88, 192, 183)
        override val progressbarFillColor = Color.rgb(253, 253, 164)
    }

    /**
     * Theme 8
     *
     * @see ThemeBase
     */
    internal class Theme8: ThemeBase {
        override val colorPrimary = Color.rgb(0, 165, 201)
        override val questionColor = Color.rgb(67, 67, 67)
        override val answerColor = Color.rgb(0, 165, 201)
        override val backgroundColor = Color.rgb(227, 230, 229)
        override val buttonColor = Color.rgb(0, 165, 201)
        override val buttonTextColor = Color.rgb(161, 242, 255)
        override val progressbarColor = Color.rgb(145, 204, 217)
        override val progressbarFillColor = Color.rgb(0, 165, 201)
    }

    /**
     * Theme 9
     *
     * @see ThemeBase
     */
    internal class Theme9: ThemeBase {
        override val colorPrimary = Color.rgb(255, 86, 86)
        override val questionColor = Color.rgb(255, 86, 86)
        override val answerColor = repeatedValue(122)
        override val backgroundColor = Color.WHITE
        override val buttonColor = Color.rgb(255, 86, 86)
        override val buttonTextColor = Color.rgb(155, 0, 0)
        override val progressbarColor = repeatedValue(205)
        override val progressbarFillColor = repeatedValue(122)
    }

    /**
     * Theme 10
     *
     * @see ThemeBase
     */
    internal class Theme10: ThemeBase {
        override val colorPrimary = repeatedValue(12)
        override val questionColor = Color.WHITE
        override val answerColor = repeatedValue(197)
        override val backgroundColor = repeatedValue(12)
        override val buttonColor = repeatedValue(12)
        override val buttonTextColor = repeatedValue(191)
        override val progressbarColor = repeatedValue(85)
        override val progressbarFillColor = repeatedValue(197)
    }

    /**
     * Theme 11
     *
     * @see ThemeBase
     */
    internal class Theme11: ThemeBase {
        override val colorPrimary = Color.rgb(0, 154, 206)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(204, 218, 241)
        override val backgroundColor = repeatedValue(41)
        override val buttonColor = Color.rgb(0, 154, 206)
        override val buttonTextColor = Color.rgb(173, 237, 255)
        override val progressbarColor = Color.rgb(96, 101, 109)
        override val progressbarFillColor = Color.rgb(204, 218, 241)
    }

    /**
     * Theme 12
     *
     * @see ThemeBase
     */
    internal class Theme12: ThemeBase {
        override val colorPrimary = Color.rgb(255, 199, 19)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(255, 199, 19)
        override val backgroundColor = repeatedValue(41)
        override val buttonColor = Color.rgb(255, 199, 19)
        override val buttonTextColor = Color.rgb(135, 93, 0)
        override val progressbarColor = Color.rgb(114, 98, 52)
        override val progressbarFillColor = Color.rgb(255, 211, 78)
    }

    /**
     * Theme 13
     *
     * @see ThemeBase
     */
    internal class Theme13: ThemeBase {
        override val colorPrimary = Color.rgb(107, 142, 150)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(255, 155, 151)
        override val backgroundColor = Color.rgb(107, 142, 150)
        override val buttonColor = Color.rgb(107, 142, 150)
        override val buttonTextColor = Color.rgb(33, 46, 48)
        override val progressbarColor = Color.rgb(155, 142, 145)
        override val progressbarFillColor = Color.rgb(255, 155, 151)
    }

    /**
     * Theme 14
     *
     * @see ThemeBase
     */
    internal class Theme14: ThemeBase {
        override val colorPrimary = Color.rgb(242, 252, 50)
        override val questionColor = Color.rgb(242, 252, 50)
        override val answerColor = Color.WHITE
        override val backgroundColor = Color.rgb(0, 87, 56)
        override val buttonColor = Color.rgb(242, 252, 50)
        override val buttonTextColor = Color.rgb(129, 135, 0)
        override val progressbarColor = Color.rgb(89, 143, 124)
        override val progressbarFillColor = Color.WHITE
    }

    /**
     * Theme 15
     *
     * @see ThemeBase
     */
    internal class Theme15: ThemeBase {
        override val colorPrimary = Color.rgb(182, 0, 0)
        override val questionColor = Color.rgb(182, 0, 0)
        override val answerColor = repeatedValue(61)
        override val backgroundColor = Color.rgb(231, 188, 20)
        override val buttonColor = Color.rgb(182, 0, 0)
        override val buttonTextColor = Color.rgb(255, 161, 133)
        override val progressbarColor = Color.rgb(166, 139, 32)
        override val progressbarFillColor = repeatedValue(61)
    }

    /**
     * Theme 16
     *
     * @see ThemeBase
     */
    internal class Theme16: ThemeBase {
        override val colorPrimary = Color.rgb(217, 179, 135)
        override val questionColor = Color.WHITE
        override val answerColor = Color.WHITE
        override val backgroundColor = Color.rgb(0, 176, 197)
        override val buttonColor = Color.rgb(217, 179, 135)
        override val buttonTextColor = Color.rgb(132, 90, 40)
        override val progressbarColor = Color.rgb(89, 199, 212)
        override val progressbarFillColor = Color.WHITE
    }

    /**
     * Theme 17
     *
     * @see ThemeBase
     */
    internal class Theme17: ThemeBase {
        override val colorPrimary = repeatedValue(87)
        override val questionColor = Color.WHITE
        override val answerColor = repeatedValue(69)
        override val backgroundColor = Color.rgb(90, 216, 198)
        override val buttonColor = repeatedValue(87)
        override val buttonTextColor = repeatedValue(202)
        override val progressbarColor = Color.rgb(80, 159, 148)
        override val progressbarFillColor = repeatedValue(69)
    }

    /**
     * Theme 18
     *
     * @see ThemeBase
     */
    internal class Theme18: ThemeBase {
        override val colorPrimary = Color.rgb(0, 206, 234)
        override val questionColor = Color.rgb(0, 206, 234)
        override val answerColor = repeatedValue(97)
        override val backgroundColor = Color.WHITE
        override val buttonColor = Color.rgb(0, 206, 234)
        override val buttonTextColor = Color.rgb(0, 92, 109)
        override val progressbarColor = repeatedValue(197)
        override val progressbarFillColor = repeatedValue(97)
    }

    /**
     * Theme 19
     *
     * @see ThemeBase
     */
    internal class Theme19: ThemeBase {
        override val colorPrimary = repeatedValue(82)
        override val questionColor = Color.rgb(0, 206, 234)
        override val answerColor = repeatedValue(97)
        override val backgroundColor = Color.WHITE
        override val buttonColor = repeatedValue(82)
        override val buttonTextColor = repeatedValue(197)
        override val progressbarColor = repeatedValue(191)
        override val progressbarFillColor = repeatedValue(82)
    }

    /**
     * Theme 20
     *
     * @see ThemeBase
     */
    internal class Theme20: ThemeBase {
        override val colorPrimary = Color.rgb(255, 119, 247)
        override val questionColor = Color.WHITE
        override val answerColor = Color.rgb(255, 201, 255)
        override val backgroundColor = Color.rgb(255, 0, 230)
        override val buttonColor = Color.rgb(255, 119, 247)
        override val buttonTextColor = Color.rgb(131, 0, 126)
        override val progressbarColor = Color.rgb(248, 70, 233)
        override val progressbarFillColor = Color.rgb(255, 201, 255)
    }

    /**
     * Theme 21
     *
     * @see ThemeBase
     */
    internal class Theme21: ThemeBase {
        override val colorPrimary = Color.rgb(136, 255, 55)
        override val questionColor = Color.rgb(0, 61, 60)
        override val answerColor = Color.rgb(0, 61, 60)
        override val backgroundColor = Color.rgb(136, 255, 55)
        override val buttonColor = Color.rgb(136, 255, 55)
        override val buttonTextColor = Color.rgb(49, 143, 0)
        override val progressbarColor = Color.rgb(87, 184, 56)
        override val progressbarFillColor = Color.rgb(0, 61, 60)
    }

    /**
     * Theme 22
     *
     * @see ThemeBase
     */
    internal class Theme22: ThemeBase {
        override val colorPrimary = repeatedValue(222)
        override val questionColor = Color.WHITE
        override val answerColor = repeatedValue(61)
        override val backgroundColor = Color.rgb(173, 132, 171)
        override val buttonColor = repeatedValue(222)
        override val buttonTextColor = repeatedValue(95)
        override val progressbarColor = Color.rgb(129, 104, 128)
        override val progressbarFillColor = repeatedValue(61)
    }

    /**
     * Theme 23
     *
     * @see ThemeBase
     */
    internal class Theme23: ThemeBase {
        override val colorPrimary = Color.rgb(226, 236, 167)
        override val questionColor = Color.WHITE
        override val answerColor = colorPrimary
        override val backgroundColor = Color.rgb(167, 200, 115)
        override val buttonColor = colorPrimary
        override val buttonTextColor = Color.rgb(109, 119, 24)
        override val progressbarColor = Color.rgb(182, 207, 130)
        override val progressbarFillColor = colorPrimary
    }

    /**
     * Theme 24
     *
     * @see ThemeBase
     */
    internal class Theme24: ThemeBase {
        override val colorPrimary = Color.rgb(195, 33, 44)
        override val questionColor = Color.BLACK
        override val answerColor = Color.WHITE
        override val backgroundColor = colorPrimary
        override val buttonColor = colorPrimary
        override val buttonTextColor = Color.rgb(249, 220, 221)
        override val progressbarColor = Color.rgb(209, 109, 116)
        override val progressbarFillColor = Color.WHITE
    }

    /**
     * Theme 25
     *
     * @see ThemeBase
     */
    internal class Theme25: ThemeBase {
        override val colorPrimary = Color.rgb(0, 189, 71)
        override val questionColor = Color.BLACK
        override val answerColor = Color.WHITE
        override val backgroundColor = colorPrimary
        override val buttonColor = colorPrimary
        override val buttonTextColor = Color.rgb(215, 247, 224)
        override val progressbarColor = Color.rgb(89, 207, 133)
        override val progressbarFillColor = Color.WHITE
    }
}