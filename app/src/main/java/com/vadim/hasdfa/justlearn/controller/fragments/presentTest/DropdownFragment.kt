package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.QDropdown
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.question_dropdown.*

/**
* Created by Raksha Vadim on 14.08.17.
*/
class DropdownFragment: QuestionFragmentBase() {

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        var error: Error? = null

        if (answer.answerText?.isEmpty() != false) {
            error = Error(context().resources.getString(R.string.answer_not_selected, mQuestion.question))
        }

        return Pair(answer, error)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.question_dropdown, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mQuestion.type != QType.Dropdown) throw IllegalArgumentException("Incorrect type")
        val q = mQuestion as? QDropdown ?: throw ClassCastException("Incorrect type: ${mQuestion.javaClass.name}")

        headerTextView.text = q.question
        configureSpinner(dropdown, q.items, {
            answer.answerText = q.items[it]
        })
        helperTextView.text = mQuestion.helpText

        view.setOnClickListener { showMore() }
        headerTextView.setOnClickListener {
            showMore()
        }

        Log.d("myLog", "$themeId")
        if (themeId >= 0) {
            val theme = FormThemes.getThemes()[themeId]

            Log.d("myLog", "$theme")
            helperTextView.setTextColor(theme.questionColor)
            headerTextView.setTextColor(theme.questionColor)
            colorizeSpinnerItem(dropdown, dropdown.selectedItemPosition)

            (view as? CardView)?.let {
                it.setCardBackgroundColor(Color.alpha(0))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.elevation = 0f
                }
                it.radius = 0f
            }
        }
    }

    private fun configureSpinner(spinnerSelectTopic: Spinner, array: MutableList<String>, onItemSelected: (Int) -> Unit){
        if (array[0] != "- Select -")
            array.add(0, "- Select -")
        spinnerSelectTopic.adapter = ArrayAdapter<String>(
                spinnerSelectTopic.context,
                R.layout.spinner_item,
                R.id.spinner_item_text_view,
                array
        )

        colorizeSpinnerItem(spinnerSelectTopic, spinnerSelectTopic.selectedItemPosition)
        spinnerSelectTopic.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterView: AdapterView<*>?) {
                colorizeSpinnerItem(spinnerSelectTopic, spinnerSelectTopic.selectedItemPosition)
            }
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0)
                    onItemSelected(position - 1)
                else
                    answer.answerText = null

                colorizeSpinnerItem(spinnerSelectTopic, position)
                submit()
            }
        }
    }

    private fun colorizeSpinnerItem(spinnerSelectTopic: Spinner, position: Int){
        var textColor = Color.BLACK
        var backColor = Color.WHITE
        if (themeId in 0 until FormThemes.getThemes().count()) {
            FormThemes.getThemes()[themeId].let {
                if (position > 0) {
                    textColor = it.answerColor
                    backColor = it.backgroundColor
                } else {
                    textColor = it.backgroundColor
                    backColor = it.questionColor
                }
            }
        }

        spinnerSelectTopic
                .findViewById<TextView?>(R.id.spinner_item_text_view)
                ?.setTextColor(textColor)
        spinnerSelectTopic
                .findViewById<RelativeLayout?>(R.id.backRL)
                ?.setBackgroundColor(backColor)
    }
}