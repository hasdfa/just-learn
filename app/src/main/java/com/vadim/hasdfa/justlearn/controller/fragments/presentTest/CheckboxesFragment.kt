package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.widget.AppCompatCheckBox
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.QCheckbox
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.question_checkboxes.*

/**
* Created by Raksha Vadim on 14.08.17.
*/
class CheckboxesFragment: QuestionFragmentBase() {

    private var isShowed = false

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        return Pair(answer, if(isShowed) null else Error())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.question_checkboxes, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mQuestion.type != QType.Checkbox) throw Exception("Incorrect type")
        val q = mQuestion as? QCheckbox ?:
                throw ClassCastException("Incorrect type: ${mQuestion.javaClass.name}")

        var theme: FormThemes.ThemeBase? = null

        headerTextView.text = q.question
        helperTextView.text = q.helpText

        view.setOnClickListener { showMore() }
        headerTextView.setOnClickListener {
            showMore()
        }

        Log.d("myLog", "$themeId")
        if (themeId >= 0) {
            theme = FormThemes.getThemes()[themeId]
            Log.d("myLog", "$theme")
            helperTextView.setTextColor(theme.questionColor)
            headerTextView.setTextColor(theme.questionColor)
            styleCardView()
        }
        checkboxGroup.removeAllViews()
        for (i in 0 until q.items.size) {
            val checkBox = AppCompatCheckBox(context)
            checkBox.text = q.items[i]
            checkBox.id = i
            checkBox.textSize = context().resources.getDimension(R.dimen.answer_text_size)

            checkBox.setTheme(theme)
            checkboxGroup.addCheckBox(checkBox)
        }
        answer.answerArray = arrayListOf()
        checkboxGroup.onCheckListener{ checkBox, b ->
            if (b) answer.answerArray!!.add(checkBox.text.toString())
            else answer.answerArray!!.remove(checkBox.text.toString())
            submit()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isShowed)
            isShowed = true
        submit()
    }

    private fun AppCompatCheckBox.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return

        setTextColor(theme.answerColor)

        val colorStateList = ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                intArrayOf(theme.questionColor, theme.questionColor)
        )
        CompoundButtonCompat.setButtonTintList(this, colorStateList)

        invalidate()
    }
}