package com.vadim.hasdfa.justlearn.core.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.util.TypedValue
import android.view.ContextThemeWrapper
import android.view.View
import com.vadim.hasdfa.justlearn.R

/**
 * Base functions, which implements in Activity & Fragment
 */
internal interface BaseFunctions {
    /**
     * Context, which must be implemented by child class
     */
    fun nullableContext(): Context?
    fun context(): Context = nullableContext()!!

            /**
     * Intent result codes for onActivityResult()
     */
    object IntentCodes {
        val REQUEST_GET_T:   Int get(){return 1}
        val REQUEST_GET_Q:   Int get(){return 2}
        val REQUEST_GET_S:   Int get(){return 3}
        val RESULT_REDIRECT: Int get(){return 302}
    }

    // MARK: UI helpers

    /**
     * Show alert with message, which describes error,
     * or if it is null with title "Something wrong.".
     * Provides on dismiss callback
     */
    fun showError(localizedMessage: String?, onDismiss: () -> Unit = {}) {
        if (nullableContext() == null) return

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(context(), R.style.AlertDialogCustom))
        var alert: AlertDialog? = null
        val messages = localizedMessage ?: "Something wrong."
        alertDialog.setTitle(messages)
        alertDialog.setPositiveButton(str(R.string.ok), { _, _ ->
            alert?.dismiss()
            onDismiss()
        })
        alertDialog.setOnDismissListener { onDismiss() }
        alert = alertDialog.create()

        if ((context() as ActivityBase).isFinishing) return
        alert.show()
    }

    /**
     * Show alert with message
     * Provides on ok and on cancel callbacks
     */
    @SuppressLint("RestrictedApi")
    fun showAlert(title: String, message: String? = null, onOk: ((AlertDialog) -> Unit)? = null, onCancel: ((AlertDialog) -> Unit)? = null, onDismiss: () -> Unit = {}){
        if (nullableContext() == null) return

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(context(), R.style.AlertDialogCustom))
        var alert: AlertDialog? = null
        alertDialog.setTitle(title)
        message?.let { alertDialog.setMessage(it) }
        onOk?.let {
            alertDialog.setPositiveButton(str(R.string.ok), { _, _ ->
                alert?.dismiss()
                onOk(alert!!)
            })
        }
        onCancel?.let {
            alertDialog.setNegativeButton(str(R.string.cancel), { _, _ ->
                alert?.dismiss()
                onCancel(alert!!)
            })
        }
        alertDialog.setOnDismissListener {
            onDismiss()
        }
        alert = alertDialog.create()
        alert.show()
    }


    val loading: View

    open fun load(isLoading: Boolean) {
        loading.visibility = if(isLoading) View.VISIBLE else View.INVISIBLE
    }


    fun showLongText(message: String, onOk: (AlertDialog) -> Unit = {}){
        if (nullableContext() == null) return

        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(context(), R.style.AlertDialogCustom))
        var alert: AlertDialog? = null
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(str(R.string.ok), { _, _ ->
            alert?.dismiss()
            onOk(alert!!)
        })
        alert = alertDialog.create()
        alert.show()
    }

    /**
     * Compute adaptive Grid for device screen size
     *
     * For mobile:
     *      Portrait: 3 columns
     *      Landscape: 4 columns
     * Tablet:
     *      Portrait: 3 columns
     *      Landscape: 4 columns
     * Medium size tablets:
     *      Portrait: 4 columns
     *      Landscape: 5 columns
     * Large size tablets:
     *      Portrait: 5 columns
     *      Landscape: 6 columns
     */
    fun getAdaptiveGridLM(): GridLayoutManager{
        val columnCount = when {
            isTablet    -> if (isLandscape) 4 else 3
            isMidTablet -> if (isLandscape) 5 else 4
            isBigTablet -> if (isLandscape) 6 else 5
            isLandscape -> 4
            else -> 3
        }
        return GridLayoutManager(context(), columnCount)
    }

    /**
     * Set need to update label state by use reflection methods
     *
     *  @param TIL Something, which inherits from {@link TextInputLayout}
     */
    fun<TIL: TextInputLayout> updateLabelState(textInputLayout: TIL) {
        val updateLabelState = textInputLayout.javaClass
                .getDeclaredMethod("updateLabelState", Boolean::class.javaPrimitiveType)
        updateLabelState.isAccessible = true
        updateLabelState.invoke(textInputLayout, true)
    }

    /**
     * Set focused text color by use reflection methods
     *
     * @param TIL Something, which inherits from {@link TextInputLayout}
     *
     * @param color Which color we want to set
     * @param textInputLayout Which color we want to change
     */
    fun<TIL: TextInputLayout> setFocusedTextColor(textInputLayout: TIL, @ColorInt color: Int) {
        val mFocusedTextColor = textInputLayout.javaClass.getDeclaredField("mFocusedTextColor")
        mFocusedTextColor.isAccessible = true

        val colors = ColorStateList.valueOf(color)
        mFocusedTextColor.set(textInputLayout, colors)
    }

    /**
     * Customize {@link TextInputLayout} color by use reflection
     *
     * @param color Which color we want to set
     */
    fun<EditText:  android.widget.EditText> setInputTextLayoutColor(color: Int, textInputLayout: TextInputLayout?, editText: EditText? = null) {
        ViewCompat.setBackgroundTintList(editText, ColorStateList.valueOf(color))

        try {
            val mDefaultTextColor = TextInputLayout::class.java.getDeclaredField("mDefaultTextColor")
            mDefaultTextColor.isAccessible = true
            mDefaultTextColor.set(textInputLayout, ColorStateList.valueOf(color))

            textInputLayout?.let {
                setFocusedTextColor(textInputLayout, color)
                updateLabelState(textInputLayout)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Simple check if all items is not empty
     *
     * @param items
     * @param function Callback on end of function with results
     */
    fun checkItems(items: List<String>, function: (Boolean, String) -> Unit) {
        var isSuccess = true
        var errMsg = ""
        if (items.isEmpty()) {
            isSuccess = false
            errMsg = str(R.string.items_are_empty)
        } else {
            items
                .filter { it.isEmpty() }
                .forEach {
                    isSuccess = false
                    errMsg = str(R.string.items_are_empty)
                    return@forEach
                }
        }
        function(isSuccess, errMsg)
    }


    /**
     * Customize {@link TextInputLayout} color by use reflection
     *
     * @param color Which color we want to set
     * @param textInputLayout Which color we want to change
     */
    fun setUpperHintColor(textInputLayout: TextInputLayout, color: Int) {
        try {
            setFocusedTextColor(textInputLayout, color)

            updateLabelState(textInputLayout)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Return configuration from resources of this context
     */
    val configuration: Configuration
        get() = context().resources.configuration

    /**
     * Return true if device is in landscape orientation
     * and false if it in portrait
     */
    val isLandscape: Boolean
        get() = with(configuration) { screenWidthDp > screenHeightDp }

    /**
     * Return true if device dimension like tablet
     */
    val isTablet: Boolean get() = configuration.smallestScreenWidthDp >= 620

    /**
     * Return true if device dimension like small tablet
     */
    val isSmallTablet: Boolean get() = configuration.smallestScreenWidthDp in 620..700

    /**
     * Return true if device dimension mid tablet
     */
    val isMidTablet: Boolean get() = configuration.smallestScreenWidthDp in 700..799

    /**
     * Return true if device dimension like big tablet
     */
    val isBigTablet: Boolean get() = configuration.smallestScreenWidthDp >= 800

    /**
     * Return width of device in dp
     */
    val width: Int get() = configuration.screenWidthDp

    /**
     * Return height of device in dp
     */
    val height: Int get() = configuration.screenHeightDp

    /**
     * Return pixels value in DP
     */
    fun getPixels(size: Float, unit: Int): Float {
        val metrics = context().resources.displayMetrics
        return TypedValue.applyDimension(unit, size, metrics)
    }

    /**
     * Myself NaN implementation for {@link Int}
     */
    fun Int.stringOrNaN(): String {
        if (this <= 0) {
            return "NaN"
        }
        return "$this"
    }


    /**
     * @param colorId Color resource id
     *
     * @return color from resource, which found by id
     */
    @ColorInt fun color(@ColorRes colorId: Int): Int = ContextCompat.getColor(context(), colorId)

    /**
     * @param stringId String resource id
     *
     * @return string from resource, which found by id
     */
    fun str(@StringRes stringId: Int): String = context().resources.getString(stringId)

    /**
     * @param drawableId Drawable resource id
     *
     * @return drawable from resource, which found by id
     */
    fun drawable(@DrawableRes drawableId: Int): Drawable? = ContextCompat.getDrawable(context(), drawableId)

    /** Color from resources, founded by id: R.color.colorPrimary */
    val colorPrimary        : Int get() = color(R.color.colorPrimary)

    /** Color from resources, founded by id: R.color.colorPrimarySelected */
    val colorPrimarySelected: Int get() = color(R.color.colorPrimarySelected)

    /** Color from resources, founded by id: R.color.colorPrimaryDark */
    val colorPrimaryDark    : Int get() = color(R.color.colorPrimaryDark)

    /** Color from resources, founded by id: R.color.colorPrimaryLight */
    val colorPrimaryLight   : Int get() = color(R.color.colorPrimaryLight)

    /** Color from resources, founded by id: R.color.colorAccent */
    val colorAccent         : Int get() = color(R.color.colorAccent)

    /** Color from resources, founded by id: R.color.colorGreen */
    val colorGreen          : Int get() = color(R.color.colorGreen)

    /** Color from resources, founded by id: R.color.colorGreenNoEnabled */
    val colorGreenNoEnabled : Int get() = color(R.color.colorGreenNoEnabled)
}