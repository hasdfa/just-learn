package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.adapters.testAdapter.PointTestAdapter
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.Test
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.model.questions.QCheckbox
import com.vadim.hasdfa.justlearn.model.questions.QDropdown
import com.vadim.hasdfa.justlearn.model.questions.QRadioButtons
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import kotlinx.android.synthetic.main.activity_create_test.*

/**
* Created by Raksha Vadim on 09.08.17, 01:42.
*/
class PoinitingTestActivity: ActivityBase() {

    private var newTest: Test = Test()
    private var mAdapter: PointTestAdapter = PointTestAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_test)
        newTest = intent.extras.getParcelable("test")!!

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mRecyclerView.layoutManager = LinearLayoutManager(this)

        mAdapter = PointTestAdapter(newTest)
        mRecyclerView.adapter = mAdapter
        mAdapter.afterUpdate = {
            checkAnswers(false) { saveImageView.setImageDrawable(drawable(R.drawable.ic_save)) }
        }

        saveImageView.setOnClickListener {
            checkAnswers {
                load(true)
                FirebaseDatabaseHelper.postNewTest(newTest, {
                    /* Success */
                    load(false)
                    showAlert(str(R.string.answer_saved), onOk = {
                        setResult(Activity.RESULT_OK)
                        supportFinishAfterTransition()
                    })
                }, {
                    /* Failure */
                    load(false)
                    showError(it)
                })
            }
        }
        editThemeImageView.visibility = View.INVISIBLE
        testSettings.visibility = View.INVISIBLE

        addQuestionFab?.hide()
    }

    override fun onStart() {
        super.onStart()
        mAdapter.notifyDataSetChanged()
    }

    private fun checkAnswers(shouldShowError: Boolean = true, function: () -> Unit) {
        var isSuccess = true
        var errorDescription = ""

        val anyIsEmpty = newTest.questions
                .any {
                    when (it) {
                        is QDropdown -> it.answer.isEmpty()
                        is QRadioButtons -> it.answer.isEmpty()
                        is QCheckbox -> it.answers.isEmpty()
                        else -> false
                    }
                }
        if (anyIsEmpty) {
            isSuccess = false
            errorDescription = "You must answer all questions"
        }

        val default = arrayOf(QType.Dropdown, QType.RadioBtn, QType.Checkbox)
        val anyHaveNoPoint = newTest.questions
                .any { it.type in default && it.getValue() == null }
        if (anyHaveNoPoint) {
            isSuccess = false
            errorDescription = "You must select value for all questions"
        }

        if (isSuccess) {
            function()
        } else {
            if (shouldShowError)
                showError(errorDescription)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable("test", newTest)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            newTest = it.getParcelable("test")
        }
    }
}