package com.vadim.hasdfa.justlearn.controller.adapters.testAdapter

import android.annotation.SuppressLint
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import com.vadim.hasdfa.justlearn.R

/**
 * Created by Raksha Vadim on 21.01.2018.
 * With love.
 * In project JustLearn.
 */
class PreviewCreateTestAdapter(private val groups: Array<String>, val enabled: (Boolean) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val selected: Array<Boolean> = Array(groups.size, { false })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
        = if (viewType == 0) FirstItem(LayoutInflater.from(parent.context).inflate(R.layout.item_question_default, parent, false))
        else SelectableItem(LayoutInflater.from(parent.context).inflate(R.layout.item_selectable, parent, false))

    override fun getItemCount() = groups.size + 1
    override fun getItemViewType(position: Int) = if (position == 0) 0 else 1

    var isContainsTitle = false
    var isSelectedMoreOne = false

    fun getResults(): Pair<String, Array<String>> {
        return title to groups.filterIndexed { i, _ -> selected[i] }.toTypedArray()
    }

    var title = ""

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val context = holder.itemView?.context
        val string: (id: Int) -> String? = { context?.resources?.getString(R.string.title) }

        if (holder is FirstItem) {
            holder.contentEditText.setText(title)
            holder.contentInputLayout.hint = string(R.string.title)
            holder.contentEditText.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    isContainsTitle = p0?.count() ?: 0 >= 3
                    enabled(isContainsTitle && isSelectedMoreOne)

                    title = p0.toString()
                }
            })
        } else if (holder is SelectableItem) {
            val at = holder.adapterPosition - 1

            holder.checkBox.text = groups[at]

            holder.checkBox.isChecked = selected[at]
            holder.button.setOnClickListener {
                selected[at] = !selected[at]
                holder.checkBox.isChecked = selected[at]

                isSelectedMoreOne = selected.any { it }
                enabled(isContainsTitle && isSelectedMoreOne)
            }
        }
    }

    fun selectGroups(groups: MutableList<String>) {
        groups.forEach {
            val index = this.groups.indexOf(it)
            selected[index] = true
            notifyItemChanged(index + 1)
        }
    }


    fun changeTitle(title: String) {
        this.title = title
        notifyItemChanged(0)
    }

    inner class FirstItem(val view: View): RecyclerView.ViewHolder(view) {
        val contentInputLayout = view.findViewById<TextInputLayout>(R.id.contentInputLayout)
        val contentEditText = view.findViewById<TextInputEditText>(R.id.contentEditText)
    }

    inner class SelectableItem(val view: View): RecyclerView.ViewHolder(view) {
        val checkBox = view.findViewById<CheckBox>(R.id.checkbox)
        val button = view.findViewById<Button>(R.id.button)
    }
}