package com.vadim.hasdfa.justlearn.controller.activity.profile

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.activity.login.SignInActivity
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import kotlinx.android.synthetic.main.activity_profile.*

/**
* Created by Raksha Vadim on 08.08.17, 15:20.
* With love.
* In project JustLearn.
*/
class ProfileActivity : ActivityBase() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = ""
        }
        mRecyclerView.isNestedScrollingEnabled = false
        mRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onStart() {
        super.onStart()
        FirebaseAuth.getInstance().currentUser?.let { user ->
            nameTextView.text = user.displayName
            emailTextView.text = user.email
            signOutButton.setOnClickListener {
                FirebaseDatabaseHelper.clearAllListeners()
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this@ProfileActivity, SignInActivity::class.java))
                finish()
            }
            resetPasswordButton.setOnClickListener {
                FirebaseAuth.getInstance().sendPasswordResetEmail(user.email!!)
                Toast.makeText(
                        this@ProfileActivity,
                        str(R.string.reset_password_action),
                        Toast.LENGTH_LONG
                ).show()
            }

            FirebaseDatabaseHelper.getUserData(user, { student ->
                // Student
                nameTextView.text = student.name
                topicTypeTextView.text = student.topic
                helperTextView.text = str(R.string.score_average)
            }, { teacher ->
                // Teacher
                nameTextView.text = teacher.name
                topicTypeTextView.text = str(R.string.teacher)
                helperDetailTextView.text = teacher.isAccepted.toString()
            }, {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, SignInActivity::class.java))
                finish()
            })
        }
    }
}