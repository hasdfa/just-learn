package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.fragments.createTest.SelectQuestionTypeFragment
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.core.base.BaseFunctions
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase

/**
* Created by Raksha Vadim on 09.08.17, 00:52.
*/
class SelectTypeActivity: ActivityBase(), SelectQuestionTypeFragment.OnItemSelected {

    override fun onSelected(p: Int) {
        val b = Bundle()
        b.putParcelable("q", QBase.getQByPosition(p))
        val intent = Intent(this, CreateQuestionActivity::class.java)
        intent.putExtras(b)
        startActivityForResult(intent, BaseFunctions.IntentCodes.RESULT_REDIRECT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data?.hasExtra("result") == true) {
            val q: QBase? = data.getParcelableExtra("result")
            Log.d("myLog", "RESULT_OK\nQ:$q")

            if (q == null) {
                showError("") {
                    finish()
                }
                return
            }

            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = str(R.string.select_question_type)

        if (savedInstanceState == null) {
            val fragment = SelectQuestionTypeFragment()
            fragment.arguments = intent.extras
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.mFrame, fragment)
                    .commit()
        }
    }
}