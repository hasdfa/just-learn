package com.vadim.hasdfa.justlearn.core.base

import android.content.Context
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.vadim.hasdfa.justlearn.model.firebase.FirebaseDatabaseHelper
import com.vadim.hasdfa.justlearn.view.LoadingPreview

/**
 * Simple fragment, which implements base functions
 * And which must be superclass for all fragments in this module
 */
open class FragmentBase: Fragment(), BaseFunctions {

    override fun nullableContext(): Context? {
        return context ?: activity
    }

    override val loading: View by lazy {
        LoadingPreview.addToParentOf(activity as ActivityBase)
    }

    /**
     * Clear all listeners, when activity stops
     *
     * Lifecycle element
     */
    override fun onStop() {
        super.onStop()
        if (FirebaseDatabaseHelper.clearAllListeners()) {
            Log.d("myLog", "Success removed all listeners")
        } else {
            Log.e("myLog", "Error removed all listeners")
        }
    }
}