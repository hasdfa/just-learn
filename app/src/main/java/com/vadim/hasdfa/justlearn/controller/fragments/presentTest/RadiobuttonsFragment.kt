package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.widget.AppCompatRadioButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.QRadioButtons
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.question_radiobtns.*

/**
* Created by Raksha Vadim on 14.08.17.
*/
class RadiobuttonsFragment : QuestionFragmentBase() {

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        var error: Error? = null

        if (answer.answerText?.isEmpty() != false) { // true or null
            error = Error("Answer for '${mQuestion.question}' isn`t selected")
        }

        return Pair(answer, error)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.question_radiobtns, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mQuestion.type != QType.RadioBtn) throw Exception("Incorrect type")
        val q = mQuestion as? QRadioButtons ?: throw ClassCastException("Incorrect type: ${mQuestion.javaClass.name}")

        headerTextView.text = q.question
        helperTextView.text = q.helpText

        view.setOnClickListener { showMore() }
        headerTextView.setOnClickListener {
            showMore()
        }

        var theme: FormThemes.ThemeBase? = null

        Log.d("myLog", "$themeId")
        if (themeId >= 0) {
            theme = FormThemes.getThemes()[themeId]
            Log.d("myLog", "$theme")
            helperTextView.setTextColor(theme.questionColor)
            headerTextView.setTextColor(theme.questionColor)
            styleCardView()
        }
        radioGroup.removeAllViews()
        for (i in 0 until q.items.size) {
            val radioButton = AppCompatRadioButton(context)
            radioButton.text = q.items[i]
            radioButton.id = i
            radioButton.setTheme(theme)
            radioButton.textSize = context().resources.getDimension(R.dimen.answer_text_size)


//            radioButton.textSize = getPixels(12f, TypedValue.COMPLEX_UNIT_SP)
            val rParams = RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT)
            radioGroup.addView(radioButton, rParams)
        }
        radioGroup.setOnCheckedChangeListener { _, i ->
            answer.answerText = q.items[i]
            submit()
        }
    }


    @SuppressLint("RestrictedApi")
    private fun AppCompatRadioButton.setTheme(theme: FormThemes.ThemeBase?) {
        if (theme == null) return

        setTextColor(theme.answerColor)

        val colorStateList = ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                intArrayOf(theme.questionColor, theme.questionColor)
        )
        CompoundButtonCompat.setButtonTintList(this, colorStateList)

        invalidate()
    }
}