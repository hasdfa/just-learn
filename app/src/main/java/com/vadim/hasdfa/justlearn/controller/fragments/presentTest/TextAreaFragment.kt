package com.vadim.hasdfa.justlearn.controller.fragments.presentTest

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.CardView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.core.base.QuestionFragmentBase
import com.vadim.hasdfa.justlearn.model.Answer
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import com.vadim.hasdfa.justlearn.utils.FormThemes
import kotlinx.android.synthetic.main.question_text_area.*

/**
* Created by Raksha Vadim on 14.08.17.
*/
class TextAreaFragment : QuestionFragmentBase() {

    /**
     * Implementation of base method
     * Return data from ui elements in this fragment
     *
     * @see QuestionFragmentBase
     */
    override fun getData(): Pair<Answer, Error?> {
        var error: Error? = null

        if (answer.answerText?.isEmpty() != false) {
            error = Error(context().resources.getString(R.string.answer_not_selected, mQuestion.question))
        } else if (answer.answerText!!.count() < 5) {
            error = Error(context().resources.getString(R.string.answer_more_than, 5))
        }

        return Pair(answer, error)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
         = inflater.inflate(R.layout.question_text_area, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mQuestion.type != QType.TextArea) throw Exception("Incorrect type")

        headerTextView.text = mQuestion.question
        helperTextView.text = mQuestion.helpText

        view.setOnClickListener { showMore() }
        headerTextView.setOnClickListener {
            showMore()
        }

        contentTextView.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val size = s?.length ?: 0
                counterTextView.text = "$size"

                answer.answerText = s?.toString()

                submit()
            }
        })
        contentTextView.clearFocus()

        Log.d("myLog", "$themeId")
        if (themeId >= 0) {
            val theme = FormThemes.getThemes()[themeId]

            headerTextView.setTextColor(theme.questionColor)
            contentTextView.setTextColor(theme.answerColor)
            helperTextView.setTextColor(theme.questionColor)
            contentTextView.setHintTextColor(theme.questionColor)
            counterTextView.setTextColor(theme.answerColor)
            divTextView.setTextColor(theme.answerColor)
            maxSymbolsTextView.setTextColor(theme.answerColor)

            ViewCompat.setBackgroundTintList(contentTextView, ColorStateList.valueOf(theme.questionColor))
            (view as? CardView)?.let {
                it.setCardBackgroundColor(Color.alpha(0))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.elevation = 0f
                }
                it.radius = 0f
            }
        }
    }
}