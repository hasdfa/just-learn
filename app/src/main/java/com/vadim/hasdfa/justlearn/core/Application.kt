package com.vadim.hasdfa.justlearn.core

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.support.multidex.MultiDex

/**
* Base Application class
*/
class Application: android.app.Application() {

    companion object {
        /**
         * This method allows to run some block of code in main thread
         */
        fun runOnMainThread(actions: () -> Unit) = Handler(Looper.getMainLooper()).postAtFrontOfQueue(actions)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}