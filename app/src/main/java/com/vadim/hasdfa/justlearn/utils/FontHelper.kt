package com.vadim.hasdfa.justlearn.utils

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Raksha Vadim on 15.03.2018.
 * With love.
 * In project JustLearn.
 */
object FontHelper {

    fun badScriptRegular(context: Context): Typeface
            = Typeface.createFromAsset(context.assets, "fonts/BadScript-Regular.ttf")

}