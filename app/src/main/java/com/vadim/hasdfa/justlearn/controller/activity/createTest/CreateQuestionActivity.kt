package com.vadim.hasdfa.justlearn.controller.activity.createTest

import android.os.Bundle
import com.vadim.hasdfa.justlearn.R
import com.vadim.hasdfa.justlearn.controller.fragments.createTest.CreateQuestionFragment
import com.vadim.hasdfa.justlearn.controller.fragments.createTest.SelectQuestionTypeFragment
import com.vadim.hasdfa.justlearn.core.base.ActivityBase
import com.vadim.hasdfa.justlearn.model.questions.*
import com.vadim.hasdfa.justlearn.model.questions.utils.QBase
import com.vadim.hasdfa.justlearn.model.questions.utils.QType
import kotlinx.android.synthetic.main.activity_create_question.*

/**
* Created by Raksha Vadim on 09.08.17, 23:32.
* With love.
* In project JustLearn.
*/
class CreateQuestionActivity: ActivityBase(), SelectQuestionTypeFragment.OnItemSelected {

    private var fragmentConstructor: CreateQuestionFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_question)
        setSupportActionBar(findViewById(R.id.toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = str(R.string.create_new_question)

        if (savedInstanceState == null) {
            if (isTablet) {
                fragmentConstructor = CreateQuestionFragment()
                fragmentConstructor?.arguments = Bundle()
                fragmentConstructor?.arguments?.putParcelable("q", QText())

                supportFragmentManager
                        .beginTransaction()
                        .add(R.id.questionConstructorFrame, fragmentConstructor)
                        .commit()

                val fragment = SelectQuestionTypeFragment()
                fragment.arguments = Bundle().also { it.putInt("selectedItem", 0) }

                supportFragmentManager
                        .beginTransaction()
                        .add(R.id.questionSelectConstructorFrame, fragment)
                        .commit()
            } else {
                fragmentConstructor = CreateQuestionFragment()
                fragmentConstructor?.arguments = intent.extras

                supportFragmentManager
                        .beginTransaction()
                        .add(R.id.questionConstructorFrame, fragmentConstructor)
                        .commit()
            }
        }

        saveImageView.setOnClickListener {
            fragmentConstructor?.getData()?.let { q ->
                checkData(q) {
                    intent.putExtra("result", it)
                    setResult(RESULT_OK, intent)
                    finish()
                }
            }
        }
    }

    private fun checkData(newQ: QBase, function: (QBase) -> Unit) {
        var isSuccess = true
        var errMsg = ""

        with (newQ) {
            when {
                type == QType.Null -> {
                    isSuccess = false
                    errMsg = str(R.string.q_is_null)
                }
                question.isEmpty() -> {
                    isSuccess = false
                    errMsg = str(R.string.q_is_empty)
                }
                helpText.isEmpty() -> {
                    helpText = when (type) {
                        QType.Text -> str(R.string.enter_answer)
                        QType.TextArea -> str(R.string.enter_answer)
                        QType.Dropdown -> str(R.string.chose_one)
                        QType.RadioBtn -> str(R.string.chose_one)
                        QType.Checkbox -> str(R.string.chose_all_correct)
                        QType.Grid -> throw NotImplementedError()
                        QType.Stack -> throw NotImplementedError()
                        QType.Null -> throw NotImplementedError()
                    }
                }
                this is QDropdown -> checkItems(items, { _1, _2 ->
                    isSuccess = _1
                    errMsg = _2
                })
                this is QRadioButtons -> checkItems(items, { _1, _2 ->
                    isSuccess = _1
                    errMsg = _2
                })
                this is QCheckbox -> checkItems(items, { _1, _2 ->
                    isSuccess = _1
                    errMsg = _2
                })
            }

            if (isSuccess) {
                function(this)
            } else {
                showError(errMsg, {})
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSelected(p: Int) {
        if (isTablet) {
            fragmentConstructor = CreateQuestionFragment()
            fragmentConstructor?.arguments = Bundle()
            fragmentConstructor?.arguments?.putParcelable("q", getBy(p))

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.questionConstructorFrame, fragmentConstructor)
                    .commit()
        }
    }

    private fun getBy(p: Int): QBase {
        return when (p) {
            0 -> QText()
            1 -> QTextArea()
            2 -> QDropdown()
            3 -> QRadioButtons()
            4 -> QCheckbox()
            else -> throw NotImplementedError()
        }
    }
}