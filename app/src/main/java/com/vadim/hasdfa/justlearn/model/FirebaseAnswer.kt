package com.vadim.hasdfa.justlearn.model

import com.google.firebase.database.IgnoreExtraProperties
import com.vadim.hasdfa.justlearn.utils.Statuses

/**
 * Created by Raksha Vadim on 14.03.2018.
 * With love.
 * In project JustLearn.
 */
@IgnoreExtraProperties
data class FirebaseAnswer(
        var answers: MutableList<CustomPair> = arrayListOf(),
        var status: Statuses = Statuses.Unknown
) {
    companion object {
        fun create(answers: MutableList<Answer>) = FirebaseAnswer(
                answers = answers.map { it cto null }.toMutableList(),
                status = Statuses.Recorded
        )

        infix fun Answer.cto(any: String?) = CustomPair(this, any)


        class CustomPair() {

            var first: Answer = Answer()
            var second: String? = null

            operator fun component1() = first
            operator fun component2() = second

            constructor(first: Answer, second: String?): this() {
                this.first = first
                this.second = second
            }
        }
    }
}